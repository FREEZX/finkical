<?php
class Users_Model extends CI_Model {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////// D A T A   R E T R I V A L //////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getAllUsers($from = -1, $count = -1, $types = array('USER', 'MODERATOR', 'ADMIN')) {
        $qarray = array();
        foreach ($types as $type) {
            $qarray[] = "?";
        }
        $sql = "SELECT id, username, firstname, lastname, user_type FROM users WHERE user_type IN (".implode(', ', $qarray).") ORDER BY firstname ASC ";
        if ($from != -1 && $count != -1)
            $sql .= "LIMIT {$from}, {$count}";
        $q = $this->db->query($sql, $types);
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getUserData($userId) {
        $sql = "SELECT id, username, firstname, lastname, user_type FROM users WHERE id = ?";
        $q = $this->db->query($sql, array($userId));
        if ($q->num_rows() <= 0) {
            return NULL;
        }
        return $q->row_array();
    }

    public function getUserGroups($userid, $from = -1, $count = -1) {
        $sql = "SELECT g.id, g.name, g.description, g.active FROM users_groups_rel AS rel
				LEFT JOIN groups AS g
				ON rel.group_id = g.id
				WHERE rel.user_id=? ORDER BY g.name ";
        if ($from != -1 && $count != -1)
            $sql .= "LIMIT {$from}, {$count}";
        $q = $this->db->query($sql, array($userid));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getUserModeratedGroups($userid, $from = -1, $count = -1) {
        $sql = "SELECT g.* FROM groups AS g
        LEFT JOIN moderators_groups_rel AS mgr
        ON g.id = mgr.group_id
        WHERE mgr.moderator_id = ? 
        ORDER BY g.name ";
        if ($from != -1 && $count != -1)
            $sql .= "LIMIT {$from}, {$count}";
        $q = $this->db->query($sql, array($userid));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getUsersCount() {
        $sql = "SELECT COUNT(id) as num FROM users";
        $q = $this->db->query($sql);
        if (!$q) {
            return NULL;
        }
        $row = $q->row();
        return $row->num;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////// D A T A   I N S E R T A T I O N  &  E D I T I O N ////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//za ova trigger da se napravi (create e problem vo ovaa situacija)
    public function insertUser($userdata) {
        $userHash = generateHashKey();
        $this->db->trans_begin();
        $this->db->query("INSERT INTO users (id, username, firstname, lastname, user_type) VALUES   (?,?,?,?,?)", array($userHash, $userdata['username'], $userdata['firstname'], $userdata['lastname'], $userdata['user_type']));
        $this->db->query("CREATE TABLE `".$this->db->escape_str($userdata['username'])."_events` (id varchar(50), name text, description text, allday tinyint, start_time TIMESTAMP, end_time TIMESTAMP, created_on TIMESTAMP, PRIMARY KEY (id), INDEX (created_on))");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
        }
        return $userHash;
    }
//izmena so moderatori
    //za ova ima trigger, da se sredi kverito
    //sredeno, treba test da se napravi
    public function editUser($userdata) {
        $sql = "UPDATE users SET user_type = ? WHERE id=?";
        $this->db->query($sql, array($userdata['type'], $userdata['userid']));
        if (!$q) {
            return NULL;
        }
        return TRUE;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////// D A T A   V A L I D A T I O N ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function isRegistered($username) {
        $sql = "SELECT id FROM users WHERE username = ?";
        $q = $this->db->query($sql, array($username));
        if (!$q || $q->num_rows() == 0) {
            return FALSE;
        }
        $row = $q->row_array();
        return $row['id'];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////// D A T A   S E A R C H ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//treba paginacija
    public function findUserByName($search) {
        $search = "%".$search."%";
        $sql = "SELECT id, username, user_type, firstname, lastname FROM users WHERE CONCAT(firstname,' ', lastname) LIKE ? ORDER BY firstname LIMIT 0, 10";
        $q = $this->db->query($sql, array($search));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

}
?>

