﻿<?php
class groups_model extends CI_Model {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////D A T A   R E T R I V A L///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getUserGroups($userId, $from = -1, $count = -1) {
        $sql = "SELECT g.id, name, description, u.firstname AS owner_firstname, u.lastname AS owner_lastname, `type`, active FROM users_groups_rel AS ugrel
				LEFT JOIN groups as g ON g.id = ugrel.group_id
                LEFT JOIN moderators_groups_rel AS mgr ON mgr.group_id=g.id
				LEFT JOIN users AS u ON mgr.moderator_id = u.id
				WHERE ugrel.user_id =? ORDER BY name";
                  if($from!=-1 && $count !=-1){
                $sql.=" LIMIT {$from}, {$count}";
                }
        $q = $this->db->query($sql, array($userId));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getOwnedGroups($userId, $from = -1, $count = -1) {
        $sql = "SELECT g.id, g.name, g.description, u.firstname AS owner_firstname, u.lastname AS owner_lastname, g.type, g.active FROM groups as g
				LEFT JOIN moderators_groups_rel AS mgr ON mgr.group_id=g.id
                LEFT JOIN users as u ON mgr.moderator_id = u.id WHERE mgr.moderator_id = ? ORDER BY name";
                  if($from!=-1 && $count !=-1){
                $sql.=" LIMIT {$from}, {$count}";
                }
        $q = $this->db->query($sql, array($userId));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getAllGroups($from = 1, $count = -1) {
        $sql = "SELECT g.id, g.name, g.description, u.firstname AS owner_firstname, u.lastname AS owner_lastname,
		CASE
				 WHEN (ugr.user_id = ?)THEN 1
				 WHEN (gr.user_id = ?) THEN -1
				 WHEN (mgr.moderator_id = ?) THEN 2
				 ELSE 0
		END AS subscribed, 
		`type`, active FROM groups AS g
                LEFT JOIN moderators_groups_rel AS mgr
                ON mgr.group_id=g.id 
				LEFT JOIN users AS u ON mgr.moderator_id = u.id
				LEFT JOIN
				(
					SELECT * FROM users_groups_rel WHERE user_id = ?
				) AS ugr 
				ON ugr.group_id = g.id
				LEFT JOIN 
				(
					SELECT * FROM group_requests WHERE user_id = ?
				) AS gr
				ON gr.group_id = g.id
				ORDER BY g.name";
				  if($from!=-1 && $count !=-1){
                $sql.=" LIMIT {$from}, {$count}";
                }
        $q = $this->db->query($sql, array($this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id')));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getGroupData($groupId) {
        $sql = "SELECT id, name, description, `type`, active FROM groups
				WHERE id = ? AND active=1";
        $q = $this->db->query($sql, array($groupId));
        if (!$q) {
            return NULL;
        }
        return $q->row_array();
    }

    public function getGroupModerators($groupId)
    {
        $sql = "SELECT mgr.moderator_id, u.firstname, u.lastname FROM moderators_groups_rel AS mgr
        LEFT JOIN users AS u
        ON mgr.moderator_id=u.id
        WHERE mgr.group_id = ?";
        $q = $this->db->query($sql, array($groupId));
        if (!$q) {
            return NULL;
        }
        return $q->row_array();   
    }

    public function getGroupsCount() {
        $sql = "SELECT COUNT(id) as num FROM groups";
        $q = $this->db->query($sql);
        if (!$q) {
            return FALSE;
        }
        $row = $q->row();
        return $row->num;
    }

    public function getUserGroupsCount($id) {
        $sql = "SELECT COUNT(id) as num FROM users_groups_rel WHERE user_id = ?";
        $q = $this->db->query($sql, array($id));
        if (!$q) {
            return FALSE;
        }
        $row = $q->row();
        return $row->num;
    }

    public function getOwnedGroupsCount($id) {
        $sql = "SELECT COUNT(id) AS num FROM moderators_groups_rel WHERE moderator_id = ?";
        $q = $this->db->query($sql, array($id));
        if (!$q) {
            return FALSE;
        }
        $row = $q->row();
        return $row->num;
    }

    public function getSubscribedUsers($gid, $uid) {
        $sql = "SELECT rel.id, u.id AS uid, u.username, u.firstname, u.lastname FROM users AS u
					LEFT JOIN users_groups_rel AS rel
					ON rel.user_id=u.id
					LEFT JOIN moderators_groups_rel AS mgr
					ON mgr.group_id = g.id
					WHERE g.id = ? AND mgr.moderator_id = ?";
        $q = $this->db->query($sql, array($gid, $uid));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getUnsubGroupUsers($gid, $uid) {
        $sql = "SELECT u.id, u.username, u.firstname, u.lastname FROM users AS u
					WHERE u.id NOT IN(
						SELECT user_id FROM
						(
								SELECT rel.user_id, rel.group_id FROM users_groups_rel AS rel
								WHERE rel.group_id=?
							UNION
								SELECT gr.user_id, gr.group_id FROM group_requests AS gr
								WHERE gr.group_id=?
						) AS other
						LEFT JOIN moderators_groups_rel AS mgr
						ON mgr.group_id = other.group_id
						AND mgr.moderator_id = ?
						) AND u.id!=? ORDER BY u.firstname";
        $q = $this->db->query($sql, array($gid, $gid, $uid, $uid));
        if (!$q) {
            return FALSE;
        }
        return $q->result_array();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// D A T A   I N S E R T A T I O N   A N D   D E L E T I O N //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//prepravka so moderator
    public function insertGroup($groupData) {
        $id = generateHashKey();
        $sql = "INSERT INTO groups (id, name, description, type, active) VALUES (?,?,?,?,1)";
        $this->db->query($sql, array($id, $groupData['name'], $groupData['description'], $groupData['type']));
         if (!$q) {
            return FALSE;
        }
        return $id;
    }

//treba da se updatne (mnogu stvari ne mi se dobri)
    public function editGroup($name, $description, $id, $type, $owner) {
        $this->db->trans_start();
        $this->db->query("UPDATE groups SET name = ?, description = ?, type = ?, WHERE id = ?", array($name, $description, $type, $id));
        $this->db->query("DELETE FROM users_groups_rel WHERE user_id=? AND group_id = ?", array($owner, $id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return TRUE;
    }

//da se napise triger za ova
    public function deleteGroups($ids) {
        $condition = array();
        $qs = array();
        foreach ($ids as $group) {
            $condition[] = $group;
            $qs[] = "?";
        }
        $this->db->trans_start();
        $this->db->query("SET foreign_key_checks=0");
        $this->db->query("DELETE FROM events WHERE group_id IN (".implode(', ', $qs).")", $condition);
        $this->db->query("DELETE FROM users_groups_rel WHERE group_id IN (".implode(', ', $qs).")", $condition);
        $this->db->query("DELETE FROM groups WHERE id IN (".implode(', ', $qs).")", $condition);
        $this->db->query("SET foreign_key_checks=1");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return FALSE;
        }
        return TRUE;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////// D A T A   V A L I D A T I O N ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//promena po moderator
    public function ValidationGroupOwner($gid, $uid) {
        $sql = "SELECT id FROM moderators_groups_rel WHERE group_id = ? AND moderator_id = ? LIMIT 1";
        $q = $this->db->query($sql, array($gid, $uid));
        if (!$q || $q->num_rows() == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function requestValidation($uid, $gid) {
        $sql = "SELECT rel.id FROM users_groups_rel as rel WHERE rel.user_id=? AND rel.group_id=? UNION SELECT gr.id FROM group_requests as gr WHERE gr.user_id = ? AND gr.group_id = ? LIMIT 1";
        $q = $this->db->query($sql, array($uid, $gid, $uid, $gid));
        if (!$q || $q->num_rows() == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function ValidationUserInUgr($gid, $uid) {
        $sql = "SELECT rel.id FROM users_groups_rel as rel WHERE rel.user_id=? AND rel.group_id=? LIMIT 1";
        $q = $this->db->query($sql, array($uid, $gid, $uid, $gid));
        if (!$q || $q->num_rows() == 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////// D A T A   R E Q U E S T ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//problem
    public function getAllRequests($gid, $id, $from = -1, $count = -1) {
        $sql = "SELECT gr.*, u.firstname, u.lastname FROM group_requests as gr
			LEFT JOIN moderators_groups_rel AS mgr
			ON gr.group_id=mgr.group_id
			LEFT JOIN users AS u
			ON u.id=gr.user_id
			WHERE mgr.group_id = ? AND mgr.moderator_id = ? ORDER BY gr.group_id";
        if ($from != -1 && $count != -1)
            $sql .= " LIMIT {$from}, {$count}";
        $q = $this->db->query($sql, array($gid, $id));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

//promena za moderator
    public function getRequestData($id, $requestid) {
        $sql = "SELECT gr.id, gr.user_id, gr.group_id FROM group_requests as gr
				LEFT JOIN moderators_groups_rel AS mgr
                ON gr.group_id=mgr.group_id 
                WHERE mgr.moderator_id = ? AND gr.id = ?";
        $q = $this->db->query($sql, array($id, $requestid));
        if (!$q || $q->num_rows() == 0) {
            return FALSE;
        }
        return $q->row_array();
    }

//promena za moderator
    public function deleteRequest($id, $requestid) {
        $sql = "DELETE gr.* FROM group_requests AS gr
				LEFT JOIN moderators_groups_rel AS mgr
				ON gr.group_id=mgr.group_id 
                WHERE mgr.moderator_id = ? AND gr.id = ?";
        $q = $this->db->query($sql, array($id, $requestid));
        if (!$q) {
            return FALSE;
        }
        return TRUE;
    }

    public function requestSubscription($userid, $groupid) {
        if ($this->requestValidation($userid, $groupid) === FALSE) {
            $groupData = $this->getGroupData($groupid);
            switch($groupData['type']) {
                case 'PUBLIC' :
                    $sql = "INSERT INTO users_groups_rel (id, user_id, group_id)
						VALUES ('".generateHashKey()."', ?, ?)";
                    break;
                case 'PRIVATE' :
                    $sql = "INSERT INTO group_requests (id, user_id, group_id)
						VALUES ('".generateHashKey()."', ?, ?)";
                    break;
            }
            $q = $this->db->query($sql, array($userid, $groupid));
            if (!$q) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
        return $groupData['type'];
    }

    public function approveSubscription($id, $requestid) {
        if ($request = $this->getRequestData($id, $requestid)) {
            $sql = "INSERT INTO users_groups_rel (id, user_id, group_id) VALUES
				('".generateHashKey()."', ?, ?)";
            $q = $this->db->query($sql, array($request['user_id'], $request['group_id']));
            if (!$q) {
                return FALSE;
            }
            return $this->deleteRequest($id, $requestid);
        } else {
            return FALSE;
        }
    }

    public function unsubscribe($userid, $groupid) {
        $sql = "DELETE FROM users_groups_rel WHERE user_id = ? AND group_id = ?";
        $q = $this->db->query($sql, array($userid, $groupid));
        if (!$q) {
            return FALSE;
        }
        return TRUE;
    }

//WTF e so if-ot??
    public function forcedSubscription($gid, $uid, $ouid) {
        if ($this->ValidationGroupOwner($gid, $ouid) === TRUE && $this->ValidationUserInUgr($gid, $uid) === FALSE && $this->ValidationGroupOwner($gid, $uid) === FALSE) {
            $this->db->trans_start();
            $this->db->query("INSERT INTO users_groups_rel (id, user_id,group_id) VALUES ('".generateHashKey()."',?,?)", array($uid, $gid));
            $this->db->query("SET foreign_key_checks=0");
            $this->db->query("DELETE FROM group_requests WHERE user_id = ? AND group_id = ?", array($uid, $gid));
            $this->db->query("SET foreign_key_checks=1");
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }

//promena po moderator
    public function forcedUnsubscription($rid, $uid) {
        $sql = "DELETE rel.* FROM users_groups_rel AS rel
					LEFT JOIN moderators_groups_rel AS mgr
					ON mgr.group_id=rel.group_id
					WHERE rel.id = ? AND mgr.moderator_id = ?";
        $q = $this->db->query($sql, array($rid, $uid));
        if (!$q) {
            return FALSE;
        }
        return TRUE;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////// D A T A   S E A R C H ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function findGroup($groupName) {
        $groupName = "%".$groupName."%";
        $sql = "SELECT g.id, name, description, u.firstname AS owner_firstname, u.lastname AS owner_lastname, 
			CASE
				 WHEN (ugr.user_id = ?) THEN 1
				 WHEN (gr.user_id = ?) THEN -1
				 WHEN (mgr.moderator_id = ?) THEN 2
				 ELSE 0
			END AS subscribed, 
			 `type`, active FROM groups AS g
                LEFT JOIN moderators_groups_rel AS mgr 
                ON mgr.group_id=g.id
				LEFT JOIN users AS u ON mgr.moderator_id = u.id
				LEFT JOIN
				(
					SELECT * FROM users_groups_rel WHERE user_id = ?
				) AS ugr
				ON ugr.group_id = g.id
				LEFT JOIN 
				(
					SELECT * FROM group_requests WHERE user_id = ?
				) AS gr
				ON gr.group_id = g.id
				WHERE (name LIKE ? OR description LIKE ?) AND active=1";
        $q = $this->db->query($sql, array($this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id'), $this->session->userdata('id'), $groupName, $groupName));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

//promena po moderator
    public function findUnsubUsersGroup($term, $gid, $uid) {
        $search = "%".$term."%";
        $sql = "SELECT u.id, u.username, u.firstname, u.lastname FROM users AS u
					WHERE u.id NOT IN(
						SELECT user_id FROM
						(
								SELECT rel.user_id, rel.group_id FROM users_groups_rel AS rel
								WHERE rel.group_id=?
							UNION
								SELECT gr.user_id, gr.group_id FROM group_requests AS gr
								WHERE gr.group_id=?
						) AS other
						LEFT JOIN moderators_groups_rel AS mgr
						ON mgr.group_id = other.group_id
						AND mgr.moderator_id = ?
						) AND (u.firstname LIKE ? OR u.lastname LIKE ?)";
        $q = $this->db->query($sql, array($gid, $gid, $uid, $search, $search));
        if (!$q) {
            return FALSE;
        }
        return $q->result_array();
    }

}
?>