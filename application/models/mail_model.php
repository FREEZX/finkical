<?php
class Mail_Model extends CI_Model {

    public function mailReport($to, $subject, $body)
    {
        $config = array(
            'protocol' => 'smtp',
            'mailpath' => '/usr/sbin/sendmail',
            'charset'=> 'utf-8',
            'wordwrap' => TRUE,
            'smtp_host' => "smtp.office365.com",
            'smtp_user' => "info@cal.finki.ukim.mk",
            'smtp_pass' => "",
            'smtp_port' => 587,
            'priority' => 1,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            '_smtp_auth' => TRUE,
            'smtp_crypto' => 'tls'
            );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from("info@cal.finki.ukim.mk");
        $this->email->to($to); 
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->send();
       //echo $this->email->print_debugger();
    }
}
?>
