<?php
class Events_Model extends CI_Model {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////D A T A   R E T R I V A L///////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getUserPublicEventsCount($id) {
        $sql = "SELECT COUNT(id) AS num
		 FROM
		 (
		 SELECT e.id
		 FROM events AS e
		 LEFT JOIN moderators_groups_rel AS mgr
		 ON e.group_id = mgr.group_id
		 WHERE mgr.moderator_id=?
		 UNION
		 SELECT e.id
		 FROM events AS e
		 LEFT JOIN users_groups_rel AS ugr
		 ON e.group_id = ugr.group_id
		 WHERE ugr.user_id=?
		 ) AS ev";
        $q = $this->db->query($sql, array($id, $id));
        if (!$q) {
            return FALSE;
        }
        $row = $q->row();
        return $row->num;

    }

    public function getGroupEvents($groupId, $from = -1, $count = -1) {
        $sql = "SELECT e.id, e.name, e.description, r.name AS room, e.allday,
				DATE_FORMAT(e.start_time, '%e %b %h:%i %p') AS start_time,
				DATE_FORMAT(e.end_time, '%e %b %h:%i %p') AS end_time, e.category
				FROM events AS e
                LEFT JOIN rooms AS r
                ON r.id=e.room
				WHERE e.group_id=?
				ORDER BY e.start_time
				DESC";
				if($from!=-1 && $count !=-1){
                $sql.=" LIMIT {$from}, {$count}";
                }
        $q = $this->db->query($sql, array($groupId));
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getAllEvents($from = -1, $count = -1) {
        $sql = "SELECT e.id, e.name AS title, e.description, r.name AS room, e.allday, e.start_time AS start, e.end_time AS end, e.category, g.name AS groupname
				FROM events AS e
				LEFT JOIN groups AS g
				ON e.group_id = g.id
				LEFT JOIN rooms AS r
				ON r.id=e.room
				ORDER BY start_time DESC";
                if($from!=-1 && $count !=-1){
                $sql.=" LIMIT {$from}, {$count}";
                }
        $q = $this->db->query($sql);
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getEventsWithinTimeInRoom($eventData) {
        $sql = "SELECT e.id, e.name, e.description, r.name AS room, e.allday, e.start_time, e.end_time, e.group_id, g.name AS group_name, e.category 
					FROM events AS e
					LEFT JOIN groups AS g
					ON g.id = e.group_id
                    LEFT JOIN rooms AS r
                    ON r.id=e.room
					WHERE (
						(
							?>=e.start_time
							AND ?<=e.end_time
						) 
						OR (
							?>=e.start_time
							AND ?<=e.end_time
						) 
						OR (
							?<=e.start_time
							AND ?>=e.end_time
						)
					) 
					AND e.room = ?";
        $q = $this->db->query($sql, array($eventData['start_time'], $eventData['start_time'], $eventData['end_time'], $eventData['end_time'], $eventData['start_time'], $eventData['end_time'], $eventData['room']));
        if (!$q) {
            return NULL;
        }
        return $q->result();
    }

    public function getEventsCount() {
        $sql = "SELECT COUNT(id) AS num FROM events";
        $q = $this->db->query($sql);
        if (!$q) {
            return NULL;
        }
        $row = $q->row();
        return $row->num;
    }

    public function getPublicEvents($from = -1, $count = -1, $start = '', $end = '') {
        $sql = "SELECT e.id, e.name AS title, e.description, r.name AS room, e.allday AS allDay, e.start_time AS start, e.end_time AS end FROM events AS e
				LEFT JOIN groups AS g
				ON e.group_id=g.id 
                LEFT JOIN rooms as r
                ON r.id=e.room
                WHERE g.type = 'PUBLIC' ";
        if ($start != '' && $end != '')
            $sql .= "AND (UNIX_TIMESTAMP(e.start_time) > ? AND UNIX_TIMESTAMP(e.end_time) < ?) ";
        $sql .= " ORDER BY e.start_time DESC ";
        if ($from != -1 && $start != -1)
            $sql .= " LIMIT {$from}, {$count} ";
        if ($start != '' && $end != '')
            $q = $this->db->query($sql, array($start, $end));
        else
            $q = $this->db->query($sql);
        if (!$q) {
            return NULL;
        } else {
            return $q->result_array();
        }
    }

    public function getSubscribedEvents($userid, $from = -1, $count = -1, $start = '', $end = '') {
        $sql = "SELECT e.id, e.name AS title, e.description, r.name AS room, e.allday AS allDay, e.start_time AS start, e.end_time AS end, e.group_id FROM events AS e
                LEFT JOIN users_groups_rel AS rel 
                ON e.group_id=rel.group_id 
                LEFT JOIN moderators_groups_rel AS mgr
                ON mgr.group_id = e.group_id
                LEFT JOIN rooms AS r
                ON e.room=r.id
				WHERE (rel.user_id=? OR mgr.moderator_id = ?) ";
        if ($start != '' && $end != '')
            $sql .= "AND (UNIX_TIMESTAMP(e.start_time) > ? AND UNIX_TIMESTAMP(e.end_time) < ?) ";
        $sql .= " ORDER BY e.start_time DESC ";
        if ($from != -1 && $start != -1)
            $sql .= " LIMIT {$from}, {$count} ";
        if ($start != '' && $end != '')
            $q = $this->db->query($sql, array($userid, $userid, $start, $end));
        else
            $q = $this->db->query($sql, array($userid, $userid));
        if (!$q) {
            return NULL;
        } else {
            return $q->result_array();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////// D A T A   I N S E R T A T I O N,  E D I T I O N   A N D   D E L E T I O N //////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function insertEvent($eventData) {
        $eventHash = generateHashKey();
        $sql = "INSERT INTO events (id, name, description, room, allday, start_time, end_time, group_id, category, created_on)
			VALUES (?,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP())";
        $q = $this->db->query($sql, array($eventHash, $eventData['name'], $eventData['description'], $eventData['room'], $eventData['allday'], $eventData['start_time'], $eventData['end_time'], $eventData['group_id'], $eventData['category']));
        if (!$q) {
            return NULL;
        }
        return $eventHash;
    }

    public function editGroupEvent($eventData) {
        $sqlUpdates = array();
        if ($eventData['name'] != NULL) {
            $sqlUpdates[] = "name = ?";
        }
        if ($eventData['description'] != NULL) {
            $sqlUpdates[] = "description = ?";
        }
        if ($eventData['room'] != NULL) {
            $sqlUpdates[] = "room = ?";
        }
        if ($eventData['allday'] != NULL) {
            $sqlUpdates[] = "allday = ?";
        }
        if ($eventData['start_time'] != NULL) {
            $sqlUpdates[] = "start_time = ?";
        }
        if ($eventData['end_time'] != NULL) {
            $sqlUpdates[] = "end_time = ?";
        }
        if ($eventData['category'] != NULL) {
            $sqlUpdates[] = "category = ?";
        }
        if (empty($sqlUpdates)) {
            return NULL;
        }
        $sql = "UPDATE events SET ".implode(" ", $sqlUpdates)." WHERE id=?";
        $q = $this->db->query($sql, array($eventData));
        if (!$q) {
            return NULL;
        }
        return TRUE;
    }

    public function deleteEvent($eid, $uid) {
        $sql = "DELETE e.* FROM events AS e 
        LEFT JOIN moderators_groups_rel AS mgr
        ON e.group_id = mgr.group_id 
        WHERE e.id = ? AND mgr.moderator_id = ? ";
        $q = $this->db->query($sql, array($eid, $uid));
        if (!$q) {
            return FALSE;
        }
        return TRUE;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// P R I V A T E  E V E N T S  F U N C T I O N S ///////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function insertUserPrivateEvent($username, $name, $description, $allday, $start_time, $end_time) {
        $sql = "INSERT INTO `".$this->db->escape_str($username)."_events` (id, name, description, allday, start_time, end_time, created_on) VALUES ('".generatehashkey()."', ?, ?, ?, ?, ?, CURRENT_TIMESTAMP())";
        $q = $this->db->query($sql, array($name, $description, $allday, $start_time, $end_time));
        if (!$q) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function deleteUserPrivateEvent($username, $id) {
        $sql = "DELETE FROM `".$this->db->escape_str($username)."_events` where id=?";
        $q = $this->db->query($sql, array($id));
        if (!$q) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function getUserPrivateEvents($username, $from = -1, $count = -1, $start = '', $end = '', $date='') {
        $sql = "SELECT e.id, e.name AS title, e.description, e.allday AS allDay, e.start_time AS start, e.end_time AS end FROM `".$this->db->escape_str($username)."_events` AS e ";
        if ($start != '' && $end != '')
            $sql .= "WHERE UNIX_TIMESTAMP(e.start_time) > ? AND UNIX_TIMESTAMP(e.end_time) < ? ";
        $sql .= "ORDER BY e.start_time DESC ";
        if ($from != -1 && $count != -1)
            $sql .= " LIMIT {$from}, {$count}";
        if ($start != '' && $end != '')
            $q = $this->db->query($sql, array($start, $end));
        else
            $q = $this->db->query($sql);
        if (!$q) {
            return NULL;
        }
        return $q->result_array();
    }

    public function getUserPrivateEventsCount($username) {
        $sql = "SELECT COUNT(id) AS num FROM `".$this->db->escape_str($username)."_events`";
        $q = $this->db->query($sql);
        if (!$q) {
            return FALSE;
        }
        $row = $q->row();
        return $row->num;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////// D A T A  S E A R C H ///////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//da se sredi so ID na room i negovoto ime
    public function findEvent($search) {
        $search = "%".$search."%";
        $sql = "SELECT * FROM (
					SELECT e.id, e.name, e.description, r.name AS room, IF(e.allday=1, true, false), e.start_time, e.end_time, e.group_id
					FROM events AS e 
					UNION
					SELECT pe.id, pe.name, pe.description, 'NONE' AS room, pe.allday, pe.start_time, pe.end_time, 'none' AS group_id
					FROM `".$this->db->escape_str($this->session->userdata('username'))."_events` AS pe
					) AS allev
				LEFT JOIN users_groups_rel AS rel
				ON allev.group_id=rel.group_id 
                LEFT JOIN rooms AS r
                ON r.id = allev.room
                WHERE ((rel.user_id=? OR allev.group_id='none') AND allev.name LIKE ?)";
        $q = $this->db->query($sql, array($this->session->userdata('id'), $search));
        if (!$q) {
            return FALSE;
        } else {
            return $q->result_array();
        }
    }

}
?>