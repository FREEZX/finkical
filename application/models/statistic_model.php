<?php

class Statistic_Model extends CI_Model {

    public function countUsers() {
        $sql = "SELECT COUNT(id) as num FROM users";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }

    public function countGroups() {
        $sql = "SELECT COUNT(id) as num FROM groups";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }

    public function countEvents() {
        $sql = "SELECT COUNT(id) as num FROM events";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }

    public function countPrivateEvents() {
        $condition = array();
        $qs = array();
        $sql = "SELECT sum(table_rows) as num
        FROM information_schema.tables
        WHERE table_schema =  'calendar'
        AND table_name LIKE  '%_events'";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }

    }

    public function countRooms() {
        $sql = "SELECT COUNT(id) as num FROM rooms";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }

    public function countRequests() {
        $sql = "SELECT COUNT(id) as num FROM group_requests";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }

    public function countRelations() {
        $sql = "SELECT COUNT(id) as num FROM users_groups_rel";
        $q = $this->db->query($sql);
        if (!$q) {
            return false;
        } else {
            return $q->row()->num;
        }
    }
}
?>
