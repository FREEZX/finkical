<?php
	function initCAS() {
		phpCAS::setDebug();
		phpCAS::client(CAS_VERSION_2_0, 'cas.finki.ukim.mk', 443, '/cas');
		phpCAS::setNoCasServerValidation();
		phpCAS::forceAuthentication();
	}
?>