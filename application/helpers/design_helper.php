<?php
if ( !defined('BASEPATH')) {
	exit('No direct script access allowed');
}

function html_header($title = '', $css = '', $js = '') {
	echo '<!doctype html>
    <html>
    <head>
    <meta charset="utf-8" />
    <title> ' . $title . '</title>
    <link rel="stylesheet" href="' . base_url() . 'resources/css/smoothness/jquery-ui-1.8.21.custom.css">';
    if($css || $css!=''){
	   load_scripts($css, 'css');
    }
	echo '<script src="' . base_url() . 'resources/js/libs/jquery-1.7.2.min.js"></script>';
	echo '<script src="' . base_url() . 'resources/js/libs/jquery-ui-1.8.21.custom.min.js"></script>';
	echo '
    </head>
    <body>';
}

function html_footer($js = '') {
	echo '
    <script type="text/javascript">
        var baseUrl = "' . base_url() . 'index.php/";
        var baseUrlCstm = "' . base_url() . '";
    </script>
    ';
    if($js || $js!=''){
	   load_scripts($js, 'js');
    }
	echo '
	</body>
	</html>';
}

function js_array($js) {
	$js_array = array(
	'calendar' => array('resources/js/libs/Dock.js', 'resources/js/libs/fullcalendar.js', 'resources/js/libs/jquery.qtip-1.0.0-rc3.min.js', 'resources/js/calendar_view-script.js', 'resources/js/dock_view.js'),
	'settings' => array('resources/js/libs/handlebars-1.0.0.beta.6.js', 'resources/js/libs/noty/jquery.noty.js', 'resources/js/libs/noty/layouts/topCenter.js', 'resources/js/libs/noty/themes/default.js', 'resources/js/settings_view-script.js', 'resources/js/libs/jquery-ui-timepicker-addon.js', 'resources/js/table_view.js'), 
	'statistic' => array('resources/js/table_view.js'),
	'calAPI' => array());
	return $js_array[$js];
}

function css_array($css) {
	$css_array = array(
	'calendar' => array('resources/css/fullcalendar.css', 'resources/css/dock.css', 'resources/css/calendar_users.css', 'resources/css/login.css', 'resources/css/header.css', 'resources/css/footer.css'),
    'settings' => array('resources/css/settings.css', 'resources/css/table.css'), 
	'mail' => array('/resources/css/mail.css'), 
	'statistic' => array('/resources/css/statistic_table.css','resources/css/login.css', 'resources/css/header.css', 'resources/css/footer.css'),
	'calAPI' => array('resources/css/calAPI.css'));
	return $css_array[$css];

}

function load_scripts($view = '', $script_type = 'js') {
	if ($view == '') {
		return false;
	}
	switch ($script_type) {
		case 'js' :
			$js_array = js_array($view);
			foreach ($js_array as $val) {
				echo '<script src="' . base_url() . $val . '"></script>';
			}
			break;
		case 'css' :
			$css_array = css_array($view);
			foreach ($css_array as $val) {
				echo '<link rel="stylesheet" href="' . base_url() . $val . '">';
			}
			break;
	}
	return true;
}
?>