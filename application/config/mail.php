<?php
if ( !defined('BASEPATH'))
      exit('No direct script access allowed');

$config = array(
      'protocol' => 'smtp',
      'mailpath' => '/usr/sbin/sendmail',
      'charset'=> 'utf-8',
      'wordwrap' => TRUE,
      'smtp_host' => "smtp.office365.com",
      'smtp_user' => "info@cal.finki.ukim.mk",
      'smtp_pass' => "",
      'smtp_port' => 587,
      'priority' => 1,
      'crlf' => "\r\n",
      'newline' => "\r\n",
      '_smtp_auth' => TRUE,
      'smtp_crypto' => 'tls'
      );
?>