<?php
if ( !defined('BASEPATH'))
	exit('No direct script access allowed');

$config = array(
	'upload_path' => 'resources/img/rooms/', 	//Snima vo /resources/img/rooms
	'allowed_types' => 'png|jpg|jpeg|gif', 		//tipovi na dozvoleni ekstenzii
	'max_size'	=> 100, 						//Max golemina na datoteka 100KB
	'encrypt_name' => TRUE 						//Imeto na datotekata ke bide kriptirana
	);

?>