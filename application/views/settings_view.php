<?php
html_header('Settings', 'settings');
?>
<div id="navigation">
	<ul class="navigation">
		<span class="nav-title"><img src="<?php echo base_url(); ?>resources/img/icons/gearsIcon.png" width="26" height="26"/>Опции</span>
		<li class="selected-option">
		    <a href="#" id="groups-settings">Групи</a>
		    <span class="rect"></span>
		    <span class="arrow"></span>
		</li>
		<li>
		    <a href="#" id="events-settings">Настани</a>
		</li>
		<?php
		if ($this->session->userdata('user_type') == 'ADMIN')
		{
		?>
		<li>
		    <a href="#" id="admin-settings">Администација</a>
		</li>
		<?php
		}
		?>
	</ul>
	<div id="nav">
		<a href="<?php echo base_url(); ?>" title="Дома">
			<img src="<?php echo base_url(); ?>resources/img/icons/calLogo.png" alt="[Дома]">
		</a>
		<a href="http://outlook.com/finki.ukim.mk" title="Outlook">
			<img src="<?php echo base_url(); ?>resources/img/icons/outlookIcon.png" alt="[Outlook]">
		</a>
		<a href="http://courses.finki.ukim.mk/login" title="Курсеви">
			<img src="<?php echo base_url(); ?>resources/img/icons/coursesIcon.png" alt="[Курсеви]">
		</a>
		<a href="http://cal.finki.ukim.mk/logout" title="Одјавете се">
			<img src="<?php echo base_url(); ?>resources/img/icons/logoutIcon.png" alt="[Одјавете се]">
		</a>
	</div>
</div>
<div class="content">
	<div class="primarytab" id="groups-settings">
		<span class="title">Групи</span>
		<a href="#" class="tab tab1 selected-suboption" data-show="subscribed-groups"><div class="tab2-title">претплатени</div><span class="arrow-down"></span></a>
		<a href="#" class="tab tab2" data-show="my-groups"><div class="tab2-title">мои</div></a>
		<a href="#" class="tab tab3" data-show="all-groups"><div class="tab2-title">сите</div></a>
		<div class="tabentry" id="subscribed-groups">
			<?php 
			if(!empty($groups)){
			?>
			<ul class="groupslist">
				<?php 
				foreach ($groups as $value) {
					?>
					<li class="group-entry" id="<?php echo $value['id']; ?>">
						<div class="name"><?php echo $value['name']; ?></div>
						<div class="operations">
							<div class="details" title="Детали"></div>
							<div class="unsubscribe" title="Откажи претплата"></div>
						</div>
					</li>
				<?php
				}
				?>
				</ul>
			<?php
			}
			else {
			?>
				<div class="empty-list">
					нема групи
				</div>
			<?php } ?>
		</div>
		<div class="tabentry" id="my-groups" style="display: none;">
			<?php
			if(!empty($mygroups)){
			?>
			<ul class="groupslist">
			<?php 
			foreach ($mygroups as $value) {
				?>
				<li class="group-entry" id="<?php echo $value['id']; ?>">
					<div class="name"><?php echo $value['name']; ?></div>
					<div class="operations">
						<div class="details" title="Детали"></div>
						<div class="mygroup" title="Моја група"></div>
					</div>
				</li>
				<?php
				}
				?>
			</ul>
				<?php
				}else{
				?>
				<div class="empty-list">
					нема групи
				</div>
				<?php } ?>
		</div>
		<div class="tabentry" id="all-groups" style="display: none;">
			<form class="search" id="search-groups">
			  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
			</form>
			<?php
			if(!empty($allgroups)){
			?>
			<ul class="groupslist">
			<?php 
			foreach ($allgroups as $value) {
				?>
				<li class="group-entry" id="<?php echo $value['id']; ?>">
					<div class="name"><?php echo $value['name']; ?></div>
					<div class="operations">
						<div class="details" title="Детали"></div>
						<?php
							if($value['subscribed'] == 2)
							{
								?>
								<div class="mygroup" title="Моја група"></div>
								<?php
								}
								else if($value['subscribed'] == 1)
								{
								?>
								<div class="unsubscribe" title="Откажи претплата"></div>
								<?php
								}
								else if($value['subscribed'] == 0)
								{
								?>
								<div class="subscribe" title="Претплати се"></div>
								<?php
								}
								else {
								?>
								<div class="pending" title="Барањето се процесира"></div>
								<?php
								}
							?>
					</div>
				</li>
				<?php
				}
				?>
				</ul>
				<?php
				} else {
				?>
				<div class="empty-list">
					нема групи
				</div>
				<?php } ?>
		</div>
	</div>
	<div class="hidden primarytab" id="events-settings">
		<span class="title">Настани</span>
		<a href="#" class="tab tab1 selected-suboption" data-show="subscribed-events"><div class="tab2-title">претплатени</div><span class="arrow-down"></span></a>
		<a href="#" class="tab tab2" data-show="my-events"><div class="tab2-title">мои</div></a>
		<a href="#" class="tab tab3" data-show="all-events"><div class="tab2-title">јавни</div></a>

		<div class="tabentry" id="subscribed-events">
		<?php if(!empty($subEvents)){ ?>
			<table class="table-list">
				<tr class="list-header">
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Почеток<div class="arrow-down">▼</div></td>
					<td>Крај<div class="arrow-down">▼</div></td>
					<td>Просторија<div class="arrow-down">▼</div></td>
				</tr>
				<?php 
					foreach ($subEvents as $value) {
						?>
						<tr class="list-row">
								<td><?php echo "{$value['title']}"; ?></td>
								<td><?php echo substr($value['start'],0 ,-3);?></td>
								<td><?php echo substr($value['end'],0 ,-3);?></td>
								<td><?php echo "{$value['room']}"; ?></td>
						</tr>
						<?php
						}
					?>
			</table>
			<?php }else{ ?>
				<div class="empty-list">
					нема настани
				</div>
			<?php } ?>
		</div>

		<div class="tabentry" id="my-events" style="display: none">
		<input type="button" class="create-personal-event" value="Нов Настан"></input>
		<?php if(!empty($myevents)){ ?>
			<table class="table-list my">
				<tr class="list-header">
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Почеток<div class="arrow-down">▼</div></td>
					<td>Крај<div class="arrow-down">▼</div></td>
					<td></td>
				</tr>
				<?php 
					foreach ($myevents as $value) {
						?>
						<tr class="list-row">
								<td><?php echo "{$value['title']}"; ?></td>
								<td><?php echo substr($value['start'],0 ,-3);?></td>
								<td><?php echo substr($value['end'],0 ,-3);?></td>
								<td><span class="delete"></span></td>
						</tr>
						<?php
						}
					?>
			</table>
			<?php }else{ ?>
				<div class="empty-list">
					нема настани
				</div>
			<?php } ?>
		</div>

		<div class="tabentry" id="all-events" style="display: none">
			<?php if(!empty($pubevents)){ ?>
			<form class="search" id="search-events">
			  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
			</form>
			<table class="table-list">
				<tr class="list-header">
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Почеток<div class="arrow-down">▼</div></td>
					<td>Крај<div class="arrow-down">▼</div></td>
				</tr>
				<?php 
					foreach ($pubevents as $value) {
						?>
						<tr class="list-row">
								<td><?php echo "{$value['title']}"; ?></td>
								<td><?php echo substr($value['start'],0 ,-3);?></td>
								<td><?php echo substr($value['end'],0 ,-3);?></td>
						</tr>
						<?php
						}
					?>
			</table>
			<?php }else{ ?>
				<div class="empty-list">
					нема настани
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="hidden primarytab" id="admin-settings">
		<span class="title">Админ</span>
		<a href="#" class="tab tab1 selected-suboption" data-show="groups-admin"><div class="tab2-title">групи</div><span class="arrow-down"></span></a>
		<a href="#" class="tab tab2" data-show="events-admin"><div class="tab2-title">настани</div></a>
		<a href="#" class="tab tab3" data-show="rooms-admin"><div class="tab2-title">простории</div></a>
		<a href="#" class="tab tab4" data-show="users-admin"><div class="tab2-title">корисници</div></a>
		<div class="tabentry" id="groups-admin">
			<form class="search tiny" id="search-groups">
			  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
			</form>
			<input type="button" class="create-group" value="Нова Група"></input>
			<?php
			if(!empty($allgroups)){
			?>
			<ul class="groupslist">
			<?php 
			foreach ($allgroups as $value) {
				?>
				<li class="group-entry" id="<?php echo $value['id']; ?>">
					<div class="name"><?php echo $value['name']; ?></div>
					<div class="operations">
						<div class="details" title="Детали"></div>
						<div class="deletegroup delete" title="Избриши група"></div>
					</div>
				</li>
			<?php
			}
			?>
			</ul>
			<?php
			}else{
			?>
				<div class="empty-list">
					нема групи
				</div>
			<?php } ?>
		</div>
		<div class="tabentry" id="events-admin" style="display: none">
			<?php if(!empty($allevents)){ ?>
			<form class="search tiny" id="search-events">
			  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
			</form>
			<table class="table-list">
				<tr class="list-header admin-events">
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Група<div class="arrow-down">▼</div></td>
					<td>Почеток<div class="arrow-down">▼</div></td>
					<td>Крај<div class="arrow-down">▼</div></td>
					<td>Просторија<div class="arrow-down">▼</div></td>
					<td><div class="event-delete"></div></td>
				</tr>

				<?php
					foreach ($allevents as $value) {
						?>
						<tr class="list-row admin-events"  data-id="<?php echo $value['id']; ?>">
							<td><?php echo $value['title']?></td>
							<td><?php echo $value['groupname']?></td>
							<td><?php echo substr($value['start'],0 ,-3);?></td>
							<td><?php echo substr($value['end'],0 ,-3);?></td>
							<td><?php echo $value['room']?></td>
							<td class="event-delete operations"><span class="delete"></span></td>
						</tr>
				<?php
				}
				?>
			</table>
			<?php }else{ ?>
				<div class="empty-list">
					нема настани
				</div>
			<?php } ?>
		</div>
		<div class="tabentry" id="rooms-admin" style="display: none">
			<input type="button" class="create-room" value="Нова Просторија"></input>
			<?php if(!empty($allrooms)){ ?>
			<table class="table-list">
				<tr class="list-header admin-rooms">
					<td>Име на просторија<div class="arrow-down">▼</div></td>
					<td>Факултет<div class="arrow-down">▼</div></td>
					<td><div class="room-delete"></div></td>
				</tr>

				<?php
					foreach ($allrooms as $value) {
						?>
						<tr class="list-row admin-rooms" data-id="<?php echo $value['id']; ?>">
							<td><?php echo $value['name']?></td>
							<td><?php echo $value['faculty_name']?></td>
							<td class="room-delete operations"><span class="delete"></span></td>
						</tr>
				<?php
				}
				?>
				</table>
				<?php
				}else{
				?>
					<div class="empty-list">
						нема простории
					</div>
				<?php } ?>
		</div>
		<div class="tabentry" id="users-admin" style="display: none">
			<?php if(!empty($allusers)){ ?>
			<form class="search tiny" id="search-users">
			  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
			</form>
			<table class="table-list" id="usersTable">
				<tr class="list-header admin-users">
					<td>Индекс<div class="arrow-down">▼</div></td>
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Презиме<div class="arrow-down">▼</div></td>
					<td>Тип/Пермисии<div class="arrow-down">▼</div></td>
				</tr>
				<?php
					foreach ($allusers as $user) {
						?>
						<tr class="list-row admin-users" data-id="<?php echo $user['id']; ?>">
							<td><?php echo "{$user['username']}"; ?></td>
							<td><?php echo "{$user['firstname']}"; ?></td>
							<td><?php echo "{$user['lastname']}"; ?></td>
							<td>
									<select>
										<option value="ADMIN"
										<?php
										if ($user['user_type'] == 'ADMIN')
											echo 'selected';
										?>>
										Администратор
										</option>
										<option value="MODERATOR"
										<?php
										if ($user['user_type'] == 'MODERATOR')
											echo 'selected';
										?>>
										Модератор
										</option>
										<option value="USER"
										<?php
										if ($user['user_type'] == 'USER')
											echo 'selected';
										?>>
										Корисник
										</option>
									</select>
							</td>
						</tr>
					<?php
					}
					?>	
					</table>
					<?php
					}else{
					?>
						<div class="empty-list">
							нема корисници
						</div>
					<?php } ?>
		</div>
	</div>
</div>



<script id="event-list-template" type="text/x-handlebars-template">
	<li class="{{#currenttab "admin-settings"}}list-header-admin admin-events{{/currenttab}}{{#currenttab "events-settings"}}list-header{{/currenttab}}">
		<div class="event-name"><span>Име</span> <div class="arrow-down">▼</div></div>
		<div><span>Група</span> <div class="arrow-down">▼</div></div>
		<div><span>Почеток</span> <div class="arrow-down">▼</div></div>
		<div><span>Крај</span> <div class="arrow-down">▼</div></div>
		{{#currenttab "admin-settings"}}
		<div class="event-room"><span>Просторија</span> <div class="arrow-down">▼</div></div>
		<div class="event-delete"></div>
		{{/currenttab}}
	</li>
	{{#each this}}
	<li class="{{#currenttab "admin-settings"}}list-row-admin admin-events{{/currenttab}}{{#currenttab "events-settings"}}list-row{{/currenttab}}">
		<div class="event-name"><span>{{name}}</span></div>
		<div><span>{{start_time}}</span></div>
		<div><span>{{start_time}}</span></div>
		<div><span>{{end_time}}</span></div>
		{{#currenttab "admin-settings"}}
		<div class="event-room"><span>{{room}}</span></div>
		<div class="event-delete operations"><span class="deleteEvent"></span></div>
		{{/currenttab}}
	</li>
	{{/each}}
</script>

<script id="user-list-template" type="text/x-handlebars-template">
	<li class="list-header-admin">
		<div><span>Индекс</span> <div class="arrow-down">▼</div></div>
		<div><span>Име</span> <div class="arrow-down">▼</div></div>
		<div><span>Презиме</span> <div class="arrow-down">▼</div></div>
		<div><span>Тип/Пермисии</span> <div class="arrow-down">▼</div></div>
	</li>

	{{#each this}}
		<li class="list-row-admin" data-id="{{id}}">
				<div><span>{{username}}</span></div>
				<div><span>{{firstname}}</span></div>
				<div><span>{{lastname}}</span></div>
				<div>
					<span>
						<select>
							<option value="ADMIN"
							{{#equal user_type "ADMIN"}}
								selected
							{{/equal}}>
							Администратор
							</option>

							<option value="MODERATOR"
							{{#equal user_type "MODERATOR"}}
								selected
							{{/equal}}>
							Модератор
							</option>

							<option value="USER"
							{{#equal user_type "USER"}}
								selected
							{{/equal}}>
							Корисник
							</option>
						</select>
					</span>
				</div>
			</li>
	{{/each}}
	</ul>
</script>

<script id="subscription-template" type="text/x-handlebars-template">
	<div class="tabs" data-groupid="{{groupid}}">
		<a href="#" class="selected" data-show="pending">Баратели</a>
		<a href="#" data-show="subscribed">Членови</a>
		<a href="#" data-show="unsubscribed">Незачленети</a>​
	</div>
	<ul class="userlist" id="pending">
		{{#each pending}}
			<li class="userlist-row" data-id="{{id}}">
				<div class="name">{{firstname}} {{lastname}}</div>
				<div class="operations">
				<div class="accept" title="Одобри"></div>
				<div class="refuse" title="Одбиј"></div>
				</div>
			</li>
		{{/each}}
	</ul>
	<ul class="userlist" id="subscribed" style="display: none">
		{{#each subscribed}}
			<li class="userlist-row" data-id="{{id}}">
				<div class="name">{{firstname}} {{lastname}}</div>
				<div class="operations">
				<div class="remove" title="Отстрани"></div>
				</div>
			</li>
			</li>
		{{/each}}
	</ul>
	<ul class="userlist" id="unsubscribed" style="display: none">
		<form class="search overlayed" id="search-users">
		  	<input id="term" name="term" type="text" size="40" placeholder="Search..." />
		</form>
		{{#each unsubscribed}}
			<li class="userlist-row" data-id="{{id}}">
				<div class="name">{{firstname}} {{lastname}}</div>
				<div class="operations">
					<div class="force-subscribe" title="Претплати"></div>
				</div>
			</li>
		{{/each}}
	</ul>
</script>

<script id="group-list-template" type="text/x-handlebars-template">
	{{#each this}}
	<li class="group-entry" id="{{id}}">
		<div class="name">{{name}}</div>
		<div class="operations">
			<div class="details" title="Детали"></div>
			{{#currenttab "admin-settings"}}
				<div class="deletegroup delete"</div>
			{{/currenttab}}
			{{#currenttab "groups-settings"}}
				{{#equal subscribed 2}}
				<div class="mygroup" title="Моја група"></div>
				{{/equal}}
				{{#equal subscribed 1}}
				<div class="unsubscribe" title="Откажи претплата"></div>
				{{/equal}}
				{{#equal subscribed 0}}
				<div class="subscribe" title="Претплати се"></div>
				{{/equal}}
				{{#equal subscribed "-1"}}
				<div class="pending" title="Барањето се процесира"></div>
				{{/equal}}
			{{/currenttab}}
		</div>
	</li>
	{{/each}}
</script>

<script id="group-details-template" type="text/x-handlebars-template">
	<ul class="details-list">
		<li>
			<div class="field-name">Име:</div>
			<div class="field-value">{{name}}</div>
		</li>
		<li>
			<div class="field-name">Модератори:</div> 
			<!--izmena na skriptata --><div class="field-value">{{#each moderators}}<p>{{firstname}} {{lastname}}</p>{{/each}}</div>
		</li>
		<li>
			<div class="field-name">Тип:</div> 
			<div class="field-value">{{type}}</div>
		</li>
		<li>
			<div class="field-name">Опис:</div>
			<div class="field-value">{{description}}</div>
		</li>

		{{#if events}}
		<li id="event-table-item">
			<div class="field-name">Настани:</div>
			<table class="table-list admin">
				<tr class="list-header">
					<td>Име<div class="arrow-down">▼</div></td>
					<td>Почеток<div class="arrow-down">▼</div></td>
					<td>Крај<div class="arrow-down">▼</div></td>
					<td>Просторија<div class="arrow-down">▼</div></td>
					<td class="event-delete"></td>
				</tr>
				{{#each events}}
					<tr class="list-row">
							<td>{{this.name}}</td>
							<td>{{this.start_time}}</td>
							<td>{{this.end_time}}</td>
							<td>{{this.room}}</td>
							<td><span class="delete"></span></td>
					</tr>
				{{/each}}
			</table>
		</li>
		{{/if}}
		{{createEventButton this.id this.owner}}
	</ul>
</script>



<div class="hidden" id="overlay">
	<div id="cross"></div>
	<div id="overlay-content"></div>
</div>

<div class="hidden" id="event-template">
	<form>
		<ul class="details-list">
		<input type="hidden" id="groupid" name="group_id"/>
		<li><div class="field-name"><label for="name">Име</label></div><div class="field-value"><input type="text" name="name" id="name"/> </div></li>
		<li><div class="field-name"><label for="description">Опис</label></div><div class="field-value"><input type="text" name="description" id="description"> </div></li>
		<li><div class="field-name"><label for="room">Просторија</label></div><div class="field-value"><select name="room" id="room">
			<?php
				foreach ($allroomsselect as $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
					<?php
					}
			?>
		</select></div></li>
		<li><div class="field-name"><label for="allday">Целодневен</label></div>
		<div class="field-value"><input type="checkbox" id="allday"  name="allday"/>
		</div></li>
		<li><div class="field-name"><label for="repeat">Повторување</label></div>
		<div class="field-value"><input type="checkbox" id="repeat" name="repeat"/>
		</div></li>
		<li><div class="field-name"><label for="start-time">Почетно време</label></div><div class="field-value"><input type="text" name="start_time" id="start-time"/> </div></li>
		<li><div class="field-name"><label for="end-time">Крајно време</label></div><div class="field-value"><input type="text" name="end_time" id="end-time"/> </div></li>
		<li><div class="field-name"><label for="category">Категорија</label></div><div class="field-value"><select name="category" id="category"> 
			<option value="PROFESSIONAL">Професионален</option>
			<option value="ACADEMIC">Академски</option>
			<option value="RECREATIVE">Рекреативен</option>
		</select> </div></li>
		<input type="button" id ="add-event" value="Додади"></input>
		</ul>
	</form>
</div>

<div class="hidden" id="personal-event-template">
	<form>
		<ul class="details-list">
		<li><div class="field-name"><label for="name">Име</label></div><div class="field-value"><input type="text" name="name" id="name"/> </div></li>
		<li><div class="field-name"><label for="description">Опис</label></div><div class="field-value"><input type="text" name="description" id="description"> </div></li>
		<li><div class="field-name"><label for="allday">Целодневен</label></div>
		<div class="field-value"><input type="checkbox" id="allday" name="allday"/>
		</div></li>
		<li><div class="field-name"><label for="repeat">Повторување</label></div>
		<div class="field-value"><input type="checkbox" id="repeat" name="repeat"/>
		</div></li>
		<li><div class="field-name"><label for="start-time">Почетно време</label></div><div class="field-value"><input type="text" name="start_time" id="start-time"/> </div></li>
		<li><div class="field-name"><label for="end-time">Крајно време</label></div><div class="field-value"><input type="text" name="end_time" id="end-time"/> </div></li>
		<input type="button" id ="add-personal-event" value="Додади"></input>
		</ul>
	</form>
</div>

<div class="hidden" id="group-template">
	<form>
		<ul class="details-list">
		<li><div class="field-name"><label for="name">Име</label></div><div class="field-value"><input type="text" name="name" id="name"> </div></li>
		<li><div class="field-name"><label for="description">Опис</label></div><div class="field-value"><input type="text" name="description" id="description"></div> </li>
		<li><div class="field-name"><label for="userid">Модератори</label></div><div class="field-value"><select name="userid" id="userid">
			<?php
				foreach ($allmodadmin as $value) {
					?>
					<option value="<?php echo $value['id']; ?>"><?php echo $value['firstname'] . ' ' . $value['lastname']; ?></option>
					<?php
					}
			?>
		</select></div></li>
		<li><div class="field-name"><label for="type">Тип</label></div><div class="field-value">
		<select id="type" name="type"> 
			<option value="PRIVATE" selected="selected">Приватна</option>
			<option value="PUBLIC">Јавна</option>
		</select></div></li>
		<input type="button" id="add-group" value="Додади">
</ul>
	</form>
</div>

<div class="hidden" id="room-template">
	<form>
		<ul class="details-list">
			<li><div class="field-name"><label for="name">Име</label></div><div class="field-value"><input type="text" name="name" id="name"/> </div></li>
			<li><div class="field-name"><label for="description">Опис</label></div><div class="field-value"><input type="text" name="description" id="description"> </div></li>
			<li><div class="field-name"><label for="faculty_name">Име на факултет</label></div><div class="field-value"><input type="text" name="faculty_name" id="faculty_name"> </div></li>
			<input type="button" id="upload-picture" value="Одбери слика"></input>
			<input type="button" id="add-room" value="Додади"></input>
		</ul>
	</form>
</div>

<div id="privilegeChangeDialog">
		Дали сте сигурни дека сакате да ги промените привилегиите на корисникот?
		<button class="yes">Да</button><button class="no">Не</button>
</div>

<div id="eventDeleteDialog" class="hidden">
		Дали сте сигурни дека сакате да го избришете настанот?
		<button class="yes">Да</button><button class="no">Не</button>
</div>

<div id="original-elements" class="hidden">
	<span class="arrow-down"></span>
	<div class="mygroup" title="Моја група"></div>
	<div class="unsubscribe" title="Откажи претплата"></div>
	<div class="subscribe" title="Претплати се"></div>
	<div class="pending" title="Барањето се процесира"></div>
</div>

<div id="fail-dialog" class="hidden">
	This action could not be completed.
</div>

<script type="text/javascript">var userId ="<?php echo $this->session->userdata('id') ?>";</script>
<?php
html_footer('settings');
?>