<?php
html_header('Statistic', 'statistic');
?>
<div class="tablediv">
	<table class="table-list">
		<tbody>
			<tr class="list-header">
				<td>Табела<div class="arrow-down">▼</div></td>
				<td>Број на редици<div class="arrow-down">▼</div></td>
			</tr>
			<tr class="list-row">
				<td>Корисници</td>
				<td><?php echo $users; ?></td>
			</tr>
			<tr class="list-row">
				<td>Групи</td>
				<td> <?php echo $groups; ?> </td>
			</tr>
			<tr class="list-row">
				<td>Простории</td>
				<td><?php echo $rooms; ?> </td>
			</tr>
			<tr class="list-row">
				<td>Релации</td>
				<td><?php echo $relations; ?></td>
			</tr>
			<tr class="list-row">
				<td>Барања кои сеуште чекаат</td>
				<td><?php echo $requests; ?></td>
			</tr>
			<tr class="list-row" >
				<td>Приватни Настани</td>
				<td><?php echo $privevents; ?></td>
			</tr>
			<tr class="list-row">
				<td>Настни</td>
				<td><?php echo $pubevents; ?></td>
			</tr>
		</tbody>
	</table>
</div>
<?php
html_footer('statistic');
?>