<?php html_header('Finki Cal', 'calendar'); ?>
<div class="container">

	<div id="meni">
		<!-- Note: comments are used between icons to remove white-space when display inline-block is used -->
		<div id="logo"><a href="http://finki.ukim.mk/"><img src="<?php echo base_url(); ?>resources/img/finki_logo.png" /></a>
		<?php 
		if(!$this->session->userdata('loggedin')){
			echo '<span id="login"><a href="login">Login</a></span>';
            echo '<div class="logininfo"><span class="botLogin"><a href="https://www.finki.ukim.mk/studentlogin.reset/">Ја заборавив лозинката</a><span></div>';
		}else{
			echo '<div class="logininfo"><span class="botLogin"><a href="https://www.finki.ukim.mk/studentlogin.reset/">Промена на лозинка</a><span></div>';
		}
		?>
		</div>
		<div id="dock">
			<ul>
				<!--
				-->
				<li>
					<a href="http://www.outlook.com/students.finki.ukim.mk"><span>Outlook</span><img src="<?php echo base_url(); ?>resources/img/icons/outlookIcon.png" alt="[Outlook]"></a>
				</li><!--
				-->
				<li>
					<a href="https://login.microsoftonline.com/login.srf?whr=students.finki.ukim.mk&amp;wa=wsignin1.0&amp;wtrealm=https%3a%2f%2faccesscontrol.windows.net%2f&amp;wreply=https%3a%2f%2ffinkiukim-my.sharepoint.com"><span>Sharepoint</span><img src="<?php echo base_url(); ?>resources/img/icons/sharepointIcon.png" alt="[Sharepoint]"></a>
				</li><!--
				-->
				<li>
					<a href="https://skydrive.live.com/"><span>Sky Drive</span><img src="<?php echo base_url(); ?>resources/img/icons/skydriveIcon.png" alt="[Sky Drive]"></a>
				</li><!--
				-->
				<li>
					<a href="http://code.finki.ukim.mk/"><span>Code</span><img src="<?php echo base_url(); ?>resources/img/icons/code.png" alt="[Code]"></a>
				</li><!--
				-->
				<li>
					<a href="http://courses.finki.ukim.mk/login"><span>Курсеви</span><img src="<?php echo base_url(); ?>resources/img/icons/coursesIcon.png" alt="[Курсеви]"></a>
				</li><!--
				ТРЕБА НОВА ИКОНА ЗА Е-ЛАБ!!!
				-->
				<li>
					<a href="http://le.finki.ukim.mk/Login.aspx?ReturnUrl=%2fdefault.aspx"><span>Learning environment</span><img src="<?php echo base_url(); ?>resources/img/icons/le-Icon.png" alt="[Learning environment]"></a>
				</li><!--
				-->
				<li>
					<a href="https://finki.iknow.ukim.edu.mk"><span>iKnow</span><img src="<?php echo base_url(); ?>resources/img/icons/coursesIcon.png" alt="[Електронско досие]"></a>
				</li><!--
				-->
				<li>
					<a href="http://help.students.finki.ukim.mk/"><span>Систем за помош</span><img src="<?php echo base_url(); ?>resources/img/icons/helpIcon.png" alt="[Систем за помош на корисници]"></a>
				</li><!--
				-->
				<li>
					<a href="http://news.finki.ukim.mk/login/index.php"><span>Соопштенија</span><img src="<?php echo base_url(); ?>resources/img/icons/newsIcon.png" alt="[Соопштенија]"></a>
				</li><!--
				-->
				<li>
					<a href="http://etest.finki.ukim.mk/"><span>еТест</span><img src="<?php echo base_url(); ?>resources/img/icons/etestIcon.png" alt="[еТест]"></a>
				</li><!--
				-->
				<li>
					<a href="settings"><span>Опции</span><img src="<?php echo base_url(); ?>resources/img/icons/gearsIcon.png" alt="[Опции]"></a>
				</li><!--
				-->
				<?php 
				if($this->session->userdata('loggedin')){
                echo '<li>
					<a href="'.base_url().'/logout"><span>Одјавете се</span><img src="'.base_url().'resources/img/icons/logoutIcon.png" alt="[Одјавете се]"/></a>
				</li>';
                }
                ?>
			</ul>
		</div>
	</div>
	<div id="calendar">
	</div>
</div>
<footer>
	<div id="footer-div">
	<span>
		©2012 Connect Lab.
		<a href="http://www.facebook.com/ConnectLab">
		<img src="<?php echo base_url(); ?>resources/img/clogo.png" />
		</a>
	</span>
	</div>
</footer>
<?php html_footer('calendar'); ?>