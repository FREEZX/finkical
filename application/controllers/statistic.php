<?php
class Statistic extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if($this->session->userdata('user_type')!='ADMIN')
            exit();
        $this->load->model('statistic_model');
    }

    public function index() {

        $information = array(
            'users'         => $this->statistic_model->countUsers(), 
            'groups'        => $this->statistic_model->countGroups(), 
            'pubevents'     => $this->statistic_model->countEvents(), 
            'privevents'    => $this->statistic_model->countPrivateEvents(),
            'rooms'         => $this->statistic_model->countRooms(), 
            'requests'      => $this->statistic_model->countRequests(), 
            'relations'     => $this->statistic_model->countRelations(), 
            );

        $this->load->view('statistic_view',$information);
    }
}
?>
