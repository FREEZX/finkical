<?php
class EventsManager extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('events_model');
    }

    public function getUserEvents() {
        $start = $_GET['start'];
        $end = $_GET['end'];
        $events = array();
        if ($this->session->userdata('loggedin')) {
            if ($temp = $this->events_model->getUserPrivateEvents($this->session->userdata('username'), -1, -1, $start, $end)) {
                foreach ($temp as $key => $value) {
                    $events[] = $value;
                }
            }
            if ($temp = $this->events_model->getSubscribedEvents($this->session->userdata('id'), -1, -1, $start, $end)) {
                foreach ($temp as $key => $value) {
                    $events[] = $value;
                }
            }
        }
        else{
        if ($temp = $this->events_model->getPublicEvents(-1, -1, $start, $end)) {
            foreach ($temp as $key => $value) {
                $events[] = $value;
            }
        }
                }
        echo json_encode($events);

        exit();
    }

    public function listUserPrivateEvents() {
        if ($this->session->userdata('loggedin')) {
            $from = $this->input->post('from');
            $count = $this->input->post('count');
            $this->form_validation->set_rules('from', 'LimitationFrom', 'is_natural');
            $this->form_validation->set_rules('count', 'LimitationCount', 'is_natural_no_zero');
            if ($this->form_validation->run())
                echo json_encode($this->events_model->getUserPrivateEvents($this->session->userdata('username')), $from, $count);
            else
                echo json_encode($this->events_model->getUserPrivateEvents($this->session->userdata('username')));
        }
        exit();
    }

    public function insertGroupEvent() {

        $startTime = $this->input->post('start_time');
        $endTime = $this->input->post('end_time');
        $name = $this->input->post('name');
        $allday = $this->input->post('allday');
        $description = $this->input->post('description');
        $room = $this->input->post('room');
        $category = $this->input->post('category');
        $groupId = $this->input->post('group_id');
        if($allday !== FALSE){
            $allday = 1;
        }
        else {
            $allday = 0;
        }

        if (
                ($this->session->userdata('user_type') != 'USER') &&
                $name &&
                (
                    ($this->session->userdata('loggedin') !== 0) &&
                    ($allday == 1 && $startTime)
                ) ||
                (
                    ($allday == 0) && 
                    ($startTime && $endTime) &&
                    ($startTime < $endTime)
                )
            ) {
            $eventData = array(
                'name'          => $name,
                'description'   => $description,
                'room'          => $room,
                'allday'        => $allday,
                'start_time'    => $startTime,
                'end_time'      => $endTime,
                'group_id'      => $groupId,
                'category'      => $category
            );

            if ($otherEvent = $this->events_model->getEventsWithinTimeInRoom($eventData)) {
                echo json_encode(array('status' => FALSE, 'data' => $otherEvent));
            } else {
                echo json_encode(array('status' => TRUE, 'data' => $this->events_model->insertEvent($eventData)));
            }
        }
        else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function deleteEvent() {
        $id = $this->input->post('id');
        if (($this->session->userdata('loggedin') || ($this->session->userdata('user_type') == 'ADMIN'))) {
            echo json_encode($this->events_model->deleteEvent($id,$this->session->userdata('id')));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    // public function editEvent() {
        // if ($this->session->userdata('loggedin') && $this->events_model->Validation($this->input->post('id'), $this->session->userdata('id')) && (($this->input->post('start_time')) < ($this->input->post('end_time')))) {
            // $eventData = array('name' => $this->input->post('name'), 'description' => $this->input->post('description'), 'room' => $this->input->post('room'), 'allday' => $this->input->post('allday'), 'start_time' => $this->input->post('start_time'), 'end_time' => $this->input->post('end_time'), 'category' => $this->input->post('category'), 'active' => $this->input->post('active'), 'id' => $this->input->post('id'));
            // echo json_encode($this->events_model->editEvent($eventData));
        // } else {
            // echo json_encode(FALSE);
        // }
        // exit();
    // }

    public function insertPrivateEvent() {
        $startTime = $this->input->post('start_time');
        $endTime = $this->input->post('end_time');
        $name = $this->input->post('name');
        $allday = $this->input->post('allday');
        $description = $this->input->post('description');
        if($allday !== FALSE){
            $allday = TRUE;
        }
        if( ($this->session->userdata('loggedin') !== FALSE) && $name && 
                (($allday && $startTime) || ($allday === FALSE && ($startTime && $endTime) && ($startTime < $endTime)))) {
            echo json_encode($this->events_model->insertUserPrivateEvent($this->session->userdata('username'), $name, $description, $allday, $startTime, $endTime));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function deletePrivateEvent() {
        if ((($this->session->userdata('loggedin') !== FALSE) && (($gid = $this->input->post('id')) !== FALSE))) {
            echo json_encode($this->events_model->deleteUserPrivateEvent($this->session->userdata('username'), $gid));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function findEvent() {
        $search = $this->input->post('term');
        if ($search !== FALSE) {
            echo json_encode($this->events_model->findEvent($search));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

}
?>