<?php
class Settings extends CI_Controller {
	public function __construct() {
		parent::__construct();
		if($this->session->userdata('id')===FALSE)
		{
			exit();
		}
		$this->load->model('groups_model');
		$this->load->model('events_model');
		$this->load->model('rooms_model');
		$this->load->model('users_model');	
	}
	
	public function index() {
		$groups = $this->groups_model->getUserGroups($this->session->userdata('id'));
		$myGroups = $this->groups_model->getOwnedGroups($this->session->userdata('id'));
		$allGroups = $this->groups_model->getAllGroups();
		$userPublicEvents = $this->events_model->getPublicEvents();
		$userPrivateEvents = $this->events_model->getUserPrivateEvents($this->session->userdata('username'));
		$userSubscribedEvents = $this->events_model->getSubscribedEvents($this->session->userdata('id'));
		$allRooms = $this->rooms_model->getAllRooms();
		$allRoomsSelect = $this->rooms_model->getAllRooms();
		if ($this->session->userdata('user_type') == 'ADMIN') {
			$allModAdmin = $this->users_model->getAllUsers( -1, -1, array("MODERATOR", "ADMIN"));
			$allUsers = $this->users_model->getAllUsers();
			$allEvents = $this->events_model->getAllEvents();
			$data = array('allusers' => $allUsers, 'mygroups' => $myGroups, 'allgroups' => $allGroups, 'allevents' => $allEvents, 'allrooms' => $allRooms, 'allroomsselect' => $allRoomsSelect, 'allmodadmin' => $allModAdmin, 'groups' => $groups, 'mygroups' => $myGroups, 'myevents' => $userPrivateEvents, 'subEvents' => $userSubscribedEvents, 'pubevents' => $userPublicEvents);
		} else {
			$data = array('groups' => $groups, 'mygroups' => $myGroups, 'allgroups' => $allGroups, 'allroomsselect' => $allRoomsSelect, 'allrooms' => $allRooms, 'myevents' => $userPrivateEvents, 'subEvents' => $userSubscribedEvents, 'pubevents' => $userPublicEvents);
		}
		$this->load->view('settings_view', $data);
	}

}
?>