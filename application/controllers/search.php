<?php
class Search extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('user_type') != 'ADMIN')
            redirect('http://cal.finki.ukim.mk/calendar', 'refresh');
        $this->load->model('users_model');
        $this->load->model('groups_model');
        $this->load->model('events_model');
    }
    public function index() {
    	$search = $this->input->post('term');
    	$type = $this->input->post('type');
    	if($search!==FALSE && $type!==FALSE)
    	{
    		switch($type)
    		{
    			case 'findUserByName':
    			{
    				echo json_encode($this->users_model->findUserByName($search));
    				break;
    			}
    			case 'findEvent':
    			{
    				echo json_encode($this->events_model->findEvent($search));
    				break;
    			}
    			case 'findGroup':
    			{
    				echo json_encode($this->groups_model->findGroup($groupName));
    				break;
    			}
    			/*case 'findUnsubUsersGroup':
    			{

    				break;
    			}*/
    			case 'findRooms':
    			{
    				echo json_encode($this->rooms_model->findRoom($search));
    				break;
    			}
    			default:
    			{
    				echo json_encode(FALSE);
    				break;
    			}
    		}
    	}
    	else{
    		echo json_encode(FALSE);   
    	} 
    	exit();
    }
	/*public function findUnsubUsersGroup() {
		if (($this->session->userdata('loggedin')) && ($this->session->userdata('user_type') != 'USER') && ($gid = $this->input->post('id')) !== FALSE) {
			$search = $this->input->post('term');
			echo json_encode($this->groups_model->findUnsubUsersGroup($search, $gid, $this->session->userdata('id')));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}*/
}
?>
