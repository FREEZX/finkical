﻿<?php
class GroupsManager extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('groups_model');
		$this->load->model('users_model');
	}

	public function deleteGroups() {
		if (($this->session->userdata('user_type') == 'ADMIN') && ($ids = $this->input->post('ids')) !== FALSE) {
			echo json_encode($this->groups_model->deleteGroups($ids));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function getGroupData() {
		if (($this->session->userdata('loggedin')) && ($groupId = $this->input->post('groupid')) !== FALSE) {
			$groupData = $this->groups_model->getGroupData($groupId);
			$this->load->model('events_model');
			$groupData['events'] = $this->events_model->getGroupEvents($groupId);
			$groupData['moderators'] = $this->groups_model->getGroupModerators($groupId);
			echo json_encode($groupData);
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function insertGroup() {
		if ($this->session->userdata('user_type') == 'ADMIN') {
			$groupName = $this->input->post('name');
			$ownerID = $this->input->post('userid');
			$groupDesc = $this->input->post('description');
			$groupType = $this->input->post('type');

			if ( !($groupName == '' || $groupDesc == '' || $ownerID == '' || !$this->users_model->getUserData($ownerID))) {
				$groupData = array("name" => $groupName, "description" => $groupDesc, "id" => generateHashKey(), "type" => $groupType, "owner" => $ownerID);
				echo json_encode($this->groups_model->insertGroup($groupData));
			}
			exit();
		}
		echo json_encode(FALSE);
		exit();
	}

	public function editGroup() {
		if ($this->session->userdata('loggedin') && $this->session->userdata('user_type') != 'USER' && $this->groups_model->Validation($this->input->post('id'), $this->session->userdata('id')) && ($groupID = $this->input->post('id')) !== FALSE) {
			$groupName = $this->input->post('name');
			$groupDescription = $this->input->post('description');
			$groupType = $this->input->post('type');
            if($this->session->userdata('user_type') == 'ADMIN')
            $owner = $this->input->post('owner');
            //treba da se napravi check dali postoi userot
            //toa ke go sredam narednive denovi
			echo json_encode($this->groups_model->editGroup($groupName, $groupDescription, $groupID, $groupType));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function requestSubscription() {
		if ($this->session->userdata('loggedin') && ($groupid = $this->input->post('groupid')) !== FALSE) {
			echo json_encode($this->groups_model->requestSubscription($this->session->userdata('id'), $groupid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function approveSubscription() {
		if (($this->session->userdata('user_type') != 'USER') && ($subscriptionid = $this->input->post('id')) !== FALSE) {
			echo json_encode($this->groups_model->approveSubscription($this->session->userdata('id'), $subscriptionid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function deleteRequest() {
		if (($this->session->userdata('user_type') != 'USER') && ($requestid = $this->input->post('id')) !== FALSE) {
			echo json_encode($this->groups_model->deleteRequest($this->session->userdata('id'), $requestid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function unsubscribe() {
		if ($this->session->userdata('loggedin') && ($groupid = $this->input->post('groupid')) !== FALSE) {
			echo json_encode($this->groups_model->unsubscribe($this->session->userdata('id'), $groupid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function getSubsribedUsers() {
		if (($this->session->userdata('loggedin')) && ($groupid = $this->input->post('groupid')) !== FALSE) {
			echo json_encode($this->groups_model->getSubsribedUsers($groupid, $this->session->userdata('id')));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function getGroupUsers() {
		if (($this->session->userdata('loggedin')) && ($this->session->userdata('user_type') != 'USER') && ($gid = $this->input->post('id')) !== FALSE) {
			$result = array('pending' => $this->groups_model->getAllRequests($gid, $this->session->userdata('id')), 'subscribed' => $this->groups_model->getSubscribedUsers($gid, $this->session->userdata('id')), 'unsubscribed' => $this->groups_model->getUnsubGroupUsers($gid, $this->session->userdata('id')));
			echo json_encode($result);
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function forcedSubscription() {
		if (($this->session->userdata('loggedin')) && ($this->session->userdata('user_type') != 'USER') && (($gid = $this->input->post('groupid')) !== FALSE) && (($uid = $this->input->post('userid')) !== FALSE)) {
			echo json_encode($this->groups_model->forcedSubscription($gid, $uid, $this->session->userdata('id')));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function forcedUnsubscription() {
		if (($this->session->userdata('loggedin')) && ($rid = $this->input->post('id')) !== FALSE) {
			echo json_encode($this->groups_model->forcedUnsubscription($rid, $this->session->userdata('id')));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function findGroup() {
		if ($this->session->userdata('loggedin')) {
			$groupName = $this->input->post('term');
			echo json_encode($this->groups_model->findGroup($groupName));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

	public function findUnsubUsersGroup() {
		if (($this->session->userdata('loggedin')) && ($this->session->userdata('user_type') != 'USER') && ($gid = $this->input->post('id')) !== FALSE) {
			$search = $this->input->post('term');
			echo json_encode($this->groups_model->findUnsubUsersGroup($search, $gid, $this->session->userdata('id')));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}

}
?>