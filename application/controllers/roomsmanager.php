<?php
class RoomsManager extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('rooms_model');
	}

	public function roomsList() {/*pub*/
		$from = $this->input->post('from');
		$count = $this->input->post('count');
		$faculty = $this->input->post('faculty');
		$this->form_validation->set_rules('from', 'LimitationFrom', 'is_natural');
		$this->form_validation->set_rules('count', 'LimitationCount', 'is_natural_no_zero');
		if ($this->form_validation->run()) {
			echo json_encode($this->rooms_model->getAllRooms($from, $count, $faculty));
		} else {

			echo json_encode($this->rooms_model->getAllRooms($faculty));
		}
		exit();
	}

	public function insertRoom() {
		if ($this->session->userdata('user_type') == 'ADMIN') {
			$room['name'] = $this->input->post('name');
			$room['faculty_name'] = $this->input->post('faculty_name');
			$room['description'] = $this->input->post('description');
			$config['upload_path'] = 'resources/img/rooms/';
			$config['allowed_types'] = 'png|jpg|jpeg|gif';
			$config['max_size']	= 100; //MAX 100KB
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			if ($room['name']!==FALSE && $room['faculty_name']!==FALSE) {
				if($this->upload->do_upload('picupload'))
				{

					$uploadData = $this->upload->data();
					$room['picture_name'] = $uploadData['file_name'];
					echo json_encode($this->rooms_model->insertRoom($room));
					exit();
				}
				else
				{
					print_r($this->upload->display_errors());
				}
			}
		}
		echo json_encode(FALSE);
		exit();
	}

	public function deleteRoom() {
		if ($this->session->userdata('user_type') == 'ADMIN' && ($roomid = $this->input->post('room_id')) !== FALSE) {
			echo json_encode($this->rooms_model->deleteRoom($roomid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}
	public function editRoom() {
		if ($this->session->userdata('user_type') == 'ADMIN') {
			$roomid = $this->input->post('room_id');
			$roomData['name'] = $this->input->post('name');
			$roomData['faculty_name'] = $this->input->post('faculty_name');
			$roomData['description'] = $this->input->post('description');
			$config['upload_path'] = 'resources/img/rooms/';
			$config['allowed_types'] = 'png|bmp|jpg|jpeg|gif';
			$config['max_size']	= 100; //MAX 100KB
			$config['encrypt_name'] = TRUE;
			$this->load->library('upload', $config);
			if ($room['name'] !== FALSE && $room['faculty_name'] !== FALSE && $roomid !== FALSE) {
				if($this->upload->do_upload('picupload'))
				{
					$uploadData = $this->upload->data();
					$roomData['picture_name'] = $uploadData['file_name'];
					echo json_encode($this->$rooms_model->editRoom($roomid, $roomData));
					//izbrisi ja prethodnata slika
				}
				else
				{
					echo json_encode($this->$rooms_model->editRoom($roomid, $roomData));
				}
			}
			echo json_encode(FALSE);
		}
		exit();
	}

	public function roomDetail() {/*pub*/
		if (($roomid = $this->input->post('id'))!==FALSE) {
			echo json_encode($this->rooms_model->roomDetails($roomid));
		} else {
			echo json_encode(FALSE);
		}
		exit();
	}
}
?>