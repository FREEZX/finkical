<?php
class Login extends CI_Controller {
	public function index($location = '') {
		$username = $this->input->post('username');

		$firstname = 'placeholder';
		$lastname = 'placeholder';
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');

		if ( !$username || !$firstname || !$lastname) {
			echo json_encode(false);
			exit();
		}
		$this->load->model('users_model');

		$userId = $this->users_model->isRegistered($username);
		if ( !$userId) {
			$userdata = array('username' => $username, 'firstname' => $firstname, 'lastname' => $lastname, 'user_type' => 'USER');
			$userId = $this->users_model->insertUser($userdata);
		}

		$userdata = $this->users_model->getUserData($userId);
		$userdata['loggedin'] = true;
		echo json_encode($userdata);

		if ($userdata) {
			$this->session->set_userdata($userdata);
		}
		$url = 'http://cal.finki.ukim.mk/' . $location;
		redirect($url, 'refresh');
	}

}
?>