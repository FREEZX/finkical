<?php
class AdminPanel extends CI_Controller {
    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('user_type') != 'ADMIN')
            redirect('http://cal.finki.ukim.mk/calendar', 'refresh');
        $this->load->model('users_model');
        $this->load->model('groups_model');
        $this->load->model('events_model');
    }
    
    public function changeUserPrivilege() {
        $utype = array("USER", "MODERATOR", "ADMIN");

        if (($userid = $this->input->post('userid')) && (in_array($type = $this->input->post('type'), $utype))) {
            $userdata = array('userid' => $userid, 'type' => $type);
            echo json_encode($this->users_model->editUser($userdata));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function eventsList() {
        $from = $this->input->post('from');
        $count = $this->input->post('count');
        $this->form_validation->set_rules('from', 'LimitationFrom', 'is_natural');
        $this->form_validation->set_rules('count', 'LimitationCount', 'is_natural_no_zero');
        if ($this->form_validation->run())
            echo json_encode($this->events_model->getAllEvents($from, $count));
        else
            echo json_encode($this->events_model->getAllEvents());
        exit();
    }

    public function usersList() {
        $utype = array("USER", "MODERATOR", "ADMIN");
        $type = $this->input->post('type');
        $from = $this->input->post('from');
        $count = $this->input->post('count');
        $this->form_validation->set_rules('from', 'LimitationFrom', 'is_natural');
        $this->form_validation->set_rules('count', 'LimitationCount', 'is_natural_no_zero');
        if ($this->form_validation->run()) {
            if (in_array($type, $utype)) {
                echo json_encode($this->users_model->getAllUsers($type, $from, $count));
            } else {
                echo json_encode($this->users_model->getAllUsers($from, $count));
            }
        } else {
            if (in_array($type, $utype)) {
                echo json_encode($this->users_model->getAllUsers($type));
            } else {
                echo json_encode($this->users_model->getAllUsers());
            }
        }
        exit();
    }

    public function insertGroup() {
        if ($this->input->post('name') !== FALSE && $this->input->post('name') && $this->input->post('description') !== FALSE && $this->input->post('userid') !== FALSE) {
            $groupData = array('name' => $this->input->post('name'), 'description' => $this->input->post('description'), 'owner' => $this->input->post('userid'));
            echo json_encode($this->groups_model->insertGroup($groupData));
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function removeGroup() {
        if ($groupids = $this->input->post('group_ids')) {
            echo $this->groups_model->deleteGroups($groupids);
        } else {
            echo json_encode(FALSE);
        }
        exit();
    }

    public function findUserByName() {
        $search = $this->input->post('term');
        echo json_encode($this->users_model->findUserByName($search));
        exit();
    }

}
?>
