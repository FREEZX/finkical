Finkical is a calendar application with many advanced features including groups, subscriptions and more!

### To install:

1. Drop the project into your www folder
2. Modify the database config file in application/config/database.php
3. Adjust the .htaccess file in the project root
4. Open '''[path]/install''' with a browser where [path] is the url to the project folder on your server, for example '''127.0.0.1/finkical/install'''
5. You're done.
If step 4 results in a 404 not found make sure step number 3 was done correctly.

Load the project root with your browser
Enjoy :)


### Tips on modifying:

