-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2013 at 03:09 PM
-- Server version: 5.5.31-0+wheezy1
-- PHP Version: 5.4.4-14+deb7u2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calendar`
--
CREATE DATABASE IF NOT EXISTS `calendar` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `calendar`;

-- --------------------------------------------------------

--
-- Table structure for table `000710_events`
--

CREATE TABLE IF NOT EXISTS `000710_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `009606_events`
--

CREATE TABLE IF NOT EXISTS `009606_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `12800_events`
--

CREATE TABLE IF NOT EXISTS `12800_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `13035_events`
--

CREATE TABLE IF NOT EXISTS `13035_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `13253_events`
--

CREATE TABLE IF NOT EXISTS `13253_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `13253_events`
--

INSERT INTO `13253_events` (`id`, `name`, `description`, `allday`, `start_time`, `end_time`, `created_on`) VALUES
('6f7b18c408d48481fc8c6d523df264624d055d49', 'Свадба', 'Ќе се женам', 1, '2012-12-31 08:00:00', '2013-01-01 08:00:00', '2012-10-26 10:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `13256_events`
--

CREATE TABLE IF NOT EXISTS `13256_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `13853_events`
--

CREATE TABLE IF NOT EXISTS `13853_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `14074_events`
--

CREATE TABLE IF NOT EXISTS `14074_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `017207_events`
--

CREATE TABLE IF NOT EXISTS `017207_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `017909_events`
--

CREATE TABLE IF NOT EXISTS `017909_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `022204_events`
--

CREATE TABLE IF NOT EXISTS `022204_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `023610_events`
--

CREATE TABLE IF NOT EXISTS `023610_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `040209_events`
--

CREATE TABLE IF NOT EXISTS `040209_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `040808_events`
--

CREATE TABLE IF NOT EXISTS `040808_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `094810_events`
--

CREATE TABLE IF NOT EXISTS `094810_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111005_events`
--

CREATE TABLE IF NOT EXISTS `111005_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111010_events`
--

CREATE TABLE IF NOT EXISTS `111010_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111023_events`
--

CREATE TABLE IF NOT EXISTS `111023_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111026_events`
--

CREATE TABLE IF NOT EXISTS `111026_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111027_events`
--

CREATE TABLE IF NOT EXISTS `111027_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111030_events`
--

CREATE TABLE IF NOT EXISTS `111030_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111035_events`
--

CREATE TABLE IF NOT EXISTS `111035_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111038_events`
--

CREATE TABLE IF NOT EXISTS `111038_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111040_events`
--

CREATE TABLE IF NOT EXISTS `111040_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111041_events`
--

CREATE TABLE IF NOT EXISTS `111041_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111042_events`
--

CREATE TABLE IF NOT EXISTS `111042_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111046_events`
--

CREATE TABLE IF NOT EXISTS `111046_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111051_events`
--

CREATE TABLE IF NOT EXISTS `111051_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111053_events`
--

CREATE TABLE IF NOT EXISTS `111053_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111058_events`
--

CREATE TABLE IF NOT EXISTS `111058_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111065_events`
--

CREATE TABLE IF NOT EXISTS `111065_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111066_events`
--

CREATE TABLE IF NOT EXISTS `111066_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111068_events`
--

CREATE TABLE IF NOT EXISTS `111068_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111069_events`
--

CREATE TABLE IF NOT EXISTS `111069_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111073_events`
--

CREATE TABLE IF NOT EXISTS `111073_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111081_events`
--

CREATE TABLE IF NOT EXISTS `111081_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111085_events`
--

CREATE TABLE IF NOT EXISTS `111085_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111087_events`
--

CREATE TABLE IF NOT EXISTS `111087_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111089_events`
--

CREATE TABLE IF NOT EXISTS `111089_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111091_events`
--

CREATE TABLE IF NOT EXISTS `111091_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111092_events`
--

CREATE TABLE IF NOT EXISTS `111092_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111096_events`
--

CREATE TABLE IF NOT EXISTS `111096_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111097_events`
--

CREATE TABLE IF NOT EXISTS `111097_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111098_events`
--

CREATE TABLE IF NOT EXISTS `111098_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111100_events`
--

CREATE TABLE IF NOT EXISTS `111100_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111101_events`
--

CREATE TABLE IF NOT EXISTS `111101_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111102_events`
--

CREATE TABLE IF NOT EXISTS `111102_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111105_events`
--

CREATE TABLE IF NOT EXISTS `111105_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111106_events`
--

CREATE TABLE IF NOT EXISTS `111106_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111108_events`
--

CREATE TABLE IF NOT EXISTS `111108_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111110_events`
--

CREATE TABLE IF NOT EXISTS `111110_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111113_events`
--

CREATE TABLE IF NOT EXISTS `111113_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111115_events`
--

CREATE TABLE IF NOT EXISTS `111115_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111117_events`
--

CREATE TABLE IF NOT EXISTS `111117_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111119_events`
--

CREATE TABLE IF NOT EXISTS `111119_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111121_events`
--

CREATE TABLE IF NOT EXISTS `111121_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111122_events`
--

CREATE TABLE IF NOT EXISTS `111122_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111126_events`
--

CREATE TABLE IF NOT EXISTS `111126_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111127_events`
--

CREATE TABLE IF NOT EXISTS `111127_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111129_events`
--

CREATE TABLE IF NOT EXISTS `111129_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111132_events`
--

CREATE TABLE IF NOT EXISTS `111132_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111135_events`
--

CREATE TABLE IF NOT EXISTS `111135_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111139_events`
--

CREATE TABLE IF NOT EXISTS `111139_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111149_events`
--

CREATE TABLE IF NOT EXISTS `111149_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111150_events`
--

CREATE TABLE IF NOT EXISTS `111150_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111152_events`
--

CREATE TABLE IF NOT EXISTS `111152_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111154_events`
--

CREATE TABLE IF NOT EXISTS `111154_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111155_events`
--

CREATE TABLE IF NOT EXISTS `111155_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111159_events`
--

CREATE TABLE IF NOT EXISTS `111159_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111179_events`
--

CREATE TABLE IF NOT EXISTS `111179_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111183_events`
--

CREATE TABLE IF NOT EXISTS `111183_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111191_events`
--

CREATE TABLE IF NOT EXISTS `111191_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111197_events`
--

CREATE TABLE IF NOT EXISTS `111197_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111201_events`
--

CREATE TABLE IF NOT EXISTS `111201_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111208_events`
--

CREATE TABLE IF NOT EXISTS `111208_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111211_events`
--

CREATE TABLE IF NOT EXISTS `111211_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111217_events`
--

CREATE TABLE IF NOT EXISTS `111217_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111221_events`
--

CREATE TABLE IF NOT EXISTS `111221_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111222_events`
--

CREATE TABLE IF NOT EXISTS `111222_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111227_events`
--

CREATE TABLE IF NOT EXISTS `111227_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111235_events`
--

CREATE TABLE IF NOT EXISTS `111235_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111236_events`
--

CREATE TABLE IF NOT EXISTS `111236_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111241_events`
--

CREATE TABLE IF NOT EXISTS `111241_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111247_events`
--

CREATE TABLE IF NOT EXISTS `111247_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `111251_events`
--

CREATE TABLE IF NOT EXISTS `111251_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112001_events`
--

CREATE TABLE IF NOT EXISTS `112001_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112003_events`
--

CREATE TABLE IF NOT EXISTS `112003_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112010_events`
--

CREATE TABLE IF NOT EXISTS `112010_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112021_events`
--

CREATE TABLE IF NOT EXISTS `112021_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `112021_events`
--

INSERT INTO `112021_events` (`id`, `name`, `description`, `allday`, `start_time`, `end_time`, `created_on`) VALUES
('d8e800199ed5144ad24a8978c8b52a8ace66e7e0', 'час - интернет технологии', 'час', 0, '2012-10-05 10:00:00', '2012-10-05 11:45:00', '2012-10-04 22:54:53'),
('a7a3dd0e5202eebf2dbceb88aebc170f1bc18069', 'test', 'test', 1, '2012-11-02 00:13:00', '2012-11-02 00:13:00', '2012-11-02 00:12:28'),
('96c7ac0708d291c448de75219e5467bc7d8001cf', 'it''s meeee', 'jadam kakao', 0, '2013-06-30 13:35:00', '2013-07-01 13:35:00', '2013-06-30 13:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `112028_events`
--

CREATE TABLE IF NOT EXISTS `112028_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112030_events`
--

CREATE TABLE IF NOT EXISTS `112030_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112033_events`
--

CREATE TABLE IF NOT EXISTS `112033_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112042_events`
--

CREATE TABLE IF NOT EXISTS `112042_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112044_events`
--

CREATE TABLE IF NOT EXISTS `112044_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112059_events`
--

CREATE TABLE IF NOT EXISTS `112059_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112062_events`
--

CREATE TABLE IF NOT EXISTS `112062_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112063_events`
--

CREATE TABLE IF NOT EXISTS `112063_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112066_events`
--

CREATE TABLE IF NOT EXISTS `112066_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112074_events`
--

CREATE TABLE IF NOT EXISTS `112074_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112092_events`
--

CREATE TABLE IF NOT EXISTS `112092_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112097_events`
--

CREATE TABLE IF NOT EXISTS `112097_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `112098_events`
--

CREATE TABLE IF NOT EXISTS `112098_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113002_events`
--

CREATE TABLE IF NOT EXISTS `113002_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113005_events`
--

CREATE TABLE IF NOT EXISTS `113005_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113010_events`
--

CREATE TABLE IF NOT EXISTS `113010_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113013_events`
--

CREATE TABLE IF NOT EXISTS `113013_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113014_events`
--

CREATE TABLE IF NOT EXISTS `113014_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113015_events`
--

CREATE TABLE IF NOT EXISTS `113015_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113016_events`
--

CREATE TABLE IF NOT EXISTS `113016_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113020_events`
--

CREATE TABLE IF NOT EXISTS `113020_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113026_events`
--

CREATE TABLE IF NOT EXISTS `113026_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113027_events`
--

CREATE TABLE IF NOT EXISTS `113027_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113031_events`
--

CREATE TABLE IF NOT EXISTS `113031_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113036_events`
--

CREATE TABLE IF NOT EXISTS `113036_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113037_events`
--

CREATE TABLE IF NOT EXISTS `113037_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113040_events`
--

CREATE TABLE IF NOT EXISTS `113040_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113043_events`
--

CREATE TABLE IF NOT EXISTS `113043_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113045_events`
--

CREATE TABLE IF NOT EXISTS `113045_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113051_events`
--

CREATE TABLE IF NOT EXISTS `113051_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113053_events`
--

CREATE TABLE IF NOT EXISTS `113053_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113062_events`
--

CREATE TABLE IF NOT EXISTS `113062_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113063_events`
--

CREATE TABLE IF NOT EXISTS `113063_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113070_events`
--

CREATE TABLE IF NOT EXISTS `113070_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113083_events`
--

CREATE TABLE IF NOT EXISTS `113083_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `113095_events`
--

CREATE TABLE IF NOT EXISTS `113095_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `114006_events`
--

CREATE TABLE IF NOT EXISTS `114006_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `115005_events`
--

CREATE TABLE IF NOT EXISTS `115005_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `115043_events`
--

CREATE TABLE IF NOT EXISTS `115043_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `115079_events`
--

CREATE TABLE IF NOT EXISTS `115079_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `116004_events`
--

CREATE TABLE IF NOT EXISTS `116004_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `116007_events`
--

CREATE TABLE IF NOT EXISTS `116007_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `116018_events`
--

CREATE TABLE IF NOT EXISTS `116018_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `116049_events`
--

CREATE TABLE IF NOT EXISTS `116049_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `117003_events`
--

CREATE TABLE IF NOT EXISTS `117003_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `117007_events`
--

CREATE TABLE IF NOT EXISTS `117007_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `117033_events`
--

CREATE TABLE IF NOT EXISTS `117033_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `119019_events`
--

CREATE TABLE IF NOT EXISTS `119019_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `119115_events`
--

CREATE TABLE IF NOT EXISTS `119115_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121009_events`
--

CREATE TABLE IF NOT EXISTS `121009_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121055_events`
--

CREATE TABLE IF NOT EXISTS `121055_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121060_events`
--

CREATE TABLE IF NOT EXISTS `121060_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121080_events`
--

CREATE TABLE IF NOT EXISTS `121080_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121123_events`
--

CREATE TABLE IF NOT EXISTS `121123_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121161_events`
--

CREATE TABLE IF NOT EXISTS `121161_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121213_events`
--

CREATE TABLE IF NOT EXISTS `121213_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `121226_events`
--

CREATE TABLE IF NOT EXISTS `121226_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `121226_events`
--

INSERT INTO `121226_events` (`id`, `name`, `description`, `allday`, `start_time`, `end_time`, `created_on`) VALUES
('a7a1121c5a55e2a79842519273e7fb4e49787abe', 'Проба', 'Проба', 1, '2012-10-13 03:18:00', '2012-10-13 08:06:00', '2012-10-12 23:23:06');

-- --------------------------------------------------------

--
-- Table structure for table `122107_events`
--

CREATE TABLE IF NOT EXISTS `122107_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('b062a8b1a7011f2dcd02848769a79427', '127.0.0.1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36', 1373288883, 'a:6:{s:2:"id";s:40:"d463b8031950dafade4f577ae4ff06afb4dfd4e7";s:8:"username";s:6:"112021";s:9:"firstname";s:10:"Владо";s:8:"lastname";s:22:"Величковски";s:9:"user_type";s:5:"ADMIN";s:8:"loggedin";b:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` tinytext COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `room` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allday` tinyint(2) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `group_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` enum('PROFESSIONAL','ACADEMIC','RECREATIVE','PRIVATE') COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'SR <=>Sport/Recreation',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` tinyint(2) unsigned DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `group_id` (`group_id`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `room`, `allday`, `start_time`, `end_time`, `group_id`, `category`, `created_on`, `active`) VALUES
('03f41f1611de5c23469b1cfbce4c60a3cb6c0450', 'Структури на податоци - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 14:00 часот.', '65584f1c062a0d25f505a9350fea025ddc1b8b60', 0, '2012-11-05 14:00:00', '2012-11-05 20:00:00', 'e3f9e8345442bf54e824cb36ba9a55df46a8d9c1', 'PROFESSIONAL', '2012-10-28 12:02:33', 1),
('04aacdb93f14101e0c3978c00b4d90402c3f0634', 'Податочни и компјутерски комуникации', 'Аудиториумски вежби по предметот, кај асс. Александра Каневче.', '77f1fad82077dfaa148f1b667f6301aa505423c7', 0, '2012-10-31 16:00:00', '2012-10-31 17:45:00', 'eb0fd5c33c4838cc102e24ac284ace997249d4f7', 'ACADEMIC', '2012-10-25 21:03:43', 1),
('08310d2c7ebf34c5a4ddd01cbd872fa8a4c429ad', 'Koмпјутерски мрежи', 'Аудиториумски вежби по предметот, кај асс. Сашо Ристов.', '95687c5c0f8b3c4cc2b04ca492d748c6a7bf971c', 0, '2012-11-07 16:00:00', '2012-11-07 17:45:00', '8ca52f2055c486db311f301d062dd7cc1bfcc54a', 'ACADEMIC', '2012-10-25 21:51:41', 1),
('1355d3693b95ba065f90bba2e50b1317ed57ade4', 'Апликациски програмски интерфејси', 'oogle, Facebook, Bing, Soundcloud, Youtube, World of Warcraft... што им е заедничко на сите овие апликации? И како да го употребите за да го подобрите сопствениот софтвер?   Дојдете на втората полу-месечна средба на Connect lab, и научете како да ги искористите можностите на најдобрите софтверски системи во вашиот нареден проект! Презентацијата ќе биде на тема Апликациски програмски интерфејс, а ќе имате можност да се запознаете и со програмскиот интерфејс на една од најпопуларните игри на денешницата, преку практичен пример изработен од член на Connect lab. ', NULL, 0, '2012-10-31 20:00:00', '2012-10-31 21:30:00', 'd4022da173f22c5dfd263279732c13c777c1306b', 'PROFESSIONAL', '2012-10-25 19:57:42', 1),
('174a2fce0b63ffb226f445085a88de74833f605d', 'Интерактивни апликации', 'Аудиториски вежби кај асс. Иван Китановски.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-10-29 18:00:00', '2012-10-29 18:45:00', '3a087759743798d212480fb56224a7ce5be0426c', 'ACADEMIC', '2012-10-25 20:45:35', 1),
('1fa17f12195d7f086ee11233dba90055df6b1a3d', 'Формални јазици и автомати', 'Аудиториумски вежби по предметот, кај асс. Миле Јованов.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-11-02 16:00:00', '2012-11-02 16:45:00', '58abead1bcb1a67f7725a4c7dd5d68eaabe69602', 'ACADEMIC', '2012-10-25 21:36:18', 1),
('245662c4a018a9f711810378dc092e8cb29a16fc', 'Линеарни трансформации', 'Предавања по предметот, кај доц. Весна Димитрова', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-10-25 14:00:00', '2012-10-25 16:45:00', 'c832c3613c16d1d0199e0bcc34a4e3ded8c97e8c', 'ACADEMIC', '2012-10-25 21:22:19', 1),
('278cb373c7cf36fb53ed7c24e85f59ba261deb0f', 'Менаџмент и маркетинг', 'Предавање по предметот кај проф. Димитар Трајанов.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-10-29 14:00:00', '2012-10-29 15:45:00', 'eb8a04711533ce9129aefa8a01e04e3a291b3623', 'ACADEMIC', '2012-10-25 20:41:05', 1),
('281c7bbd5a48a12361b4812270968d63f74a91ad', 'Дигитална електроника', 'Аудиториски вежби по предметот дигитална електроника, кај демонстратор Владимир Здравески.', NULL, 0, '2012-10-25 17:00:00', '2012-10-25 17:45:00', '593df1d15bf348454af641c520242109e190ef4a', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('28553c01ddca83330d17b27db5359fbb699b5080', 'Маркетинг и менаџмент', 'Аудиториумски вежби кај асс. Смиљка Јаневска - Саркањац.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-10-29 16:00:00', '2012-10-29 17:45:00', 'eb8a04711533ce9129aefa8a01e04e3a291b3623', 'ACADEMIC', '2012-10-25 20:42:14', 1),
('2be96f43b60afe214347ef7babacda5aaad5d6d2', 'Веројатност и статистика', 'Аудиториски вежби по Веројатност и статистика , кај асс. Александра Поповска Митровиќ', NULL, 0, '2012-10-26 14:00:00', '2012-10-26 15:45:00', '59fa4b932f5006d750ae113f3da107a3a1897b1e', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('34de0c754e6d3c022524e1a35f521153a1b3376c', 'Концепти за развој на софтвер - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 08:00 часот.', 'b2053ff8494fb3f9d383af6bf18d2008b0a29005', 0, '2012-11-03 08:00:00', '2012-11-03 20:00:00', 'ef97288072d35e6755dc4d415a14aec2ae7c86fd', 'ACADEMIC', '2012-10-28 11:50:32', 1),
('3621639d9353264668a53972cddf2b874bd4da44', 'Компјутерски мрежи', 'Предавања по предметот, кај проф, Марјан Гушев.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-11-06 12:00:00', '2012-11-06 13:45:00', '8ca52f2055c486db311f301d062dd7cc1bfcc54a', 'ACADEMIC', '2012-10-25 21:49:51', 1),
('365003cca9d6238901bf3005b1464aff56ff0236', 'Веројатност и статистика', 'Аудиториски вежби по Веројатност и статистика , кај асс. Билјана Тојтвскао', NULL, 0, '2012-10-26 14:00:00', '2012-10-26 15:45:00', '59fa4b932f5006d750ae113f3da107a3a1897b1e', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('401d0f6d3169d9f09c304ad74fb683c69a1458bf', 'Структури на податоци', 'Предавања по предметот, кај проф. Ана Богданова - Мадевска.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-11-06 10:00:00', '2012-11-06 11:45:00', 'e3f9e8345442bf54e824cb36ba9a55df46a8d9c1', 'ACADEMIC', '2012-10-25 21:41:52', 1),
('4107f82f890d0304a419c7ea01f0e58ba4c93275', 'Алгоритми и податочни структури', 'Предавања по предметот, кај доц. Анастас Мишев.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-10-30 10:00:00', '2012-10-30 11:45:00', '94622408f7dfe92bfa8f638b27819180cb4b63a1', 'ACADEMIC', '2012-10-25 20:52:31', 1),
('4237a9a3e678fe8cf81ca990d38986f10e130ef6', 'Алгоритми и податочни структури', 'Аудиториумски вежби по предметот кај Асс. Игор Кулев.', NULL, 0, '2012-10-26 18:00:00', '2012-10-26 19:45:00', '94f0fae975eb152434fbac496a3ea63de250a1f9', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('442bfd5be58546b7846cd8913f7ceddc62b982cd', 'Податочни и компјутерски комуникации', 'Аудиториумски вежби кај асс. Александра Богојеска', NULL, 0, '2012-10-26 18:00:00', '2012-10-26 19:45:00', 'ed016dd680ef96b63f824195b9c14468e6177a09', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('4d71e5b906732f020da2e14ff920c486f74a0f63', 'Веројатност и статистика - Прв парцијален испит', 'Распределба по простории ќе биде дополнително испратена од одговорните на предметот.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-11-03 18:00:00', '2012-11-03 20:00:00', '13a385c71c73d056ac529a2796f9b16c4c788379', 'ACADEMIC', '2012-10-28 22:15:33', 1),
('59130b62ed758aedd0a28a1c7d4ad1e585fb6a73', 'Структури на податоци', 'Аудиториумски вежби по предметот, кај асс. Магдалена Костова.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-11-09 15:00:00', '2012-11-09 17:45:00', 'e3f9e8345442bf54e824cb36ba9a55df46a8d9c1', 'ACADEMIC', '2012-10-25 21:44:38', 1),
('5a5d829f9c0e8ef6e29497215b823862f0a022db', 'Алгоритми и податочни структури', 'Аудиториумски вежби по предметот кај Асс. Игор Кулев.', NULL, 0, '2012-10-26 16:00:00', '2012-10-26 17:45:00', '94f0fae975eb152434fbac496a3ea63de250a1f9', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('68c07b21ef89839b6ad3a4bb4c51684c6cad699f', 'Интерактивни апликации', 'Предавања по предметот, кај доц. Ивица Димитровски.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-10-30 14:00:00', '2012-10-30 15:45:00', '3a087759743798d212480fb56224a7ce5be0426c', 'ACADEMIC', '2012-10-25 20:47:34', 1),
('7793f64452662c19db4b023214288dee6c5f9e62', 'Формални јазици и автомати', 'Предавања по предметот, кај доц. Марија Михова.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-10-30 08:00:00', '2012-10-30 09:45:00', '58abead1bcb1a67f7725a4c7dd5d68eaabe69602', 'ACADEMIC', '2012-10-25 21:34:17', 1),
('785f78e6bc4ed11892753d672188ef5373bc8d4f', 'Алгоритми и податочни структури', 'Аудиториумски вежби по предметот, кај асс.Магдалена Костоска. ', '95687c5c0f8b3c4cc2b04ca492d748c6a7bf971c', 0, '2012-10-31 18:00:00', '2012-10-31 19:45:00', '94622408f7dfe92bfa8f638b27819180cb4b63a1', 'ACADEMIC', '2012-10-25 20:54:41', 1),
('7962dc5b43091de2976bd7dcf33da78c25f2c47f', 'Веројатност и статистика', 'Аудиториумски вежби по предметот, кај асс. Наташа Илиоска.', '7c862adccb70934e34a61ae827e6ad160b7ed7d8', 0, '2012-10-29 14:00:00', '2012-10-29 15:45:00', '13a385c71c73d056ac529a2796f9b16c4c788379', 'ACADEMIC', '2012-10-25 21:25:24', 1),
('7effc58d56a7d1962a41f9c54bf1bbf4248643c0', 'Концепти за развој на софтвер - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 08:00 часот.', '7bb1020eef17156f2846a810b5b14de179309335', 0, '2012-11-03 08:00:00', '2012-11-03 20:00:00', 'ac6ab04af50ea6555775eb2a6dcfb95cac41e791', 'ACADEMIC', '2012-10-28 11:49:17', 1),
('814c9b00d0b8078318d4c27ed88a5ef7d11d66fd', 'Алгоритми и податочни структури - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 14:00 часот.', '7bb1020eef17156f2846a810b5b14de179309335', 0, '2012-11-05 14:00:00', '2012-11-05 20:00:00', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', 'PROFESSIONAL', '2012-10-28 11:59:30', 1),
('90f85d9e361fd8919fb1127e074933366c81e36a', 'Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распределни во термини и лаборатории. Првиот термин ќе започне во 08:00 часот.', 'e8a14b5547e4d72dd3ec7951d03c13976b61afa0', 0, '2012-11-03 08:00:00', '2012-11-03 20:00:00', '3706ede822e9f5b6531a47e5318c1ff75362d389', 'ACADEMIC', '2012-10-28 11:11:38', 1),
('94309a841848caef25ec96279c727a685b4b67af', 'Алгоритми и податочни структури - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 14:00 часот.', 'e8a14b5547e4d72dd3ec7951d03c13976b61afa0', 0, '2012-11-05 14:00:00', '2012-11-05 20:00:00', 'c5e3296e0993e2d2f0c2e9e80304a9cd1788f970', 'PROFESSIONAL', '2012-10-28 12:00:11', 1),
('9bedbc4fdfa3bbb35a085407fb5b1f0de34dd708', 'Податочни и компјутерски комуникации', 'Аудиториумски вежби по предметот, кај асс. Сашо Ристов.', '95687c5c0f8b3c4cc2b04ca492d748c6a7bf971c', 0, '2012-10-31 16:00:00', '2012-10-31 17:45:00', 'eb0fd5c33c4838cc102e24ac284ace997249d4f7', 'PROFESSIONAL', '2012-10-25 21:02:44', 1),
('a2dd22c99418d7d46e8e37ece5c5ae98e78de91c', 'Физика 1 - Прв парцијален испит', 'Испитот ќе се оддржи во просторија 223 во зградата на ФЕИТ и Машински.', '5b274867f4c69074bdffc08d1cda5f6b78ad950c', 0, '2012-11-03 15:00:00', '2012-11-03 18:00:00', '470b2cce66c840f0887d57b5fc06be9f598dc509', 'ACADEMIC', '2012-10-28 22:03:31', 1),
('a6097265c06c1e1429416ba3e2b2aca281fb679b', 'Алгоритми и податочни структури - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 14:00 часот.', 'b2053ff8494fb3f9d383af6bf18d2008b0a29005', 0, '2012-11-05 14:00:00', '2012-11-05 20:00:00', '94622408f7dfe92bfa8f638b27819180cb4b63a1', 'ACADEMIC', '2012-10-28 11:56:57', 1),
('abca311c95163aa445b2f2e67940bf0d2b68a7ef', 'Концепти за развој на софтвер - Прв парцијален испит ', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 08:00 часот.', '79f39d37bfc168102e96ed6c41ecbe315f599822', 0, '2012-11-03 08:00:00', '2012-11-03 20:00:00', '7075307e3e56a027fdbb688392c0b7ecd7907995', 'ACADEMIC', '2012-10-28 11:42:41', 1),
('ac8d20157e12406a084521f8add921ac405722ef', 'Податочни и компјутерски комуникации', 'Предавања од предметот, кај проф. Марјан Гушев.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-10-30 12:00:00', '2012-10-30 13:45:00', 'eb0fd5c33c4838cc102e24ac284ace997249d4f7', 'ACADEMIC', '2012-10-25 21:01:03', 1),
('b366176e2046b690634d8b9d7243e08177d6629b', 'Основи на електротехника 1 - Прв парцијален испит', 'Распределба по простории ќе биде дополнително испратена од одговорните на предметот.', '320a8c2e624ba490415225fb37df4da244f86b4a', 0, '2012-10-28 14:00:00', '2012-10-28 17:00:00', '3d80a5bb44028a99c7689450034d492784f1f539', 'ACADEMIC', '2012-10-28 22:21:57', 1),
('b42a88b38ed5e5679d70cf98d8e316ac04b83b2b', 'Веројатност и статистика - Прв парцијален испит', 'Распределба по простории ќе биде дополнително испратена од одговорните на предметот.', '35337745520ced632cfc4ebabd32b144f902b924', 0, '2012-11-03 18:00:00', '2012-11-03 20:00:00', '59fa4b932f5006d750ae113f3da107a3a1897b1e', 'ACADEMIC', '2012-10-28 22:16:26', 1),
('b8d948a6e5ee6a135f5e3be6f5d05e5d456755fc', 'Математика - Прв парцијален испит', 'Дополнително ќе бидат објавени од одговорните на предметот списоци кои студенти во која просторија ќе полагаат.', 'b0335281b4d8193cddebb54260aecb0a4d3afaf6', 0, '2012-11-03 15:00:00', '2012-11-03 18:00:00', 'e3666806d715ea52488f110183e02789b0ee850b', 'ACADEMIC', '2012-10-28 22:09:56', 1),
('bf1096d6c0a6bc7be67f8823140f4f134f49a7e5', 'Линеарни трансформации / Калкулус 3', 'Аудиториумски вежби по предметот, кај асс. Весна Димитриевска Ристовска.', '1e77a06e4db02d43cbccafc24c085f98073f0090', 0, '2012-10-25 17:00:00', '2012-10-25 19:45:00', 'c832c3613c16d1d0199e0bcc34a4e3ded8c97e8c', 'ACADEMIC', '2012-10-25 21:23:23', 1),
('c6775f1574043ab28a663214a6b68f3d28f9add5', 'ИТ системи за учење', 'Предавање по предметот кај проф. Гоце Арменски', '4a207077a92f569691c76246b3da00e22abc9135', 0, '2012-10-25 17:00:00', '2012-10-25 18:45:00', '8e155d7b23db7596968794faaea106a6563afd43', 'ACADEMIC', '2012-10-25 20:34:36', 1),
('c86f1d19a34aa225d4034dcb507bf280bc20df26', 'Менаџмент во ИКТ', 'Предавања по предметот, кај доц. Љупчо Антовски.', '327ecc350865a73ddadf9c0df4932418263b4645', 0, '2012-11-08 14:00:00', '2012-11-08 16:45:00', 'fa4ea1de6fd777c8ecb2eb8279033f4c13deccee', 'ACADEMIC', '2012-10-25 21:53:31', 1),
('c8ea324cfd6aff3e5449deccf8d81f6c118e25ec', 'Интернет', 'Предавања по предметот, кај доц. Гоце Арменски.', '7c862adccb70934e34a61ae827e6ad160b7ed7d8', 0, '2012-10-29 16:00:00', '2012-10-29 17:45:00', '359fd4feb5dc02a6ab8be9d2b8e0ff88c8440de5', 'ACADEMIC', '2012-10-25 21:30:09', 1),
('c99db7bcdd83fc2d5c15d11f20a0bd2cb7730d85', 'Алгоритми и податочни структури - Прв парцијален испит', 'Дополнително ќе бидат објавени списоци во кои ќе бидете распоредени во различни термини и лаборатории. Првиот термин започнува во 14:00 часот.', 'cec2a4c929faeef6ed83db2c3a61ae85eb2e5c13', 0, '2012-11-05 14:00:00', '2012-11-05 20:00:00', '94f0fae975eb152434fbac496a3ea63de250a1f9', 'ACADEMIC', '2012-10-28 11:58:17', 1),
('caadc50e64b575056d85e135be2197e21b239492', 'Логички кола и дискретни автомати - Прв парцијален испит', 'Распределба по простории ќе биде дополнително испратена од одговорните на предметот.', 'ae7985daa77b62167dfb74b57ee4536e7a605ce5', 0, '2012-11-03 18:00:00', '2012-11-03 20:00:00', '05d3beefa1e1ed803af535b079c329e340f08dad', 'ACADEMIC', '2012-10-28 22:18:57', 1),
('cbf0922b192f686cbd2c6a1a62e617b195993877', 'Интернет', 'Аудиториумски вежби по предметот, кај асс. Сашо Ристов.', '7c862adccb70934e34a61ae827e6ad160b7ed7d8', 0, '2012-10-29 18:00:00', '2012-10-29 18:45:00', '359fd4feb5dc02a6ab8be9d2b8e0ff88c8440de5', 'ACADEMIC', '2012-10-25 21:31:50', 1),
('d0f5fd51a2434bce92449cf16d051c1cde8a09c5', 'Дигитална електроника', 'Предавање по предметот дигитална електроника, кај доц. Ласко Баснарков.', NULL, 0, '2012-10-25 15:00:00', '2012-10-25 16:45:00', '593df1d15bf348454af641c520242109e190ef4a', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('e1a1110a4b544b56d98d0b6318fd872cebe977a9', 'Средба на Connect', 'Теми: node.js и програма за менторство', 'b0335281b4d8193cddebb54260aecb0a4d3afaf6', 0, '2012-12-12 20:00:00', '2012-12-12 23:00:00', 'd4022da173f22c5dfd263279732c13c777c1306b', 'ACADEMIC', '2012-12-10 23:52:22', 1),
('e81a2ca04fccd4a633af2315ffaf3e00e28dcf41', 'Предавање', 'Предавање по предметот Интернет технологии', NULL, 0, '2012-10-26 12:00:00', '2012-10-26 13:45:00', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('e88aee0dd359329f9e37d34430e671794d0cdeae', 'Податочни и компјутерски комуникации', 'Аудиториумски вежби кај асс. Александра Каневче', NULL, 0, '2012-10-26 18:00:00', '2012-10-26 19:45:00', 'ed016dd680ef96b63f824195b9c14468e6177a09', 'ACADEMIC', '2012-10-25 19:57:42', 1),
('f46d8adb86dc9389ae2d3c0c258bc9a161d1d238', 'ИТ системи за учење', 'Аудиториски вежби кај асс. Кристина Копиќ', '4a207077a92f569691c76246b3da00e22abc9135', 0, '2012-10-25 19:00:00', '2012-10-25 19:45:00', '8e155d7b23db7596968794faaea106a6563afd43', 'ACADEMIC', '2012-10-25 20:35:34', 1),
('febfd52a87da84f3cc81799663d6915bda1b0eaa', 'Системска анализа и дизајн - Прв парцијален испит', 'Распределба по простории ќе биде дополнително испратена од одговорните на предметот.', '320a8c2e624ba490415225fb37df4da244f86b4a', 0, '2012-11-05 14:00:00', '2012-11-05 17:00:00', 'e7fea359e3d191a2efb70e50cd27307266e368ac', 'ACADEMIC', '2012-10-28 22:23:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` tinytext COLLATE utf8_unicode_ci,
  `type` enum('PRIVATE','PUBLIC') COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `type`, `active`, `created_on`) VALUES
('02fc1f622c09432b403e25fbf7205fa62d62b93a', 'Основи на софтверско инженерство - Група 2', 'Група за студентите кај проф. Ѓорѓи Маџаров', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('03643977582ec50835f4eca3bf62115913ea4566', 'Интернет Програмирање', 'За студентите на ИКИ/Инфо', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('052fba55892b001f65e27fc8d737a2ecdb1a50bc', 'Е-бизнис', 'Група за студентите кај проф. Љупчо Антовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('05d3beefa1e1ed803af535b079c329e340f08dad', 'Логички кола и дискретни автомати', 'За студентите на ИКИ/ИНФО', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('0d8843693140e70e5c40d5deb90278427a49f573', 'Компјутерска Електроника 2', 'Оваа група е креирана за студентите кои што го слушаат предметот Компјутерска Електроника 2 во 3-та година, насока ИКИ, кај проф. Караџинов Љ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('0e002345875e442c2ae55819db40da7ae60e851c', 'Калкулус 1 - Група 4', 'За студентите од прва година Група 4', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('13a385c71c73d056ac529a2796f9b16c4c788379', 'Веројатност и статистика (АСИ)', 'Оваа група е за студентите од студиската програма Академски студии по информатика што го слушаат предметот кај проф. Жанета Попеска. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('13ee486da0d9050a264dbfd8d247ee9f3304e299', 'Мрежни и  дистрибуирани  оперативни системи', 'Група за студентите кај проф. Боро Јакимовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('1420982a020b51e9c357459e43cb8204a56daedb', 'Студентски парламент - ФИНКИ', 'Оваа група му припаѓа на студентскиот парламент.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('15eb378c0cb454a2f0af505eddc971a201c6092e', 'Компјутерски апликации', 'За прва година Група 5', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('165019b8ba1a4d9f472159882cc3cb318fde1371', 'Компјутерски Мрежи', 'Оваа група е креирана за студентите кои што го слушаат предметот Компјутерски Мрежи во 3-та година, насока ИКИ, кај проф. Мишковски Игор', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('1843d3bfa404bf5a31dae0f671e5133ec8fe986d', 'Мрежен софтвер', 'За студентите од студиската прогама Информатика и компјутерско инженерство, како и сите што го слушаат овој предмет кај проф. Аристотел Те', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('1c44cef69a275dfcde04370c2476f7578c0c6373', 'Вовед во интернет (ИНФО)', 'За студентите во прва година студии на студиската програма Професионални студии по информатика.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('1fbc377a7abff8ec52d06d2c692fef460921ce48', 'Проектирање на компјутерски мрежи', 'Група за студентите кај проф. Дејан Спасов', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('201bfeb5c7b57533c9732cadb914a891817c70e2', 'Веројатност и статистика - Група 2', 'Оваа група е креирана за студентите кои што го слушаат предметот во прва група, кај проф. Верица Бакева.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('2080ce939540151134808e76ca462180ae3528a9', 'Интелигентни системи', 'Група за студентите кај проф. Ана Мадевска Богданова', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('223a458181c510935fa058a5bb979930585f363e', 'Дискретна математика 1 - Група 2', 'Група за студентите кај проф. Весна Димитриова', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('22a89d1dd8c39971f3b304d72395a87071ed95fc', 'Вовед во роботика', 'Оваа група е креирана за студентите кои што го слушаат предметот Вовед во роботика во 3-та година, насока ИКИ, кај проф. Кулаков Андреа', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('23723901e0d72bda5b7f35874b08704051aef5aa', 'Професионални вештини - Група 5', 'За студентите од прва година  од Група 5', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('27575bd5e4115fcc6299b8000b6f05a0c3fad025', 'Професионални вештини - Група 1', 'За сите студенти во прва група во прва година на студии.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('2996e0578bfbc0acea45f0579c6081943d3f34db', 'Напредно програмирање ', 'Оваа група е наменета за студентите кои што го слушаат предметот Напредно програмирање, кај доц. Ѓорѓи Маџаров.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('29e95be8980ac760c41886393303f0bc846574ae', 'ФИНКИ', 'Официјална група на ФИНКИ', 'PUBLIC', 1, '0000-00-00 00:00:00'),
('2c79bcfce163357cf0441d03b5da3da37f1a4129', 'Компјутеризирани мерења', ' Оваа група е креирана за студентите кои што го слушаат предметот Компјутеризирани мерења во 3-та година, насока ИКИ, кај проф. Арсов Љупчо.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('2facee9f25cf8a26ada71bdf249dcdf48bda7260', 'Калкулус 1 - Група 2', 'Група за студентите кај проф. Верица Бакева', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('304652e4d5fe23fbc63cd76bb39a8ee5c34b08fa', 'Линеарни трансформации (ИКИ)', 'За студентите на ИКИ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('30f2164978614df18033bbbb1118b27679cac60d', 'Вештачка интелигенција', 'За студентите од студиската програма Информатика и компјутерско инженерство, како и сите кои го слушаат овој предмет кај проф. Андреа Кула', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3320bd150eded48eca91edc7150b1e3062253e83', 'Компјутерска Графика', ' Оваа група е креирана за студентите кои што го слушаат предметот Компјутерска Графика во 3-та година, насока ИКИ, кај проф. Михајлов Драган', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('34035e78f3e8dced30cdd20255dba764bfa21768', 'Бази на податоци', ' Оваа група е креирана за студентите кои што го слушаат предметот Бази на податоци во 3-та година, насока ИКИ, кај проф. Калајџиски Слободан', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('357e5ae6f979cae2256d2093012cf2ba6cc6a248', 'Анализа и логички дизајн на информациски системи', 'Група за студентите кај Маргита Кон Поповска', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('359fd4feb5dc02a6ab8be9d2b8e0ff88c8440de5', 'Интернет', 'Оваа група е за студентите од студиската програма Академски студии по информатика, што го слушаат предметот кај проф. Гоце Арменски. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3706ede822e9f5b6531a47e5318c1ff75362d389', 'Концепти за развој на софтвер - Група 1', 'За сите студенти во прва група во прва година на студии.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3a087759743798d212480fb56224a7ce5be0426c', 'Интерактивни апликации (ПЕТ)', 'Оваа група е за студентите од насоката Примена на Е технологии, кои го слушаат овој предмет кај доц. Ивица Димитровски. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3a4e3b4f27a3858566c1288d7bcf35c85f08dec3', 'Web дизајн (ИНФО/ТК/КСИА)', 'Група за студентите кај проф. Иван Чорбев', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3cd8f45460ce5de8815b8980d39d08ab14a8eff8', 'Основи на софтверско инженерство - Група 5', 'За студентите од прва година Група 5  ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3d80a5bb44028a99c7689450034d492784f1f539', 'Основи на електротехника 1', ' Оваа група е креирана за студентите кои што го слушаат предметот Основи на електротехника 1 во 1-ва година, насока ИКИ, кај проф. Митрески Ко', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('3dc6345bfbd69a34061abd92cc1304fc65a03a54', 'Системски софтвер', 'Група за студентите кај проф. Боро Јакимовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('44385f355086b35bc8ca3edca98f6cc8013154cf', 'Дискретна математика 1 -Група 4', 'За студентите од прва година Група 4', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('470b2cce66c840f0887d57b5fc06be9f598dc509', 'Физика 1', ' Оваа група е креирана за студентите кои што го слушаат предметот Физика 1 во 1-ва година, насока ИКИ, кај проф. Баснарков Ласко', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('4751597eb5d291a06a45c7e13639ad3b2bf1efbd', 'Мрежни оперативни системи', ' Оваа група е креирана за студентите кои што го слушаат предметот Мрежни оперативни системи во 3-та година, насока ИКИ, кај проф. Филипоска С', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('4ce973ec1830bc06e742a5bc2f2c9af6029e71f6', 'Основи на софтверско инженерство -Група 4', 'За студентите од прва година Група 4', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('525b952f85c09718c3f5004bbe1a3b57d6ffa0b4', 'Дистрибуирани компјутерски системи', 'За студентите од студиската програма Информатика и компјутерско инженерство, како и сите кои го слушаат овој предмет кај доц. Соња Гиевска.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('58abead1bcb1a67f7725a4c7dd5d68eaabe69602', 'Формални јазици и автомати', 'Оваа група е за студентите од студиската програма Академски студии по информатика, кои го слушаат предметот кај доц. Марија Михова.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('593df1d15bf348454af641c520242109e190ef4a', 'Дигитална електроника', 'Оваа група е наменета за студентите кои што го слушаат предметот кај доц. Ласко Баснарков.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('59fa4b932f5006d750ae113f3da107a3a1897b1e', 'Веројатност и статистика - Група 1', 'Оваа група е креирана за студентите кои што го слушаат предметот во прва група, кај проф. Жанета Попеска.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('5ead2e0673b4731f299181eea45d74aa94119bde', 'Компјутерски Мрежи', 'Оваа група е креирана за студентите кои што го слушаат предметот Компјутерски Мрежи во 3-та година, насока ИКИ, кај проф. Филипоска Соња', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('5f2f39846017256fd0007376559580e326676995', 'Основи на програмирање', ' Оваа група е креирана за студентите кои што го слушаат предметот Основи на програмирање во 1-ва година, насока АСИ, кај проф. Мадевска Богда', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('63d1ecd97e982f7034658d454094181e564f1f87', 'Математика 1 (ИКИ)', ' Оваа група е креирана за студентите кои што го слушаат предметот Математика 1 (ИКИ) во 1-ва година, насока ИКИ, кај проф. Бакева Верица', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('6701ebefedd76c9bb08410194ceb79d2112c5859', 'Вовед во ИТ', ' Оваа група е креирана за студентите кои што го слушаат предметот Вовед во ИТ во 1-ва година, насока АСИ, кај проф. Спасов Дејан', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('6870a59e9d9610d6fdc6a5dec85a428b3d69ae1d', 'Компјутерски апликации', ' Оваа група е креирана за студентите кои што го слушаат предметот Компјутерски апликации во 1-ва година, насока ИТ, кај проф.Здравкова Катер', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('6e755cd9d51c641bc3a70f5eafae4f11d77870e6', 'Професионални вештини - Група 4', 'За студентите од прва година Група 4', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('7075307e3e56a027fdbb688392c0b7ecd7907995', 'Концепти за развој на софтвер - Група 2', 'Група за студентите кај проф. Ѓорѓи Маџаров', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('71c8b4f5239876cb45f99cdb3347d4bde3b708b7', 'Калкулус 1 - Група 1', 'За сите студенти од прва година, во прва група на студии.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('7716aefea4b8ba3185ce3f8d5dbb2b0df4f3a92b', 'Веројатност и статистика', 'Оваа група е креирана за студентите кои што го слушаат предметот Веројатност и статистика во 3-та година, насока ИКИ, кај проф. Бучковска Ане', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('778698d529b999a99176055b67d03396b5544d2d', 'Дискретна математика 1 - Група 1', 'За сите студенти во прва група во прва година на студии.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('7950273a1a31e249353f0acfacb049f9b2958d8b', 'Вовед во интернет', 'За прва година Група 5', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('7da6848f162e50254bfb05adb4095e5a2bbd367b', 'Структурирано програмирање (ИНФО)', 'За студентите во прва година на студиската програма  Професионални студии по информатика.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('7ffff05529e9f8fb3c3fdbd6ae90751712bce999', 'Анализа на софтверски потреби', 'Група за студентите кај проф. Љупчо Антовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('819a5d1c930c8275ef0f190ea0d562287acc7e3c', 'Интернет технологии', 'Оваа група е наменета за студентите кои што го слушаат предметот кај доц. Гоце Арменски.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('826ce4499f7cde5b7db3993b18c1bb3e71d755cd', 'Структурирано програмирање', ' Оваа група е креирана за студентите кои што го слушаат предметот Структурирано Програмирање во 1-ва година, насока ИКИ, кај проф. Димитровс', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('829d6507d6f317c4f61bb089b947462fbf858c40', 'Логичко и функционално програмирање', 'За студентите на Инфо', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('83e4de87350d5f36b7fa0777df71f8fb479b0d5b', 'Менаџмент во ИКТ', 'Група за студентите кај проф. Љупчо Антовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8819a91502420d5a18c35c10ae473ee987673b2d', 'Машинска интелегенција и учење', ' Оваа група е креирана за студентите кои што го слушаат предметот Машинска интелегенција и учење во 3-та година, насока ИКИ, кај проф. Коцаре', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('88bc47740875ad81a177e1c871804433312e1741', 'Компјутерски Мрежи (ИНФО)', 'Група за студентите кај проф. Соња Филиповска', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8b3e20ba35026b7f1d02460df7d44691a1e51de3', 'Мрежно Програмирање', ' Оваа група е креирана за студентите кои што го слушаат предметот Мрежно Програмирање во 3-та година, насока ИКИ, кај проф. Трајанов Димитар', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8c81741e683199f6cec356efa3ad1d4ad3759947', 'Вградливи компјутерски системи', 'За студентите од студиската програма Информатика и компјутерско инженерство, како и они што го слушаат предметот кај проф. Аристотел Тенто', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8ca52f2055c486db311f301d062dd7cc1bfcc54a', 'Компјутерски мрежи', 'За сите кои го слушаат предметот кај проф. Марјан Гушев.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8e155d7b23db7596968794faaea106a6563afd43', 'ИТ системи за учење', 'Оваа група е за студентите кои што го слушаат предметот ИТ системи за учење кај доц. Гоце Арменски. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('8f0025d62eb146b1e352ed818df99f75cf4e4eb8', 'Интеракција Човек - Компјутер', 'Група за студентите кај проф. Невена Ацковска', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('90c2f3fa932db68a4c30d42856560b70045b5a55', 'Бази на податоци', 'Оваа група е креирана за студентите кои што го слушаат предметот Бази на податоци во 3-та година, насока ИКИ, кај проф. Давчев Данчо', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('94622408f7dfe92bfa8f638b27819180cb4b63a1', 'Алгоритми и податочни структури (ПЕТ)', 'Оваа група е за студентите од насоката Примена на Е технологии, кои го слушаат предметот кај доц. Анастас Мишев.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('94f0fae975eb152434fbac496a3ea63de250a1f9', 'Алгоритми и податочни структури - Група 1', 'Оваа група е креирана за студентите кои што го слушаат предметот Алгоритми и податочни структури во група 1, кај проф. Владимир Трајковиќ.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('95dae04506f9df1d5edfc39d17fbbb24800e4c82', 'Основи на електротехника 1 ', ' Оваа група е креирана за студентите кои што го слушаат предметот Основи на електротехника 1 во 1-ва година, насока ИКИ, кај проф. Наумоски Ан', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('96eefa8a9846769aacbf053bd0abdb2ab59e946b', 'Бази на податоци 2', 'Група за студентите кај проф. Горан Велинов', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('98d08bebcfebcf97544532a71e305cc3fc5bd1e9', 'Мултимедијални технологии', 'Група за студентите кај проф. Марија Михова', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('9b382eaa6c9b732724322254b2d3c1afcc067c40', 'Објектно ориентирани системи', 'За студентите на ИКИ/ИНФО', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('9c4a44655ff6d18754c6440653d678f12766fe8c', 'Математика 1 (ИНФО) / Калкулус 1 (ИТ)', 'За студентите во прва година студии, на студиските програми Професионални студии по информатика и Професионални студии по информатички те', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', 'Алгоритми и податочни структури - Група 2', 'Група за студентите кај проф. Игор Трајковски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('a0aaaccd015167b5f89ca7916a85a73efc517f73', 'Интелегентни кориснички интерфејси', 'За студентите од студиската програма Информатика и компјутерско инженерство, како и сите кои го слушаат овој предмет кај доц. Соња Гиевска.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('a3d52896940676658cfc7ba042b69790a61228ea', 'Микропроцесори и микроконтролери', 'Група за студентите кај проф. Невена Ацковска', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('a46e05f866918aca0ecebb6230ea0cf38270fdae', 'EESTEC', 'EESTEC LC Skopje.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ab19919f7a34107e7cfd016277b0a3cdadaea07a', 'ИТ системи за учење', 'Група за студентите од група 3 кои слушаат ИТ системи за учење кај проф. д-р. Гоце Арменски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ac6ab04af50ea6555775eb2a6dcfb95cac41e791', 'Концепти за развој на софтвер - Група 4', 'За студентите од прва година Група 4', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ad30e44f0f5d2024881d5da817c07377cebf81e8', 'Софтверско Инженерство', 'Оваа група е креирана за студентите кои што го слушаат предметот Софтверско Инженерство во 3-та година, насока ИКИ, кај проф. Ѓорѓевиќ Дејан.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('b38e484f50d29239c24e496d951d40ce83ad07ec', 'Современи компјутерски системи', 'Група за студентите кај проф. Анастас Мишев', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('b62242f4751b0a27a015e40ddc0339e4a6d54ffd', 'Податочни и компјутерски комуникации - Група 2', 'Група за студентите кај проф. Дејан Спасов', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('b7f9a73f5227a7f95e9e454bd86e58db864401ea', 'Експертни Системи', 'Група за студентите кај проф. Владимир Трајковиќ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ba70226fe7a9e4784ee21291da98da6e02d53f45', 'Дискретна математика 1 - Група 5', 'За студентите од прва година Група 5', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('bcb88244037e30e1290a5fe0e93d66b9da2f28ad', 'Професионална етика', 'Група за студентите кај проф. Катерина Здравкова', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('bcc80b1c07f6857db9fd01d21be685222e8b5e46', 'Современи процесорски архитектури', 'За студентите од студиската програма Информатика и компјутерско инженерство што го слушаат овој предмет кај проф. Аристотел Тентов.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('bd2db4ae547ac95b08eae1b9be352d41b91207e5', 'Дискретни структури 1 ', ' Оваа група е креирана за студентите кои што го слушаат предметот Дискретни структури 1 во 1-ва година, насока ИТ, кај проф. Попеска Жанета', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('be1254b2c334d1208d124ba5c52cd0421e37d398', 'Web дизајн (ИКИ)', 'За студентите од студиската програма Информатика и компјутерско инженерство, како и сите кои го слушаат овој предмет кај проф. Драган Миха', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('c48a7061008f761e3ab019e370a44ab42a2ffd6c', 'Интерактивни апликации - Група 1/2', 'Оваа група е наменета за студентите што го слушаат предметот во прва група, кај проф. Сузана Лошковска. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('c5e3296e0993e2d2f0c2e9e80304a9cd1788f970', 'Алгоритми и структури на податоци(ИКИ/ИНФО)', 'За студентите на ИКИ/ИНФО', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('c832c3613c16d1d0199e0bcc34a4e3ded8c97e8c', 'Калкулус 3 / Линеарни трансформации', 'Оваа група е за студентите од насоките АСИ и ИКИ, кои што го слушаат предметот кај доц. Весна Димитрова.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('c9f5871be721b644869fbc8eb1c35b008a27cd09', 'Софтверско инженерство(ИНФО)', 'Група за студентите кај проф. Дејан Ѓорѓевиќ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('cd75a63d508bd762872df9bc9f1537e4cc22c446', 'Oснови на програмирање (ИТ)', 'Оваа група е за студентите во прва година на студиската програма ИТ.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d1fe3d0cb7bf6adceba2be6a03eea6a4be161104', 'Веројатност и статистика', 'Оваа група е креирана за студентите кои што го слушаат предметот Веројатност и статистика во 3-та година, насока ИКИ, кај проф. Санева Катери', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d2415e934b6faf265449f83c5af9d9570a28e641', 'Интернет технологии (ИТ)', 'Група за студентите кај проф. Марјан Гушев', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d4022da173f22c5dfd263279732c13c777c1306b', 'Connect lab', 'Запознајте се со најновите технологии и практики во софтверското инженерство! Стекнете искуство на реални и предизвикувачки проекти!', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d6cebae7f429e674a8726de7e3398be335e2d197', 'Стохаички процеси', 'Група за студентите што го слушаат овој предмет', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d6f3e7f90a62baf2a10d73e42a705664ff581f13', 'Статичка обработка на податоци', 'За студентите од студиската програма Информатика и компјутерско инженерство.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('d91b0e01a4e6088e679ca47f9facc3c77b17c7b5', 'Конструкција на софтвер', 'Група за студентите кај проф. Боро Јакимовски', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('da1fd31ffa916428dc472b809c0a09493c59c918', 'Калкулус 1 ', ' Оваа група е креирана за студентите кои што го слушаат предметот Калкулус 1 во 1-ва година, насока АСИ, кај проф. Марковски Смиле', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('da7890b253593bcfafe8ea039810e5519fef8275', 'Програмски практикум', 'За сите кои го слушаат овој предмет кај доц. Иван Чорбев.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e1a5ef2d65e96da439c8bcb6cc9d4b356f8b2170', 'Професионални Вештини - Група 2', 'Група за студентите кај проф. Розита Петринска Лабудовиќ и проф. Катерина Видова', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e3666806d715ea52488f110183e02789b0ee850b', 'Математика (ИНФО)/ Математика 1 (ИТ)', ' Оваа група е креирана за студентите кои што го слушаат предметот Математика 1 во 1-ва година, насока ИТ, кај проф. Кусакатов Ванчо', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e3f9e8345442bf54e824cb36ba9a55df46a8d9c1', 'Структури на податоци', 'Оваа група е за студентите од студиската програма Академски студии по информатика, кои го слушаат предметот кај проф. Ана Богданова- Мадевс', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e7fea359e3d191a2efb70e50cd27307266e368ac', 'Системска анализа и дизајн', 'За студентите во прва година студии на студиската програма Професионални студии по информатика.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e907bc6620764b00c16f967c80bdb6ac10e58c16', 'Менаџмент и маркетинг', 'Оваа група е наменета за студентите кои што го слушаат предметот во прва група, кај проф. Димитар Трајанов.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('e9a295605eba92bb7f00bc02cd73828f7e6b4f72', 'Теорија на програмирање', 'Оваа група е креирана за студентите кои што го слушаат предметот Теорија на програмирање, во прва група.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ead34fcbe00cde7a99ef84abff9543fc22744e99', 'Логички кола и дискретни автомати', 'За студентите на ИКИ/ИНФО', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('eb0fd5c33c4838cc102e24ac284ace997249d4f7', 'Податочни и компјутерски комуникации (ПЕТ)', 'Оваа група е за студентите од студиската програма Примена на Е технологии кои го слушаат предметот кај  проф. Марјан Гушев. ', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('eb8a04711533ce9129aefa8a01e04e3a291b3623', 'Менаџмент и маркетинг (ПЕТ)', 'Оваа група е за студентите од насоката Примена на Е технологии, кои го слушаат предметот кај проф. Димитар Трајанов.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ed016dd680ef96b63f824195b9c14468e6177a09', 'Податочни и компјутерски комуникации - Група 1', 'Оваа група е креирана за студентите кои што го слушаат предметот Податочни и компјутерски комуникации во прва група, кај доц. Игор Мишковск', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('ef97288072d35e6755dc4d415a14aec2ae7c86fd', 'Концепти за развој на софтвер - Група 5', 'За студентите од прва година Група 5', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('f248972b58eb9f2cc5f6344246aa66347bce2327', 'Податочни и компјутерски комуникации (АСИ)', 'Група за студентите кај проф. Марјан Гушев', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('f2cb578c46cdc89c1cc34dfff2c32f755ec6c0e2', 'Компјутерска Графика (ИНФО)', 'Група за студентите кај проф. Драган Михајлов', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('f456982b189ab6fbfed3126f3658c0b793e23c93', 'Англиски јазик (ИНФО)', 'За студентите во прва година, на студиската програма Професионални студии по информатикка.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('f9e840f0be4df4fab5cd1dcc6728e4f548e3ec10', 'Основи на софтверско инженерство', 'За сите студенти во прва група во прва година на студии.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('fa4ea1de6fd777c8ecb2eb8279033f4c13deccee', 'Менаџмент во ИКТ', 'За оние кои го слушаат предметот кај доц. Љупчо Антовски.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('fc1688cd004c21c7e533d4a3f5e9bdfd9700cef8', 'Бази на податоци', 'Група за студентите кај проф. Маргарита Кон Поповска', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('fe10b0bca70113f2e303410dd28a233aa5d28ad6', 'Web базирани системи', 'За студентите кои го слушаат овој предмет кај проф. Димитар Трајанов.', 'PRIVATE', 1, '0000-00-00 00:00:00'),
('fe158ef63da69d0e60b0e0fcf77f0bb364167262', 'Дискретни структури 1 ', ' Оваа група е креирана за студентите кои што го слушаат предметот Дискретни структури во 1-ва година, насока АСИ, кај проф. Михова Марија', 'PRIVATE', 1, '0000-00-00 00:00:00');

--
-- Triggers `groups`
--
DROP TRIGGER IF EXISTS `AddNewPublicGroupToUsers`;
DELIMITER //
CREATE TRIGGER `AddNewPublicGroupToUsers` AFTER INSERT ON `groups`
 FOR EACH ROW BEGIN
IF (NEW.type = 'PUBLIC') THEN
INSERT INTO users_groups_rel (id,group_id,user_id) SELECT SHA1(CONCAT(rand()*999,current_date(),current_time(),'s3cr37',rand()*999)) AS id, NEW.id AS group_id, u.id AS user_id FROM users AS u;
END IF;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `CleanOnGroupDelete`;
DELIMITER //
CREATE TRIGGER `CleanOnGroupDelete` BEFORE DELETE ON `groups`
 FOR EACH ROW BEGIN
DELETE FROM users_groups_rel WHERE group_id = OLD.id;
DELETE FROM group_requests WHERE group_id = OLD.id;
DELETE FROM moderators_groups_rel WHERE group_id = OLD.id;
DELETE FROM events WHERE group_id = OLD.id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `group_requests`
--

CREATE TABLE IF NOT EXISTS `group_requests` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `group_requests_user_id` (`user_id`),
  KEY `group_requests_group_id` (`group_id`),
  KEY `created_on` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `group_requests`
--

INSERT INTO `group_requests` (`id`, `user_id`, `group_id`, `created_on`) VALUES
('01e2f8ec53ce15e56e03a905bf95d48d9b1fa740', '99cbadfecd6508126d9b67aed7cd3a571fa7f0db', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('040c4d1ffbeca700dcc2b74fce28bd7773b7665d', 'abcce4ad1f371b751de6acc8c71a5053241dbdec', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('04cc451dd399999e2be8d59947ece28277ef1e39', 'f04f1a3705fd10cf3a78f16a9958718a5f13404f', 'd6cebae7f429e674a8726de7e3398be335e2d197', '0000-00-00 00:00:00'),
('1c17ba2aeb58b61a9575460dccdeb6c9d97eef23', '434af05134fefede190baad82d5d20d2e4139a9b', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('2b931a53dfe71e197d47b288fd08d50e9cc5a69f', '2f49f5406c36337f0f3fa7343e3bf82fef1129b9', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('31532fc03dd2db87021ddff05548dd3c29834316', '7f16719ab33716a9a412699f4304d03f4420cc03', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('38de2b3412023a0188017bc32218e00c06616327', '87833dd3fe42d950312048a247abee56c512206a', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('3a0651efdb8a5e284b5c13965a1270528f99d078', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('3ece1ee0733471fdeae2e4bb4332b989fee95744', '34f9c80cb9012fb85ea164e6402bb4b79fb206a9', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('5b385cb49dc6875b1bd6c6f363e1edfd881b5e41', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('6dbafbfcaf4acd8fb2dd2ba2026641f3e39498d7', 'b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('7645aefcb2fdbd3455f7a13fb8d2efcdb66ef560', '67b254e35503df2475eb744a4c7e5f98d8c761f4', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('7b4698f70329f14e2899bd811647de7bd3302de1', '46acd41fed173c3802e87ec23907ef878e506435', 'c5e3296e0993e2d2f0c2e9e80304a9cd1788f970', '0000-00-00 00:00:00'),
('7d1bb1cd2cd404d8eab2d307499f9cda4e7bb420', '67b254e35503df2475eb744a4c7e5f98d8c761f4', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('98ab6829792b2f1b23ea969a18d6939378112ee5', '4bf0af1d6b07d27d15d63975461fa10bae141f2d', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('991bb610ab95680ec6281f92f8012d6cb7b04eda', '203684fea75da2b934935d0bcdcd943bda4e815e', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('9ca3f8029a28e1d62b4bc4cf135bcc5814cf3876', '5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('b30327442a64d93f7c9a20eeb252d29e0b73cf94', '297948d12687b2cfef2f1cb61b15c788431a5759', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('b581be1e3ac3c1290db60573333d1569f6d4c44b', 'c1b01a4133fb1272944aaf7904233757e71c1e0f', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('cdbfac73b294dff213b06ff4e374069de02269cb', '1c40df7d68a0067a5ce823550bd84a9b90c7dcbc', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('cf1a5954832ec81546b599269f7be14b65df5e5f', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '2013-07-01 22:44:31'),
('d3e007d030a6d61c0aee83ca9f65b31f3526ffd7', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', '201bfeb5c7b57533c9732cadb914a891817c70e2', '0000-00-00 00:00:00'),
('d4f775d07c53930a97c9cfc2738ba2fe20d3825e', 'aaca762861b15094109fd3abe8246aceaf15feed', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('d74845d18ec02ee82335f86c6c625d0761957b75', '5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('d74fec484b654390b2988ccaf98a5a6c92276402', '95e0bd59c78e6a773e87738f10cdd01956321d56', 'a46e05f866918aca0ecebb6230ea0cf38270fdae', '0000-00-00 00:00:00'),
('dd45db8a5984635cb8f69dee0dbcdf712a09eaf8', '0fb59db5e8ea83ce86c1ed3a140ac24362f12999', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('e2a40601dfa6891851aeb79cfd3c57a06ec6c71b', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('eb1a637b385f029f29e047b0f76c11ab1e40c96f', 'b1f197edf63f3b29243cd4a6e555f5c6a8c13330', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('efa7a93ea0303ab4c62ac87b8d0c06d4e24c4272', 'ca8f687f06392db3ca37c04aa5054424c99f434c', 'fe10b0bca70113f2e303410dd28a233aa5d28ad6', '0000-00-00 00:00:00'),
('f13067b9072e5c2087d138d1f21118393f6adcf9', '67b254e35503df2475eb744a4c7e5f98d8c761f4', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kiril_events`
--

CREATE TABLE IF NOT EXISTS `kiril_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logger`
--

CREATE TABLE IF NOT EXISTS `logger` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `current_url` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parm` longtext COLLATE utf8_unicode_ci NOT NULL,
  `get_parm` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `created_on` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `milos.jovanovik_events`
--

CREATE TABLE IF NOT EXISTS `milos.jovanovik_events` (
  `id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `allday` tinyint(4) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `moderators_groups_rel`
--

CREATE TABLE IF NOT EXISTS `moderators_groups_rel` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `moderator_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `created_on` (`created_on`),
  KEY `moderator_id` (`moderator_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `moderators_groups_rel`
--
DROP TRIGGER IF EXISTS `CleanOnSetGroupModerator`;
DELIMITER //
CREATE TRIGGER `CleanOnSetGroupModerator` AFTER INSERT ON `moderators_groups_rel`
 FOR EACH ROW BEGIN
DELETE FROM group_requests WHERE group_id = NEW.group_id AND user_id = NEW.moderator_id;
DELETE FROM users_groups_rel WHERE group_id = NEW.group_id AND user_id = NEW.moderator_id;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reporter` text COLLATE utf8_unicode_ci NOT NULL,
  `to` text COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `reporter`, `to`, `subject`, `body`, `created_on`) VALUES
('a489c9eaf58cf7fdce891992d99c90d403dae27b', '[112021] Владо Величковски', 'connect@finki.ukim.mk', 'Пријава на грешка во ФИНКИкал', 'asdasd', '2012-11-01 22:19:13');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `picture_path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faculty_name` text COLLATE utf8_unicode_ci,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `description`, `picture_path`, `faculty_name`, `created_on`) VALUES
('1e77a06e4db02d43cbccafc24c085f98073f0090', 'Б2', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('2314a97195f0290d0901ce1ff3250789d2753b74', '203', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('28d1776842d6bd9910b359529d659fa6ea8b1a1c', 'И12', 'Предавална', NULL, 'Природно - математички факултет', '0000-00-00 00:00:00'),
('320a8c2e624ba490415225fb37df4da244f86b4a', '203', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('327ecc350865a73ddadf9c0df4932418263b4645', '115', 'Читална / Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('3296618a68d33e0e6f1e01c28d9467021c0c98e5', '204', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('35337745520ced632cfc4ebabd32b144f902b924', 'Б3', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('3dfaa1f47905213ef51f8cd5fc15ae9eea748e32', '114', 'Предавална / Читална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('4a207077a92f569691c76246b3da00e22abc9135', '216', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('58d3ab548721784ad465a3dadad894e16fb34df5', '204', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('5b274867f4c69074bdffc08d1cda5f6b78ad950c', 'П7', 'Предавална', NULL, 'Природно - математички факултет', '0000-00-00 00:00:00'),
('65584f1c062a0d25f505a9350fea025ddc1b8b60', '118', 'Компјутерска училница / лабораторија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('77e6d6b4bfda11577d538c99c352318e45071062', 'Лабораторија за мултимедијални апликации', 'Лабораторија за мултимедијални апликации, Деканат', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('77f1fad82077dfaa148f1b667f6301aa505423c7', '124', 'Предавална', NULL, 'Машински факултет', '0000-00-00 00:00:00'),
('79f39d37bfc168102e96ed6c41ecbe315f599822', '200', 'Компјутерска училница / лабораторија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('7bb1020eef17156f2846a810b5b14de179309335', 'Лабораторија - подрумска', 'Компјутерска училница / Лабораторија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('7c862adccb70934e34a61ae827e6ad160b7ed7d8', '302', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('95687c5c0f8b3c4cc2b04ca492d748c6a7bf971c', '123', 'Предавална', NULL, 'Машински факултет', '0000-00-00 00:00:00'),
('ae7985daa77b62167dfb74b57ee4536e7a605ce5', '315', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('b0335281b4d8193cddebb54260aecb0a4d3afaf6', '117', 'Предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('b2053ff8494fb3f9d383af6bf18d2008b0a29005', '215', 'Компјутерска училница / лабораторија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('c4642c9c76b9162050764e6cd6da94fc9eeaab6f', 'Компјутерски центар', 'Лабораторија за пресметки со високи перформанси', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('cec2a4c929faeef6ed83db2c3a61ae85eb2e5c13', '200а', 'Професорска канцеларија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('e8a14b5547e4d72dd3ec7951d03c13976b61afa0', 'NULL', 'Голема соба', NULL, 'ФИНКИ', '0000-00-00 00:00:00'),
('ebf075bf02652d7d0747894e85b395986d5c2266', 'И19', 'Предавална', NULL, 'Природно - математички факултет', '0000-00-00 00:00:00'),
('edef0fd5f2a5b4826671a61c26f21b06ff597407', 'Б1 ', 'Нокиа / СМАРТ лабораторија', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00'),
('eee6c8b69cb6fbc56ba3f4d770b546c1c650d082', '116', 'Читална / предавална', NULL, 'Технолошко - металуршки факултет', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_type` enum('USER','MODERATOR','ADMIN') COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `user_type`, `created_on`) VALUES
('0038bff4093c70a357cd73fcd032d41ad8c0daf5', '117003', '0', 'placeholder', 'USER', '2013-07-01 00:51:00'),
('0076b06e109251a1fb424b94d34bfc7a31703d9b', '111121', '111121', '111121', 'USER', '0000-00-00 00:00:00'),
('0177fa261ab830a8cd033134bcf0a5e189ad6c95', '111098', '111098', '111098', 'USER', '0000-00-00 00:00:00'),
('06e02b06a00423d924699e7d64ea5450e23921ec', '111073', '111073', '111073', 'USER', '0000-00-00 00:00:00'),
('07a54e90f790ae4f8717cbebf4680c22e3135d7d', '111201', '111201', '111201', 'USER', '0000-00-00 00:00:00'),
('082a42970550e73de6eae81a0115721670fa4ae0', '112063', '112063', '112063', 'USER', '0000-00-00 00:00:00'),
('098db512c2fcf89eef0c22c5d1b87959eb722853', '13035', '13035', '13035', 'USER', '0000-00-00 00:00:00'),
('09ef51e57e1eff67dc3163a2a8707a841d6779a9', '111208', '111208', '111208', 'USER', '0000-00-00 00:00:00'),
('0c4c7e6553226ed3a9a60690bfd8dbd79eabddd5', '113031', '113031', '113031', 'USER', '0000-00-00 00:00:00'),
('0e1efd3a02e2eff69c60a7a149e21faed105b20e', '111132', '111132', '111132', 'USER', '0000-00-00 00:00:00'),
('0fb59db5e8ea83ce86c1ed3a140ac24362f12999', '121060', 'placeholder', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('1113458a8fbd03a81c20e2eef4e83350f14c8f53', '113051', '113051', '113051', 'USER', '0000-00-00 00:00:00'),
('122f232bc3480c24a9b67bc72e5c1cc372abe43d', '111135', '111135', '111135', 'USER', '0000-00-00 00:00:00'),
('1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', '111129', '111129', '111129', 'USER', '0000-00-00 00:00:00'),
('18ccb3f307380a58c43e20f6ccd9476e123059c4', '040808', '040808', '040808', 'USER', '0000-00-00 00:00:00'),
('1c40df7d68a0067a5ce823550bd84a9b90c7dcbc', '111101', '111101', '111101', 'USER', '0000-00-00 00:00:00'),
('203684fea75da2b934935d0bcdcd943bda4e815e', '111235', '111235', '111235', 'USER', '0000-00-00 00:00:00'),
('20b5995a0114ff7efe1f6f784ac61b32368daf5d', '111087', '111087', '111087', 'USER', '0000-00-00 00:00:00'),
('218cd822da03fed93624815f5e1c549b2a02fce2', '111115', '111115', '111115', 'USER', '0000-00-00 00:00:00'),
('24553211159cb3a67d5e5b691a1260ab4caf4fd6', '13853', '13853', '13853', 'USER', '0000-00-00 00:00:00'),
('256a33f6bb2c251ee381fe3ae70e9c7e10c6b773', '115079', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('26b6699fa21f4609a3f9d4a91e0e25d25cad31ff', '113062', '113062', '113062', 'USER', '0000-00-00 00:00:00'),
('26e290271c3b1dfbcc3a8051357be6cc4a68b370', '111150', '111150', '111150', 'USER', '0000-00-00 00:00:00'),
('297948d12687b2cfef2f1cb61b15c788431a5759', '111005', '111005', '111005', 'USER', '0000-00-00 00:00:00'),
('2de2783cbe1d1b7ca7a075c69efab6e201023f5b', '111126', '111126', '111126', 'USER', '0000-00-00 00:00:00'),
('2f49f5406c36337f0f3fa7343e3bf82fef1129b9', '111227', 'Кристијан', 'Трајковски', 'ADMIN', '0000-00-00 00:00:00'),
('334160b7e08d0a76b4bf5e30c7672abdb487d028', '117007', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('34f9c80cb9012fb85ea164e6402bb4b79fb206a9', '121055', 'placeholder', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('35947f9a84d372086e1079a0bf7c04df339cd752', '111058', '111058', '111058', 'USER', '0000-00-00 00:00:00'),
('3667c1803108ae08523745a76409eeee6999974e', '111108', '111108', '111108', 'USER', '0000-00-00 00:00:00'),
('37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', '112003', '112003', '112003', 'USER', '0000-00-00 00:00:00'),
('3f2035b21d62474c798f3af7aa737a67515eede6', '113063', '113063', '113063', 'USER', '0000-00-00 00:00:00'),
('3f8af0fdd862b2ba0f9a92cea9daae3e40f0e9aa', '13256', '13256', '13256', 'USER', '0000-00-00 00:00:00'),
('41e67fda8bf2f27fb26540893fb2f87dcfb7ea6f', '111183', '111183', '111183', 'USER', '0000-00-00 00:00:00'),
('434af05134fefede190baad82d5d20d2e4139a9b', '122107', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('442fc82c703944a48c5df57c53ee76a80214ca26', '111236', '111236', '111236', 'USER', '0000-00-00 00:00:00'),
('44c4a4d11f7f7687034ec99aae6680476823c28f', '112044', '112044', '112044', 'USER', '0000-00-00 00:00:00'),
('46acd41fed173c3802e87ec23907ef878e506435', '115005', '115005', '115005', 'USER', '0000-00-00 00:00:00'),
('488510d7fd80f5c6bf8a0016a72024296b730237', '111241', '111241', '111241', 'USER', '0000-00-00 00:00:00'),
('4bf0af1d6b07d27d15d63975461fa10bae141f2d', '111127', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('4ddd34d60fbc26f130590a9e88a695a37d04c69c', '111105', '111105', '111105', 'USER', '0000-00-00 00:00:00'),
('4e20d3f9f8bf0c6327a00481fc3ebe8e248f0f42', '13253', '13253', '13253', 'USER', '0000-00-00 00:00:00'),
('4e210efb9fe432fa5431f7f7c098bb9c4418fa53', '117033', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('52dfe68781c69d7d2289e0f7ab9ce52c58893027', '111046', '111046', '111046', 'USER', '0000-00-00 00:00:00'),
('556b2020f49f56befbccc5cddea91c75611732e5', '112030', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('56c664f556139646009c5085902d864d0aec7489', '121161', '121161', '121161', 'USER', '0000-00-00 00:00:00'),
('582b2b6bfb89ae7f042937f3f5229502edae9b4f', '017909', '017909', '017909', 'USER', '0000-00-00 00:00:00'),
('591a650d7a2fb61f59542dbf3988cf5e070f07d1', '112059', '112059', '112059', 'USER', '0000-00-00 00:00:00'),
('5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', '111159', '111159', '111159', 'USER', '0000-00-00 00:00:00'),
('5ec7758c30c69894e63e1226c4ffd316b0deb6c3', '111122', '111122', '111122', 'USER', '0000-00-00 00:00:00'),
('6206c28d5dd38fac93fd162727adce2643f35fb5', '111066', '111066', '111066', 'USER', '0000-00-00 00:00:00'),
('63972591a378a6838fcf1d2985f8073751cb0339', '111065', '111065', '111065', 'USER', '0000-00-00 00:00:00'),
('67b254e35503df2475eb744a4c7e5f98d8c761f4', '111042', '111042', '111042', 'USER', '0000-00-00 00:00:00'),
('694c19780c680d1924a4995c4dcc77d372fd4286', '111119', '111119', '111119', 'USER', '0000-00-00 00:00:00'),
('69c5694f83d228949215da67bec4aaa4dbf20305', '111053', '111053', '111053', 'USER', '0000-00-00 00:00:00'),
('6abd238ad90db1262ed7af63e41c4137f53d5a82', '111139', '111139', '111139', 'USER', '0000-00-00 00:00:00'),
('6cbbc361a109eeeb0e84c41fa90422ec65e78ead', '113095', '113095', '113095', 'USER', '0000-00-00 00:00:00'),
('6de1bc0cef4b3ef9cef51e8bcc70ff5f952b7712', '111085', 'placeholder', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('74594700b181117efc5ea1d0d694284d8e780fea', '111113', '111113', '111113', 'USER', '0000-00-00 00:00:00'),
('786e153e0309b35da63ef455df309f84efe0135b', '113083', '113083', '113083', 'USER', '0000-00-00 00:00:00'),
('79b47d3e24d3731a27114143ca47572cbe72ef89', '116018', '116018', '116018', 'USER', '0000-00-00 00:00:00'),
('79ffc61107fa0b544f970805129dff3a990348ab', '113070', '113070', '113070', 'USER', '0000-00-00 00:00:00'),
('7af2386d611b99381c8414f62f16cd0e631ad5d5', '113053', '113053', '113053', 'USER', '0000-00-00 00:00:00'),
('7c89a21c6e3a4d558fa8ce068a68976179aae95b', '113013', '113013', '113013', 'USER', '0000-00-00 00:00:00'),
('7f16719ab33716a9a412699f4304d03f4420cc03', '111154', '111154', '111154', 'USER', '0000-00-00 00:00:00'),
('80b5157219d10709e6fab882477e4f10232e2c3c', '116049', '116049', '116049', 'USER', '0000-00-00 00:00:00'),
('811ae807cd749d57fc57e160a4f150c432082bb3', '112028', '112028', '112028', 'USER', '0000-00-00 00:00:00'),
('838c670e6ad8b3074d4fc35322bdb5deaecc07b7', '111191', '111191', '111191', 'USER', '0000-00-00 00:00:00'),
('84e63835eac4dd6aacbb432ca1818bb434d8ac97', '111068', '111068', '111068', 'USER', '0000-00-00 00:00:00'),
('851d94a798715ecb78b7df6e8e16933a1f9b0217', '113020', '113020', '113020', 'USER', '0000-00-00 00:00:00'),
('855214b20e5e451e60cc0b99facdbd6cf054c057', '111197', '111197', '111197', 'USER', '0000-00-00 00:00:00'),
('857e7ac33eeb54414cf847203bb72381da668651', '121213', '121213', '121213', 'USER', '0000-00-00 00:00:00'),
('875abd46a00746aca64282a01733a7ac799cd0e8', '113040', '113040', '113040', 'USER', '0000-00-00 00:00:00'),
('87833dd3fe42d950312048a247abee56c512206a', '111221', 'Христијан', 'Тодоровски', 'USER', '2013-07-01 23:54:26'),
('8b22aa1ec0421f8cd5c7deb15b7a66a874f843e0', '112074', '112074', '112074', 'USER', '0000-00-00 00:00:00'),
('8b3562b7c72507351952d5fb69642043bfb017d8', '111100', '111100', '111100', 'USER', '0000-00-00 00:00:00'),
('8c57536789ab46301206b1f9c3c24fcce824cf07', '023610', '023610', '023610', 'USER', '0000-00-00 00:00:00'),
('95e0bd59c78e6a773e87738f10cdd01956321d56', '111106', '111106', '111106', 'USER', '0000-00-00 00:00:00'),
('95fc87913b9f609f2d2a72c23da0f46a2c396909', '111091', '111091', '111091', 'USER', '0000-00-00 00:00:00'),
('96df91e3b82c16b7c6dc546afbe75975fbfd4e47', '009606', '009606', '009606', 'USER', '0000-00-00 00:00:00'),
('99cbadfecd6508126d9b67aed7cd3a571fa7f0db', '116007', '116007', '116007', 'USER', '0000-00-00 00:00:00'),
('9a4879b7f8dff6f51b73b56ed3b442b5f9c9060c', '113014', '113014', '113014', 'USER', '0000-00-00 00:00:00'),
('9a9cdb3e8d0e9474867fab7b3307a4a42a9adeb6', '121080', '121080', '121080', 'USER', '0000-00-00 00:00:00'),
('9a9ffe5a30758cc818c69101ee6736183fdc9c31', '111247', '111247', '111247', 'USER', '0000-00-00 00:00:00'),
('9be32d97036c4bd7b33488d0538816e8e2111221', '112042', '112042', '112042', 'USER', '0000-00-00 00:00:00'),
('9cadb6676e0fbe42f38ffe35544bfb68e1545db3', '113027', '113027', '113027', 'USER', '0000-00-00 00:00:00'),
('a030f2dd2275da01941d67008fff368410ce5adc', '113036', '113036', '113036', 'USER', '0000-00-00 00:00:00'),
('a152b68ecebc4932be4601e5134efe804edb4900', '111097', '111097', '111097', 'USER', '0000-00-00 00:00:00'),
('a3366bf67af458b23a059b4c64620afd5ef57059', '113015', '113015', '113015', 'USER', '0000-00-00 00:00:00'),
('a48943e486eeecc5dacb8e55a623ee7e0dc351f7', '121009', '121009', '121009', 'USER', '0000-00-00 00:00:00'),
('a5140634241c8cb30c813b6c6616bf459e77c658', '111035', '111035', '111035', 'USER', '0000-00-00 00:00:00'),
('aaca762861b15094109fd3abe8246aceaf15feed', '121123', '121123', '121123', 'USER', '0000-00-00 00:00:00'),
('abcce4ad1f371b751de6acc8c71a5053241dbdec', '111217', '111217', '111217', 'USER', '0000-00-00 00:00:00'),
('ae86f25902cca5e83a6568de251430b9f972c693', '111117', '111117', '111117', 'USER', '0000-00-00 00:00:00'),
('afd889796bb809d3328a5314409bbf9bdfc5ee33', '111010', '111010', '111010', 'USER', '0000-00-00 00:00:00'),
('afde714ca232c50a47e68469938d120b1c7a9710', '111152', '111152', '111152', 'USER', '0000-00-00 00:00:00'),
('afdf7c5ceb82bc59e6d0b2e55397620887703afc', '115043', '115043', '115043', 'USER', '0000-00-00 00:00:00'),
('b097be1d4ce228d16f898b8bbdf448485a9694f7', '112010', '112010', '112010', 'USER', '0000-00-00 00:00:00'),
('b1f197edf63f3b29243cd4a6e555f5c6a8c13330', '111023', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('b28d3bb7d7182b9cd713bab2c991169bac78a13b', '112098', '112098', '112098', 'USER', '0000-00-00 00:00:00'),
('b42fecdedd5bfaeefdb5d9741c761fb2a8d59916', 'kiril', 'kiril', 'kiril', 'USER', '0000-00-00 00:00:00'),
('b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', '111149', '111149', '111149', 'USER', '0000-00-00 00:00:00'),
('b77e9151bdb749c12d6e309835772f1ed898c341', '111081', '111081', '111081', 'USER', '0000-00-00 00:00:00'),
('bbf458a9e2ac0b49835a622f6fc4208d0151f621', '111251', '111251', '111251', 'USER', '0000-00-00 00:00:00'),
('c1b01a4133fb1272944aaf7904233757e71c1e0f', '111222', '111222', '111222', 'USER', '0000-00-00 00:00:00'),
('c574df4ab917e71dee722e1c3d5963307be1ad28', '113002', '113002', '113002', 'USER', '0000-00-00 00:00:00'),
('c74efa4b4bfafb662a373d360dfefa3a3e10f33b', '119115', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('c83612ead69fb09a166954909e89a1492465407d', '111030', '111030', '111030', 'USER', '0000-00-00 00:00:00'),
('ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', '111069', '111069', '111069', 'USER', '0000-00-00 00:00:00'),
('ca8f687f06392db3ca37c04aa5054424c99f434c', 'milos.jovanovik', '0', 'placeholder', 'ADMIN', '0000-00-00 00:00:00'),
('cbe7d4a4921c8b4081b73c0579234e54e08bb7f2', '119019', '119019', '119019', 'USER', '0000-00-00 00:00:00'),
('cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', '111110', '111110', '111110', 'USER', '0000-00-00 00:00:00'),
('ccde072bb13495de3da51216138469ab53dacd7a', '113016', '113016', '113016', 'USER', '0000-00-00 00:00:00'),
('cd3e8bce1f8b5fb55f24b7d1c68dc0ca58dd8eb3', '094810', 'Дарко', 'Митовски', 'MODERATOR', '0000-00-00 00:00:00'),
('cd65d2be40926a71b15df420496309791552b061', '111040', '111040', '111040', 'USER', '0000-00-00 00:00:00'),
('cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', '111179', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('d359ff9cfa7c0416a09b65e5476206267f3d4334', '113043', '113043', '113043', 'USER', '0000-00-00 00:00:00'),
('d463b8031950dafade4f577ae4ff06afb4dfd4e7', '112021', 'Владо', 'Величковски', 'ADMIN', '0000-00-00 00:00:00'),
('d4f56feb5bbfabcce4dc320b1c3c9ce5691f7723', '121226', '121226', '121226', 'USER', '0000-00-00 00:00:00'),
('d661ab82373fd8212a2b7db4bb8f8cb4afeba1dc', '022204', 'Цветан', 'Џикоски', 'MODERATOR', '0000-00-00 00:00:00'),
('d9a3dafc885d633069774083ea3546dabccf1fc0', '112033', '112033', '112033', 'USER', '0000-00-00 00:00:00'),
('dbc7ff3fb1910023b034c55b012168b36ad18a6f', '113045', '113045', '113045', 'USER', '0000-00-00 00:00:00'),
('dc81c63924583724ead007b9333fd3673d5284f5', '14074', '14074', '14074', 'USER', '0000-00-00 00:00:00'),
('de2804005363c30567b708bb7161befcac12e920', '111211', '111211', '111211', 'USER', '0000-00-00 00:00:00'),
('de37878d9b465e71f98dd0afebfdb6eb0434fb39', '111041', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('de528d3b0696706e44f31ea35bcfc795f9be9ea9', '111096', '111096', '111096', 'USER', '0000-00-00 00:00:00'),
('dfa8c0f7224598a5b5755d47575dea5ad5126cd3', '017207', '017207', '017207', 'USER', '0000-00-00 00:00:00'),
('e033912e12e262323237f27c03c5e69c49857bb7', '111092', '111092', '111092', 'USER', '0000-00-00 00:00:00'),
('e123c8b0862cb9b327edd4d7d4405f303f573864', '111038', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('e220833d35c0b1a59f7d259aa7f873da7875523a', '111155', '111155', '111155', 'USER', '0000-00-00 00:00:00'),
('e2c482a187edbb53722db44fb915a5f5f28d4b18', '111051', '111051', '111051', 'MODERATOR', '0000-00-00 00:00:00'),
('e3222c0f6cb1d59d6f89a2e3c7c1498d843384f5', '000710', '000710', '000710', 'USER', '0000-00-00 00:00:00'),
('e32d77636dc245a11ec090467d10bf413b69aff8', '111102', '111102', '111102', 'USER', '0000-00-00 00:00:00'),
('e36eca4399ccd5d1176b637848f9076f2c6dfdc9', '12800', '12800', '12800', 'USER', '0000-00-00 00:00:00'),
('e3c1aa812ce52d2fe42b8d7b0609dd07bf7c4205', '040209', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('e4c88451ebecb9af671e7567a9091608d42d92de', '112097', '112097', '112097', 'USER', '0000-00-00 00:00:00'),
('e6ec1757798c724652e81d024082dc8395166aca', '112062', '112062', '112062', 'USER', '0000-00-00 00:00:00'),
('e8356e2cffeda6bc053691c7f416b63d8d5943e1', '112001', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('eab361df7a1860b4ea71d6467b35d3001a1426e2', '113005', '113005', '113005', 'USER', '0000-00-00 00:00:00'),
('ec5d89e3ecc41caea00fc833d19e834372622d23', '111089', '0', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('eca542f2d538bf2e75a202b66e3fe78480174011', '112066', '112066', '112066', 'USER', '0000-00-00 00:00:00'),
('edccbb2901568bfc7132b4029965821d62acc781', '114006', 'Митре', 'Анѓелкоски', 'ADMIN', '0000-00-00 00:00:00'),
('ee6a3b99eef3714dd4f9646f68ba5fe3233f7857', '116004', '116004', '116004', 'USER', '0000-00-00 00:00:00'),
('f04f1a3705fd10cf3a78f16a9958718a5f13404f', '111027', 'Дино', 'Бојаџиевски', 'MODERATOR', '0000-00-00 00:00:00'),
('f061b3ff92d01f3336eee702e1e883fc3a280595', '113026', '113026', '113026', 'USER', '0000-00-00 00:00:00'),
('f238fe75d61543163ac415c2463f18449524dc84', '112092', 'placeholder', 'placeholder', 'USER', '0000-00-00 00:00:00'),
('f2edf7c2a41fa0f2a731663811baaa9cfa77d175', '111026', '111026', '111026', 'USER', '0000-00-00 00:00:00'),
('f40105ee8842db467b79106505b226c438e5baab', '113037', '113037', '113037', 'USER', '0000-00-00 00:00:00'),
('f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', '113010', '113010', '113010', 'USER', '0000-00-00 00:00:00');

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `AddNewUserToPublicGroup`;
DELIMITER //
CREATE TRIGGER `AddNewUserToPublicGroup` AFTER INSERT ON `users`
 FOR EACH ROW BEGIN

INSERT INTO users_groups_rel (SELECT SHA1(CONCAT(rand()*1000,current_date(),current_time(),'s3cR37',rand()*1000)) AS id,
                              NEW.id AS user_id,
                              id AS group_id 
                              FROM groups 
                              WHERE type = 'PUBLIC');
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `CleanOnUserUpdate`;
DELIMITER //
CREATE TRIGGER `CleanOnUserUpdate` AFTER UPDATE ON `users`
 FOR EACH ROW BEGIN
IF (OLD.user_type != NEW.user_type) AND (NEW.user_type = 'USER') THEN
DELETE FROM moderators_groups_rel WHERE moderator_id = NEW.id;
END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups_rel`
--

CREATE TABLE IF NOT EXISTS `users_groups_rel` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`),
  KEY `created_on` (`created_on`),
  KEY `created_on_2` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups_rel`
--

INSERT INTO `users_groups_rel` (`id`, `user_id`, `group_id`, `created_on`) VALUES
('0384d9c967ddc01a3295fcb9e13badfabf0cd5a3', '56c664f556139646009c5085902d864d0aec7489', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('06aa72070f1e1d007cd68c133bd6e261c6dffdf0', '26b6699fa21f4609a3f9d4a91e0e25d25cad31ff', '3a087759743798d212480fb56224a7ce5be0426c', '0000-00-00 00:00:00'),
('072e437a679386c25c749b85022c1182a44dd3a4', '434af05134fefede190baad82d5d20d2e4139a9b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('09689cf9d9dfc10bef9fe43dc6f4829f8bafae30', 'e3c1aa812ce52d2fe42b8d7b0609dd07bf7c4205', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('096bcafa8ffdaa17b67264a14739d7462b1884ec', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('09f35264bb2f0d1cb7a9a0775fef9795648488c1', '26b6699fa21f4609a3f9d4a91e0e25d25cad31ff', '94622408f7dfe92bfa8f638b27819180cb4b63a1', '0000-00-00 00:00:00'),
('0a4ec6b98b7009db7badc81330a74a917d9f879e', '297948d12687b2cfef2f1cb61b15c788431a5759', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('0a6c53101dc133060918e8ac599512fb7dd7aa98', 'e2c482a187edbb53722db44fb915a5f5f28d4b18', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('0b0a6f50e6213d380300a2d5da5c0e26f8a6bac3', 'e123c8b0862cb9b327edd4d7d4405f303f573864', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('0b73c190edb2c42e14fb486ef0a41e5226a9cf08', 'cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('0b8e0194dfcbce304ad3ca477ba42a71cce612b9', '9a9ffe5a30758cc818c69101ee6736183fdc9c31', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('0c173fe0c76078bea666cf5962bdc498bfe17065', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('0c6738e019650fcf4c8482d3acdf0b62d99682fd', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('0e32ce17137233055798a281a86affa4584b205c', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('0fe59a4c07c52aff2a9439bc4b236980da325efb', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', '593df1d15bf348454af641c520242109e190ef4a', '0000-00-00 00:00:00'),
('108e0d6b068047f12abd14f696e78fdd6b9f6315', '87833dd3fe42d950312048a247abee56c512206a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('12f756517a56b0a94cf805aa49946b7ecdb053da', 'e220833d35c0b1a59f7d259aa7f873da7875523a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('136d2a31d406f8570182027d096f7dfd8de0b49b', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('13d43d1d7f483a881ad631b41e11fb00efd513c3', '7c89a21c6e3a4d558fa8ce068a68976179aae95b', '94622408f7dfe92bfa8f638b27819180cb4b63a1', '0000-00-00 00:00:00'),
('1489c97e929f0d88e26ccf976dd6e23c7bcc3cb5', '18ccb3f307380a58c43e20f6ccd9476e123059c4', 'fe10b0bca70113f2e303410dd28a233aa5d28ad6', '0000-00-00 00:00:00'),
('14e251259ca0703f5ff8ec59d6d7b5a1817d135d', '297948d12687b2cfef2f1cb61b15c788431a5759', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('15f069702f62521a6b22b1193aae88540142e269', '35947f9a84d372086e1079a0bf7c04df339cd752', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('16f87a67627b3ecaa375cccb85426015ec9e1a89', 'cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('17b290b08f97b84f42aa6b81eb1d408340ba060c', '082a42970550e73de6eae81a0115721670fa4ae0', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', '0000-00-00 00:00:00'),
('17dda43b8af71cadfb321b42970e5407d09ec8eb', '591a650d7a2fb61f59542dbf3988cf5e070f07d1', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('190c69290fb2fd5a68aa7d2c3baa3e45447603f0', '0fb59db5e8ea83ce86c1ed3a140ac24362f12999', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('1a06d66ec3236463d4017043984a950127ac35fa', '2de2783cbe1d1b7ca7a075c69efab6e201023f5b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('1a537ec4431f8d096e93d2f91f27c6593ae292bb', 'a3366bf67af458b23a059b4c64620afd5ef57059', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('1b70ff3d7af1a6ab469fe9f0de9e97890583eaa2', 'edccbb2901568bfc7132b4029965821d62acc781', '71c8b4f5239876cb45f99cdb3347d4bde3b708b7', '0000-00-00 00:00:00'),
('1c40be06ee1f2193aa8e90f4a0d810bcb7375b5e', 'd359ff9cfa7c0416a09b65e5476206267f3d4334', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('1c644d9bf970f163e113504fa5ac2d8611d4c8ef', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', '829d6507d6f317c4f61bb089b947462fbf858c40', '0000-00-00 00:00:00'),
('1eac684677ede90a3f0d794c14900eb1b94a2947', 'de37878d9b465e71f98dd0afebfdb6eb0434fb39', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('1eb1ba725f1dcf24a945cdf347fb930a77ba51ce', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('1f3938efeacc9d7c70c68dede0a7b8bdcc004d12', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('1f633a5b66ce395cee894f2274533202ab7022dd', 'c574df4ab917e71dee722e1c3d5963307be1ad28', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('21258276bba53ea62ca67292188db1b72d84132f', '95e0bd59c78e6a773e87738f10cdd01956321d56', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', '0000-00-00 00:00:00'),
('22aeb3efb09a46bc25740d05db40ff1b858d1bbb', '218cd822da03fed93624815f5e1c549b2a02fce2', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('239d34905af1e55de933eda5f7c73d1e1afa065e', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('2415a4d9c24a04404d0f3d177a8010cf80f85190', '334160b7e08d0a76b4bf5e30c7672abdb487d028', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('246c4113e8ad6823144214be2a5d57e2b5285645', 'a030f2dd2275da01941d67008fff368410ce5adc', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('250a3b2c6ea99df7548415ea71dc07249251ab6b', 'eab361df7a1860b4ea71d6467b35d3001a1426e2', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('253fc5234839c6f710f5a1a5848f4edb8c274f8b', 'dc81c63924583724ead007b9333fd3673d5284f5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('25c19f1d4baa838c7fa0bbdb5be0638792a672c0', '694c19780c680d1924a4995c4dcc77d372fd4286', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('25e492ddfa8b1c80d9634c78ae033f1b1e6e42fe', '851d94a798715ecb78b7df6e8e16933a1f9b0217', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('268fbf7a52b3cb53f8d7e8a7c7f7823be67fe93e', '6abd238ad90db1262ed7af63e41c4137f53d5a82', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('26e4106f9aa4fc0f4db5ac0313af85660c95afe5', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('279a350c18a3fe093ba3e70737199ffb1dcaaad6', 'b097be1d4ce228d16f898b8bbdf448485a9694f7', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('27af1516a5750b151dd320b92881f096440cacb9', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('2837453bc489f8354e84887e9401869152b56b9d', '87833dd3fe42d950312048a247abee56c512206a', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('2955faab13d5a11bac0335de052266affe0cfef9', '2f49f5406c36337f0f3fa7343e3bf82fef1129b9', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('2a9f299633d9422ad16d298f88b530789c694131', 'b28d3bb7d7182b9cd713bab2c991169bac78a13b', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('2b812c176e96473094c451c94321e8857da55ac5', 'dbc7ff3fb1910023b034c55b012168b36ad18a6f', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('2feb59d09593c40ab893aa88348bb7335f6ac620', 'cd65d2be40926a71b15df420496309791552b061', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('31256d204d1d0e0c973f4e47f21d940d5462c6d1', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('31c110c61134714af2c8a62e6100bbd56aeeb6b2', '09ef51e57e1eff67dc3163a2a8707a841d6779a9', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('3238b8fad253983b80bb0ecd00d793f756b47813', '9cadb6676e0fbe42f38ffe35544bfb68e1545db3', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('32b3f70334a9ca992fa2ed07590a09c7f260d96a', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('350be5cf0b3a4462785095445adbbfb7ef210dd5', '0076b06e109251a1fb424b94d34bfc7a31703d9b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('35125e3b6d466820a123ac997b02ba7d1a39b907', 'c1b01a4133fb1272944aaf7904233757e71c1e0f', 'b62242f4751b0a27a015e40ddc0339e4a6d54ffd', '0000-00-00 00:00:00'),
('36e11c961ca1db4c63cf59c521b79440118197c2', '9a4879b7f8dff6f51b73b56ed3b442b5f9c9060c', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('3b2c801e3495b9a6bd961bb0f2e864028e7224d6', 'd9a3dafc885d633069774083ea3546dabccf1fc0', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('3b588e195c58c34fb86670682a04a574698d08a6', 'afde714ca232c50a47e68469938d120b1c7a9710', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('3d559e48ccf52f302a3ddffcd79f3f11612b2432', 'a152b68ecebc4932be4601e5134efe804edb4900', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('3e5548a10845c8d191ade2f7b22c4e16eb9c43d5', '87833dd3fe42d950312048a247abee56c512206a', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('3ee2c36ccd47d96f7c025e99dcacf2572de6f4ba', '442fc82c703944a48c5df57c53ee76a80214ca26', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('3fbb4fb776ffa0a57d677b799f0ccafe7b47fb1f', 'e3222c0f6cb1d59d6f89a2e3c7c1498d843384f5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('41611c6fd5b490ca314b460f06d6c0233bff0849', 'f2edf7c2a41fa0f2a731663811baaa9cfa77d175', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('4181e201ffbc183d83fe231ee953cc769a529d8a', 'b42fecdedd5bfaeefdb5d9741c761fb2a8d59916', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('425da5076ee8f618eb7de97e2a429005667f61db', 'f061b3ff92d01f3336eee702e1e883fc3a280595', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('42d0d2b53b05f7d7b2efbd81141af394a9e0ffce', '4bf0af1d6b07d27d15d63975461fa10bae141f2d', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('43a1f0134d056d8ea6dd27129518e7d170278954', 'b28d3bb7d7182b9cd713bab2c991169bac78a13b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('448ba2d339db7c8ad14fd096f735b09b82d45abf', '67b254e35503df2475eb744a4c7e5f98d8c761f4', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('45edb862a177ab0eade1952a5c6efe58e4313944', '811ae807cd749d57fc57e160a4f150c432082bb3', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('48baff41d403d365721801b9d9195ad25eec5af9', '9a9cdb3e8d0e9474867fab7b3307a4a42a9adeb6', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('490aa2ef4c2d32920bb9b8cd40e4a019e07d62e2', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', '0000-00-00 00:00:00'),
('498049f769a58823ca62a0a474e5b36f4bc85a22', '442fc82c703944a48c5df57c53ee76a80214ca26', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('49e02b9ca2d12fbaa60f1ff08a5e564328543939', 'e033912e12e262323237f27c03c5e69c49857bb7', '593df1d15bf348454af641c520242109e190ef4a', '0000-00-00 00:00:00'),
('4a486c1eaf63cb85272f31723d9eda7a0b7df79f', '44c4a4d11f7f7687034ec99aae6680476823c28f', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('4afcc2f8137e455f942c6997a6bb729bb0b85d53', '8b3562b7c72507351952d5fb69642043bfb017d8', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('4ee08f16b552cd89b0b5a26b96bbded06d08c370', '9cadb6676e0fbe42f38ffe35544bfb68e1545db3', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('4f0f4bbcccb3a7ba1ed5e44d35c2d39c018faf9c', '855214b20e5e451e60cc0b99facdbd6cf054c057', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('4f39aa027022158a1bbdf7bf78fea4d3ddbefc77', '9a9ffe5a30758cc818c69101ee6736183fdc9c31', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('50b17a8728e241658a6f5b7ee37f3238de638764', '811ae807cd749d57fc57e160a4f150c432082bb3', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('51bdfc33f18e46684ae644111278c60708f18947', '99cbadfecd6508126d9b67aed7cd3a571fa7f0db', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('5236b7c055b0b7eb96cd781313d2910f9762c9ac', 'cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('53542cee73cc2ecd8e0205be8b32a6609f42e899', 'c83612ead69fb09a166954909e89a1492465407d', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('53d349daa30609b7b44a1092350cc8ad465a3fc9', 'cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('55867e68fe0e71d9314805952f1615b18a936872', '7af2386d611b99381c8414f62f16cd0e631ad5d5', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('589a2f7e81555fe258a4f6b5dad2932d0d86c682', 'cd65d2be40926a71b15df420496309791552b061', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('5913fb66a35d8ff850475d6629a9658f85b899ee', '74594700b181117efc5ea1d0d694284d8e780fea', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('59c9ecb8cb519b0f2a77b0a67043c339f4b2dab6', 'c1b01a4133fb1272944aaf7904233757e71c1e0f', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('5cd3ccd9437424191a4f2b5c7417a1caa7bb683b', '218cd822da03fed93624815f5e1c549b2a02fce2', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('5e2300ace5fb0b9a1de5d9e4f2778c9d546d4625', 'ec5d89e3ecc41caea00fc833d19e834372622d23', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('5eef5b611e021fe2d49acafd5f07e017cf644f33', '218cd822da03fed93624815f5e1c549b2a02fce2', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('5fb45efc39c450b22c2f5c7a299f84b8d2753670', '37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('6194df46671d77c9c69ddb095349cd2356c036ee', '786e153e0309b35da63ef455df309f84efe0135b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('6653f4784f1aad0be1cf0a9dd104a12a4da90a7d', '2f49f5406c36337f0f3fa7343e3bf82fef1129b9', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('68e66fdcd47142858febf01142257f049e484db5', '7af2386d611b99381c8414f62f16cd0e631ad5d5', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('68f8c75bc5b4577288572f4f3703410f95082e8c', '9be32d97036c4bd7b33488d0538816e8e2111221', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('6d0810bc6a8858fef4f7fe5386a5f219fb2ed642', '875abd46a00746aca64282a01733a7ac799cd0e8', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('6d25d26322485916b21aa893da0fc6524ade71c0', 'e033912e12e262323237f27c03c5e69c49857bb7', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('6d7a0876828e8c1372dfc1b4211cac30cb91897b', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', '8b3e20ba35026b7f1d02460df7d44691a1e51de3', '0000-00-00 00:00:00'),
('6e6d89d59a6db7279bfbff0561e66da89b300a5c', '442fc82c703944a48c5df57c53ee76a80214ca26', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('70ddf8f4ec9b0fd8f6780160629f5d9f46c419e5', 'edccbb2901568bfc7132b4029965821d62acc781', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('71bca0b51ecfba1be1fc94b4f44f77247b842cf1', 'ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('7317d4f2183e4a85e6abfd6647fede5ff2f3d34a', 'ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('73ddef1806396d8cb2e0528fd4305ab746dce91b', '67b254e35503df2475eb744a4c7e5f98d8c761f4', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('76aa9fa3284ebd5b7073b48e25017375b6f709eb', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('76cfdd0b5c33050edda7a509b1ca4ba27e886518', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('780693ebad6bd79cba7903fa4f025b71b9a9834a', '7af2386d611b99381c8414f62f16cd0e631ad5d5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('78c299a3a070208c600c5ce48c29c277ab244190', '06e02b06a00423d924699e7d64ea5450e23921ec', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('78d0b69843f978fa1525b1352b25d64fc7195976', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', '3a4e3b4f27a3858566c1288d7bcf35c85f08dec3', '0000-00-00 00:00:00'),
('7d6b66d1967d420c2e779d460cdc4788c08ec810', '838c670e6ad8b3074d4fc35322bdb5deaecc07b7', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('7e3640079b5c437ae33f2889563d012f08a03128', 'cd65d2be40926a71b15df420496309791552b061', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('7f03bc9dc2951dc298bc35f849c544866c79d010', '0076b06e109251a1fb424b94d34bfc7a31703d9b', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('7fbd5e7edc31c22c5a82edb7aa54517830c24072', 'cd65d2be40926a71b15df420496309791552b061', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('801b1ecf82a757b35e02877d9dd7e7e56fad4705', '9a9ffe5a30758cc818c69101ee6736183fdc9c31', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('812acb73c1f743f5e965fb9e32d0914bc4e7ff91', '082a42970550e73de6eae81a0115721670fa4ae0', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('812b687de9adae1af68bc1e2c20feccd842dff9b', '2f49f5406c36337f0f3fa7343e3bf82fef1129b9', 'b62242f4751b0a27a015e40ddc0339e4a6d54ffd', '0000-00-00 00:00:00'),
('814c366e016efe46397d41a46d38dbe84013ec85', 'afd889796bb809d3328a5314409bbf9bdfc5ee33', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('816cfd974e92e19f59ae6b89886308b072471957', '098db512c2fcf89eef0c22c5d1b87959eb722853', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('8432f542d08adc2188fc1104fdc4072c5bf3f3b7', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('867b42a5c4b6ee33909a2bd2dcfb985012f879b9', '0e1efd3a02e2eff69c60a7a149e21faed105b20e', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('8767c6017fac3f16344123f8d1aed6ebc2e754af', '9a9cdb3e8d0e9474867fab7b3307a4a42a9adeb6', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('87b5dbb198fbf3035a1f52ca21e09f934a2ac273', 'de2804005363c30567b708bb7161befcac12e920', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('87c214dba27de0ee58d0921630f04199a53fb134', '811ae807cd749d57fc57e160a4f150c432082bb3', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('89f375f6a078e03543d046f68675f4152f82b2a6', 'a5140634241c8cb30c813b6c6616bf459e77c658', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('8a82c7e25646dd5be863fa9dfc913ec278d84b9c', '95e0bd59c78e6a773e87738f10cdd01956321d56', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('8bc0795e41788050234cdf62434baf2ade6ee824', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', '94622408f7dfe92bfa8f638b27819180cb4b63a1', '0000-00-00 00:00:00'),
('8f70614f9250ea49a93f61152ec7082aa8c9dd06', '2f49f5406c36337f0f3fa7343e3bf82fef1129b9', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('91f41a5dc39ed279c799749e8544174aeff0ace5', '4ddd34d60fbc26f130590a9e88a695a37d04c69c', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('92682ca466b1814fd5e81761540a1e8a4d91cb79', 'f238fe75d61543163ac415c2463f18449524dc84', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('927ef33d2d9c990d8cc8f5d60c879d234147ee07', 'aaca762861b15094109fd3abe8246aceaf15feed', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('92ae8ad017e263010589f6746cc79884fb69303c', '95e0bd59c78e6a773e87738f10cdd01956321d56', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('93dcea059a11a634dc9d77704b004e57102076e1', '1c40df7d68a0067a5ce823550bd84a9b90c7dcbc', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('93e2d9a26a2bf06d25a94001075841f35f131d60', 'b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('949002d6962a51d24057ffa044e6fa79dd7a5c24', 'afde714ca232c50a47e68469938d120b1c7a9710', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('94fc43e6100944a32de76aaf72f2d250db5bb40a', 'cc2418dfe7b9fa0c0cdc2458b368fb202e1a85a6', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('95486b87fb81880429c4a377690222f48d95e19c', 'b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('9561628eaedc2c2e942966860430b8691b5a4f9f', '95e0bd59c78e6a773e87738f10cdd01956321d56', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('9573cd35b3405f40b780a6b09693c1980c91af2f', 'e6ec1757798c724652e81d024082dc8395166aca', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('9745afbb59f3ce73c656140f4cad25a8cd1c2a47', 'e2c482a187edbb53722db44fb915a5f5f28d4b18', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('975792bc2387a95c40cc8a5be85a95df4f074580', '442fc82c703944a48c5df57c53ee76a80214ca26', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('981c92825c1b09f1572d0248acd0b1a3d1117ebe', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('984782307790f89f15dada9fe17cb3ce213ef1d5', 'ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('99575514a0b4a5a6a078ae0cb1354d41c29ca82f', '297948d12687b2cfef2f1cb61b15c788431a5759', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('9a5287522bbb82d1a4e88de86eb56f886bfa1a93', 'cd65d2be40926a71b15df420496309791552b061', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('9aae595631202f3cbaa1f15089dd2a89121aa831', '8c57536789ab46301206b1f9c3c24fcce824cf07', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('9ca0fd483a1be1bee23c277cff50e4b2b9b55888', '63972591a378a6838fcf1d2985f8073751cb0339', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('9cfc5559df2429ac4ac491ea1c465b2617e45fc8', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('9f38fce933cf9284e37d798300cf285c88cf5836', '488510d7fd80f5c6bf8a0016a72024296b730237', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('9f3bb58a44b048fc1a943d71d41a638f96b59ea3', 'b1f197edf63f3b29243cd4a6e555f5c6a8c13330', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a02922e317a08c03f95f4193694ef5878a2d7736', 'edccbb2901568bfc7132b4029965821d62acc781', '778698d529b999a99176055b67d03396b5544d2d', '0000-00-00 00:00:00'),
('a046a8740f0ec2f2e8504bbe1cce4899695f75bc', '442fc82c703944a48c5df57c53ee76a80214ca26', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('a08f64c9b4e3c3687e01f70776aac20bf4a3b26e', '35947f9a84d372086e1079a0bf7c04df339cd752', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('a0f1603de89f7a422f77d639a85e061d7c950435', '7f16719ab33716a9a412699f4304d03f4420cc03', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('a117976e5446c550d55c8b6a6c9de496f723de72', 'e36eca4399ccd5d1176b637848f9076f2c6dfdc9', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a1d502cf65856897a63dd1a43ed80c25d2e5269d', '79b47d3e24d3731a27114143ca47572cbe72ef89', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a240dc4758473a399b7b284ea41bd3f850629a0e', 'de528d3b0696706e44f31ea35bcfc795f9be9ea9', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a25f67912e9635307f72f447ea2979a93fb5500b', '203684fea75da2b934935d0bcdcd943bda4e815e', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('a286668ec62060f812792cc69285b64873b446b6', 'd4f56feb5bbfabcce4dc320b1c3c9ce5691f7723', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a2bced9c2398e98b6eccd5a6fff30f8e42f32de3', '52dfe68781c69d7d2289e0f7ab9ce52c58893027', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a31ec8c7e37948f4c160faa3d9cb7c4babd042bb', '7f16719ab33716a9a412699f4304d03f4420cc03', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a333c92afd7758c729a2e498bcaa8536bee38c38', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('a34ec868a3bfc78a9244d8260049aacd39d9acfb', '9a9ffe5a30758cc818c69101ee6736183fdc9c31', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a42cd49c8c78fddde4962b4ff2d77917b7e4ffbd', 'edccbb2901568bfc7132b4029965821d62acc781', '3706ede822e9f5b6531a47e5318c1ff75362d389', '0000-00-00 00:00:00'),
('a4312fe553d09ca4817781d2c1810f64a08b83a3', '7f16719ab33716a9a412699f4304d03f4420cc03', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('a4b523bafbdaf53cf8bf0b2d59fa371b143422cd', '811ae807cd749d57fc57e160a4f150c432082bb3', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', '0000-00-00 00:00:00'),
('a5ded6f68f6ea3e5d9348d0c2ac7fe28b8f4da69', '1414a9e43cee875f0b79d756807ecfcd8e8f0ff1', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('a7cc1d7f3fb1df9b119c0bb1abbec50bf0c93a54', 'ca8f687f06392db3ca37c04aa5054424c99f434c', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('a7e09a23c6846a312f17aa4bae8c711d985d4815', '79ffc61107fa0b544f970805129dff3a990348ab', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('aa1a827a4172fb88d8e26e4f8882bf2d34c41357', '3f8af0fdd862b2ba0f9a92cea9daae3e40f0e9aa', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('aa9e582439c37325438a5e84c98fe63a9051e2bd', 'ccde072bb13495de3da51216138469ab53dacd7a', '3a087759743798d212480fb56224a7ce5be0426c', '0000-00-00 00:00:00'),
('aaab199467f796d4c8b3556dcc85fa02039b3751', 'e4c88451ebecb9af671e7567a9091608d42d92de', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('adf5ef09e9195adafc10a1f21430fd5b185511ca', '63972591a378a6838fcf1d2985f8073751cb0339', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('ae877763649ef60e4626b848ed619ddce831f8da', '09ef51e57e1eff67dc3163a2a8707a841d6779a9', '94622408f7dfe92bfa8f638b27819180cb4b63a1', '0000-00-00 00:00:00'),
('b01f2142a01efbcbe3c868a057f2fe4b170fa524', '95e0bd59c78e6a773e87738f10cdd01956321d56', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('b03f2346423a3bdba888fc8acde4b5c03658fece', '811ae807cd749d57fc57e160a4f150c432082bb3', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('b13c28a20dcb5d714a144aea8c2a802353d44a5c', '5ec7758c30c69894e63e1226c4ffd316b0deb6c3', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('b42aa8c133abeacd2f010f2aa7e539545c1de7c2', 'ccde072bb13495de3da51216138469ab53dacd7a', 'eb8a04711533ce9129aefa8a01e04e3a291b3623', '0000-00-00 00:00:00'),
('b530babcca8c1bb6c05ca67c9be938925d4a6f18', '875abd46a00746aca64282a01733a7ac799cd0e8', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('b5bb310a5d5cfd573acf344cec945ce64daab679', 'c83612ead69fb09a166954909e89a1492465407d', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('b6aa0c3b7f30ab740500af7a181954b56e1eee9e', 'ccde072bb13495de3da51216138469ab53dacd7a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('b6c973aaabe64bfbf7688c6869e73052fe05b4ec', 'eca542f2d538bf2e75a202b66e3fe78480174011', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('b78c1f7fed7e9484dce3856e53aab9f8013d28c6', '96df91e3b82c16b7c6dc546afbe75975fbfd4e47', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('b8699c681096e68c3cc865de7ae26c3c09504db5', 'de2804005363c30567b708bb7161befcac12e920', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('b88e536fc12e06f53c0e730ebe816ba09de2f406', 'd661ab82373fd8212a2b7db4bb8f8cb4afeba1dc', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('bb184ba905ec4ced389ed26143b4930b65e2d179', 'afde714ca232c50a47e68469938d120b1c7a9710', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('bb23e448beb4b0243f5ce5e294ed93e02a912924', 'f40105ee8842db467b79106505b226c438e5baab', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('bb372376b43cbef46543be774a4bb6fff92eb129', '582b2b6bfb89ae7f042937f3f5229502edae9b4f', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('bbaf3789bce7c0bdfe6f8ca6608ff2d251baf04e', 'de2804005363c30567b708bb7161befcac12e920', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('bc63cdcc42fee689810b0808c8496737a7c6785c', '37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('bd34eafdd00eb5661fdb927b22df42364f599b49', '1c40df7d68a0067a5ce823550bd84a9b90c7dcbc', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('bdb8e2ff0ea298d47b99d5acf9ea6f9c5068a2eb', '95fc87913b9f609f2d2a72c23da0f46a2c396909', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('bde2331de45fdbc2c6ecaec3bab076b0c730954e', '37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('bdec9cfa05a1b1103c5e245ee286f2ccd90c8ef0', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('be4bc0cbb0f9145f3a76c6200be0c3559d46e02e', '87833dd3fe42d950312048a247abee56c512206a', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('bec9e6d02f169749ae70edf7801ab1fab840078c', '95e0bd59c78e6a773e87738f10cdd01956321d56', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('c0ba6c3d88458944a99be1410ae5ade3c9d77b5d', '37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', '819a5d1c930c8275ef0f190ea0d562287acc7e3c', '0000-00-00 00:00:00'),
('c3df11ba4c51b10eda8b3164d05b1142f77f6a66', '0177fa261ab830a8cd033134bcf0a5e189ad6c95', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c3e773fd4903453d3cf207cd661f0030368a9ce6', 'cd3e8bce1f8b5fb55f24b7d1c68dc0ca58dd8eb3', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c3f4eb4b21813d1381582636756ca900cf82bf81', 'ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('c4263c4e5b3d86118c78f21637c05fc351eab198', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('c45e5291a4482c295420f0d063a6b7b5c730a264', 'cbe7d4a4921c8b4081b73c0579234e54e08bb7f2', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c4e518259f0217a5e39b772d130d0392da3343bf', '6206c28d5dd38fac93fd162727adce2643f35fb5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c6bfac09b3b60ea132ea0142629e165e4519ede2', '35947f9a84d372086e1079a0bf7c04df339cd752', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('c6d7893bfe3dff2510ec188df4a246fa84623018', '122f232bc3480c24a9b67bc72e5c1cc372abe43d', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c77c92ce5265c591769f9d960ce612a79233778f', 'ee6a3b99eef3714dd4f9646f68ba5fe3233f7857', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c7b76f3df7e3c9bc8db3598314ab4eede7f0f2a3', '20b5995a0114ff7efe1f6f784ac61b32368daf5d', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('c848b55d5d28224d768af9ddf7c6879fa415698e', 'd463b8031950dafade4f577ae4ff06afb4dfd4e7', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('c868bd8530359eebe03303cd3b3aa309507bc292', '5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('ca4248885b615223b3f7279d61c17fc5b830ed8f', '34f9c80cb9012fb85ea164e6402bb4b79fb206a9', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('caad15c02217152678b57cbc72fce1b39547a01d', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cad94e991e2f1f6411d53722f5be6e0309dbc2fd', 'b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('cbbce8956ac537d029aa5649e663b10167594ba2', '24553211159cb3a67d5e5b691a1260ab4caf4fd6', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cc7720457d624dbd3d3eae9acc49280c80778b18', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('cccf50370851b3d87049a503f40cc6ebdb5f32bf', 'e8356e2cffeda6bc053691c7f416b63d8d5943e1', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cd54230358f6edf580c5e6ad884972f5a277c91a', '63972591a378a6838fcf1d2985f8073751cb0339', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('cd7f7ac7bf891acb076dec4b3d86c1eafd008f84', '26b6699fa21f4609a3f9d4a91e0e25d25cad31ff', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cdb148718bbd6443c0d5ecedad4d5f744864bed1', '4e20d3f9f8bf0c6327a00481fc3ebe8e248f0f42', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('ce0d983ae83a7da0bda6447270de4bf7ddf6e81f', '41e67fda8bf2f27fb26540893fb2f87dcfb7ea6f', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('ce83e1e409ef59950278b21ac75e4a4f99aedbcd', 'dfa8c0f7224598a5b5755d47575dea5ad5126cd3', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cef5e73aa6ae5ed4aa85487e6e160f4e9f85e580', '1c40df7d68a0067a5ce823550bd84a9b90c7dcbc', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('cfc6bc4911191a7e7ba1a95aadcf9b45a748ed4e', '63972591a378a6838fcf1d2985f8073751cb0339', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('cfc7e42ccc20c125ffadea6d0747011ddae084e7', 'aaca762861b15094109fd3abe8246aceaf15feed', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('d03ee76e553a2f84b977a7647f92263269067388', 'c1b01a4133fb1272944aaf7904233757e71c1e0f', '9f8430bf2fdf3daf06fdad1ce2191685fcf30fc6', '0000-00-00 00:00:00'),
('d055efd3367551aeba2b80855efb73fc58aadeb7', '3f2035b21d62474c798f3af7aa737a67515eede6', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d0a6bd80b314dc955591fece75aa12a6ec3706d1', 'bbf458a9e2ac0b49835a622f6fc4208d0151f621', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d23580fd7f09ba2aa4b3604bbf106aedaa41c812', '256a33f6bb2c251ee381fe3ae70e9c7e10c6b773', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d29da31768633b9d02703015a1e866f61ccffcdc', '99cbadfecd6508126d9b67aed7cd3a571fa7f0db', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d4bc756673ead1ae6a753c969f632e5e10231ddb', 'c1b01a4133fb1272944aaf7904233757e71c1e0f', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('d623a1936963e8e1f651a5dc0a46f8bc1ea794b6', 'c74efa4b4bfafb662a373d360dfefa3a3e10f33b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d6a644081dc743774abdfe8acdcfb6790dec9be6', '35947f9a84d372086e1079a0bf7c04df339cd752', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('d703ee7546416577b2b168cdeccca0ffb75cb140', 'e033912e12e262323237f27c03c5e69c49857bb7', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('d76130afa7a4f48af6c0bc738ec24e638d957b60', '7c89a21c6e3a4d558fa8ce068a68976179aae95b', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d7d6fa37aa904677518f32a719cd36e066b7afae', '0c4c7e6553226ed3a9a60690bfd8dbd79eabddd5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d8232e71419ccd8aa28548ba5f9197a16cdb5760', '95e0bd59c78e6a773e87738f10cdd01956321d56', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('d8a0808be6d38ec54c7e3fb2c2ab6f3d2db41164', '9a9ffe5a30758cc818c69101ee6736183fdc9c31', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('d8ad58bace0335324183b0a77b4c97650e8825d6', 'f9d2f47220f19dcbfd4c1c0afc3cbca06435f011', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('d9f1d906fcd91fadae4f258f9660090348ff146d', 'ae86f25902cca5e83a6568de251430b9f972c693', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('daa697451a70469be0e1f6d88454bd87f85c9b6c', '95e0bd59c78e6a773e87738f10cdd01956321d56', '593df1d15bf348454af641c520242109e190ef4a', '0000-00-00 00:00:00'),
('dba6746a0b73683b70cc25bb8b092d4dc5e1d466', '95e0bd59c78e6a773e87738f10cdd01956321d56', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('dbd0544a985d8e70f12bcdf466a417a40e640574', '5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('dc6ed7922cd807f90a51216b49df9bee53a6e4c9', '37ef3095e4985c4a2a54990ea0db01e97bb9ce4a', 'e907bc6620764b00c16f967c80bdb6ac10e58c16', '0000-00-00 00:00:00'),
('def73bbd7569269092ba6d2a0abc3dbbac4aaba0', '06e02b06a00423d924699e7d64ea5450e23921ec', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('df241488ebf03c8e3f9a58618dd236ba2021798d', '3667c1803108ae08523745a76409eeee6999974e', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('dfeae67278715663781a4d3555ff71f05458298c', '63972591a378a6838fcf1d2985f8073751cb0339', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('e0d762ffe807c4a764c7081843bafa33d27507e6', '857e7ac33eeb54414cf847203bb72381da668651', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('e1d53f0413241d0c27da4f24ce6d38cbeb1c1f14', 'ccde072bb13495de3da51216138469ab53dacd7a', 'eb0fd5c33c4838cc102e24ac284ace997249d4f7', '0000-00-00 00:00:00'),
('e2fdcca52c3213f40b58049186d881c7b9f588cc', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('e37f98bef505848ea6e2123e9345ffba260e3ffb', 'e2c482a187edbb53722db44fb915a5f5f28d4b18', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('e4128c38675a5b47276d283a5e1614571d23fd8b', '06e02b06a00423d924699e7d64ea5450e23921ec', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('e6a6a9c4577d10276b6357764aadebd44068037c', 'cd65d2be40926a71b15df420496309791552b061', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('e77158417007816386ef2444150874796cfe6fd6', 'ca12ede32a0dc83c64adfc9f42216d5ddc6f53a9', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('e7cb9bede8c17d16d8ce315160be01282300a2ae', '95e0bd59c78e6a773e87738f10cdd01956321d56', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('e96195b02c58e46cfed16a0dceda9ca839fa08be', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', '593df1d15bf348454af641c520242109e190ef4a', '0000-00-00 00:00:00'),
('e9bf8b9c1f8979f549c7f07c73c0a5360be82b36', 'b77e9151bdb749c12d6e309835772f1ed898c341', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('ea8593467a25e6d7328952e197a2ee29de6b7d26', '46acd41fed173c3802e87ec23907ef878e506435', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('ea895e815aaf90e04f5737fd8e78270dea7c6a28', 'cf53837e402dcf27b4fb87e3d0ff2fdb8c546867', 'b62242f4751b0a27a015e40ddc0339e4a6d54ffd', '0000-00-00 00:00:00'),
('eb26ca6cf9f52bc812e971064ce031a048047416', '8b22aa1ec0421f8cd5c7deb15b7a66a874f843e0', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('eb2fde7a5c48b28608e4eaaece88c0f138e4df9c', '0076b06e109251a1fb424b94d34bfc7a31703d9b', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('eb747788d023947c9ebfcf7877f757ced4c1bcfc', '4e210efb9fe432fa5431f7f7c098bb9c4418fa53', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('eba9a9122d1189fa9dd41f964d83c138b90ec0c3', '6cbbc361a109eeeb0e84c41fa90422ec65e78ead', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('ed27efd1c433e92ddcf41165a82cbd314bc100a6', '06e02b06a00423d924699e7d64ea5450e23921ec', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('ed38c1ae9a77a99107940ab3a6b2cb260b22bf69', '297948d12687b2cfef2f1cb61b15c788431a5759', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('ee91cb44c7c5f8dd3a9eee0071ed0309dd70e32a', 'd4f56feb5bbfabcce4dc320b1c3c9ce5691f7723', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('ef739c73431f8ecbb5acbd7bd78c727251ad65a0', 'e32d77636dc245a11ec090467d10bf413b69aff8', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('efd3ae4a36314f73cd05dae6d9c036fcd7e1f25e', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('efed3557292c10b68f01dadc683538b7ccf14a3b', '67b254e35503df2475eb744a4c7e5f98d8c761f4', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('f13481b2f0cd6ff5eb3714f1af4085f007260c40', 'e220833d35c0b1a59f7d259aa7f873da7875523a', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('f1a6604e39923df27fc250cbe8bcb573256e81c2', 'edccbb2901568bfc7132b4029965821d62acc781', 'd4022da173f22c5dfd263279732c13c777c1306b', '0000-00-00 00:00:00'),
('f1d1f8e02b918895707b5dd2497e39a1bd6aeb3d', 'afdf7c5ceb82bc59e6d0b2e55397620887703afc', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('f24d537cef5f621ebceffb571e24aa98cb9b91bf', '6abd238ad90db1262ed7af63e41c4137f53d5a82', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('f336681962fb4ac0a94979e343c53227573f2c4d', 'edccbb2901568bfc7132b4029965821d62acc781', '03643977582ec50835f4eca3bf62115913ea4566', '0000-00-00 00:00:00'),
('f47a11f7c123c06b2d1ce1f6169922fa857a06f1', '6de1bc0cef4b3ef9cef51e8bcc70ff5f952b7712', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('f49884e99c7ffa4ab764ef1dbc5dfeba7d668054', '35947f9a84d372086e1079a0bf7c04df339cd752', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('f576735dc1491262e86f79b6a4732e6d3b6690d4', '7af2386d611b99381c8414f62f16cd0e631ad5d5', 'c48a7061008f761e3ab019e370a44ab42a2ffd6c', '0000-00-00 00:00:00'),
('f61cfe1765c0e29974082a45631537de0648423e', '556b2020f49f56befbccc5cddea91c75611732e5', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('f788edda81d70bbced513d06a66d31fac66bbf53', '5c8dc1f5798bddb2e3156526cd5e69278fe32b2a', '2996e0578bfbc0acea45f0579c6081943d3f34db', '0000-00-00 00:00:00'),
('f7e68fe524cbf56f0fcc01c1d474994e2ed34a4c', 'edccbb2901568bfc7132b4029965821d62acc781', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('f81e8d1db3a40d63d451ce989dab6ee7db6b2382', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', 'c5e3296e0993e2d2f0c2e9e80304a9cd1788f970', '0000-00-00 00:00:00'),
('f88d04f20270ef4adfa8378ff6893c478c8786e6', 'abcce4ad1f371b751de6acc8c71a5053241dbdec', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('f8a499ba4fa528538ce67952c18d25276ad59fd9', '1113458a8fbd03a81c20e2eef4e83350f14c8f53', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('f91d51cf01ef8a19fc1592501ac5a60d3ce3867e', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', '03643977582ec50835f4eca3bf62115913ea4566', '0000-00-00 00:00:00'),
('f98f6ab80ce2326314cf8b67c64c9f0f9446139d', '218cd822da03fed93624815f5e1c549b2a02fce2', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('fa375755f7af1920630a54640c50cdfe737845ef', 'b28d3bb7d7182b9cd713bab2c991169bac78a13b', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00'),
('fa3c8892867e26e8c04f265d328392654e2770da', '35947f9a84d372086e1079a0bf7c04df339cd752', 'e9a295605eba92bb7f00bc02cd73828f7e6b4f72', '0000-00-00 00:00:00'),
('fae04bbe1f9eab77fdef8af3893cd2c088b54747', '07a54e90f790ae4f8717cbebf4680c22e3135d7d', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('fb69fc9a70eff890b8ee08032f0f07c44ec5e0b3', '80b5157219d10709e6fab882477e4f10232e2c3c', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('fb97813e1e1443e5e8c4dda5dd0daf21e212e9c1', '0038bff4093c70a357cd73fcd032d41ad8c0daf5', 'f2cb578c46cdc89c1cc34dfff2c32f755ec6c0e2', '0000-00-00 00:00:00'),
('fc966d8b5f8af7fcd41df62b6619cba723f0e78d', '84e63835eac4dd6aacbb432ca1818bb434d8ac97', '59fa4b932f5006d750ae113f3da107a3a1897b1e', '0000-00-00 00:00:00'),
('fd28d42cd7e972791888cda830943daa3433a096', 'afde714ca232c50a47e68469938d120b1c7a9710', '94f0fae975eb152434fbac496a3ea63de250a1f9', '0000-00-00 00:00:00'),
('fdd3e312578df24516a6cf3657dc7661acd5800c', 'b54532b8c8c2bd1b1815d89c4933b141c16b2f1a', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('fe59fe201693a034040961f5e8a1eccc0da8ec28', '875abd46a00746aca64282a01733a7ac799cd0e8', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('fe8b0cc9b604cc032e1030020bf21cc89b88d060', '26e290271c3b1dfbcc3a8051357be6cc4a68b370', '29e95be8980ac760c41886393303f0bc846574ae', '0000-00-00 00:00:00'),
('febfa6902dd584d8954d0973e257deefaaa086ac', '06e02b06a00423d924699e7d64ea5450e23921ec', 'ed016dd680ef96b63f824195b9c14468e6177a09', '0000-00-00 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `group_requests`
--
ALTER TABLE `group_requests`
  ADD CONSTRAINT `group_requests_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `group_requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `group_requests_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `group_requests_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `moderators_groups_rel`
--
ALTER TABLE `moderators_groups_rel`
  ADD CONSTRAINT `moderators_groups_rel_ibfk_1` FOREIGN KEY (`moderator_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `moderators_groups_rel_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `users_groups_rel`
--
ALTER TABLE `users_groups_rel`
  ADD CONSTRAINT `rel_group_id` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`),
  ADD CONSTRAINT `rel_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_groups_rel_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_groups_rel_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
