$(document).ready(function() {

	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	/* initialize the external events
	 -----------------------------------------------------------------*/

	// $('#external-events div.external-event').each(function() {

	// 	// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
	// 	// it doesn't need to have a start or end
	// 	var eventObject = {
	// 		title : $.trim($(this).text()) // use the element's text as the event title
	// 	};

	// 	// store the Event Object in the DOM element so we can get to it later
	// 	$(this).data('eventObject', eventObject);

	// 	// make the event draggable using jQuery UI
	// 	$(this).draggable({
	// 		zIndex : 999,
	// 		revert : true, // will cause the event to go back to its
	// 		revertDuration : 0  //  original position after the drag
	// 	});

	// });
	/* initialize the calendar
	 -----------------------------------------------------------------*/

	$('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,agendaDay'
		},
		timeformat : 'H(:mm)',
		events: "eventsmanager/getUserEvents",
		eventRender: function(event, element) {
			console.log(event);
	        element.qtip({
	            content: event.description, //+"<div style='margin-top: 10px'>"+event.category+"</div>"
				style: { 
					padding: 10,
					background: '#F5F5F5',
					color: 'black',
					width: 400,
					border: {
						width: 1,
						radius: 1,
						color: '#E0E0E0'
					},
				},
	            position: {
					corner: {
						target: 'bottomMiddle',
						tooltip: 'topLeft'
					}
				}
	        });
	    }
		// editable : true,
		// droppable : true, // this allows things to be dropped onto the calendar !!!
		// drop : function(date, allDay) {// this function is called when something is dropped

		// 	// retrieve the dropped element's stored Event Object
		// 	var originalEventObject = $(this).data('eventObject');

		// 	// we need to copy it, so that multiple events don't have a reference to the same object
		// 	var copiedEventObject = $.extend({}, originalEventObject);

		// 	// assign it the date that was reported
		// 	copiedEventObject.start = date;
		// 	copiedEventObject.allDay = allDay;

		// 	// render the event on the calendar
		// 	// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
		// 	$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

		// 	// is the "remove after drop" checkbox checked?
		// 	if($('#drop-remove').is(':checked')) {
		// 		// if so, remove the element from the "Draggable Events" list
		// 		$(this).remove();
		// 	}

		// }
	});

/* This function makes login up and down scroll real-time
	var $sidebar = $("#sidebar"),
        $window = $(window),
        offset = $sidebar.offset(),
        topPadding = 20;

    $window.scroll(function() {
        if ($window.scrollTop() > offset.top) {
            $sidebar.stop().animate({
                marginTop: $window.scrollTop() - offset.top + topPadding
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });
    */

});
