$(document).ready(function(){
	$("#dock a").hover(
		function(){
			$("span", this).animate().stop();
			$("span", this).css({opacity:'0', 'left': '-30px'});
			$("span", this).animate({opacity:'1', 'left': '5px'}, 500);
			$("span", this).animate({'left': '0px'}, 200);
		}
	);
});