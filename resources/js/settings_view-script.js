var timeOut;

Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
	if (arguments.length < 3)
		throw new Error("Handlebars Helper equal needs 2 parameters");
	if (lvalue != rvalue) {
		return options.inverse(this);
	} else {
		return options.fn(this);
	}
});

Handlebars.registerHelper('currenttab', function(value, options) {
	if (value != settingsFunctionality.globals.currentTab) {
		return options.inverse(this);
	} else {
		return options.fn(this);
	}
});

Handlebars.registerHelper('createEventButton', function(groupId, ownerId) {
	if (ownerId === userId) {
		return new Handlebars.SafeString('<input type="button" class="create-event" value="Креирај настан" data-groupid="' + groupId + '"></input>' + '<input type="button" class="manage-subscriptions" value="Менаџмент на корисници" data-groupid="' + groupId + '"></input>');
	}
});



var settingsFunctionality = {
	globals : {
		overlay : $('div#overlay'),
		navMenu : $('ul.navigation'),
		content : $('div.content'),
		arrow : $('span.arrow'),
		originalElements : $('div#original-elements'),
		arrowDown : $('div#original-elements span.arrow-down'),
		eventTemplate : $('div#event-template'),
		personalEventTemplate : $('div#personal-event-template'),
		groupTemplate : $('div#group-template'),
		roomTemplate : $('div#room-template'),
		privilegeDialog : $('#privilegeChangeDialog'),
		eventDeleteDialog : $('#eventDeleteDialog'),
		errorDialog : $('fail-dialog'),
		currentTab : 'groups-settings',
		currentSubTab : 'subscribed-groups',
		subscriptionTemplate : $('script#subscription-template'),

		currentPage : 0,
		itemsPerPage : 10,

		//EVENTS MANAGEMENT VARS
		deletingEvent : null,

		//USERS MANAGEMENT VARS
		selectedUser : null,
		currentSelect : null,
		oldType : null,
		newType : null
	},

	/*=======================================================*/
	/* Initialize the view, bind events                      */
	/*=======================================================*/

	init : function() {
		var overlay = settingsFunctionality.globals.overlay;
		var navMenu = settingsFunctionality.globals.navMenu;
		var content = settingsFunctionality.globals.content;
		// content.html($('div#groups-settings').html());
		navMenu.on('click', 'a', settingsFunctionality.changeTab);
		content.on('click', 'div.subscribe', settingsFunctionality.subscribe);
		content.on('click', 'div.unsubscribe', settingsFunctionality.unsubscribe);
		content.on('click', 'div.deletegroup', settingsFunctionality.deleteGroup);
		content.on('click', 'div.details', settingsFunctionality.showGroupDetails);
		content.on('input', '#search-groups input[type="text"]', settingsFunctionality.findGroupsTimeout);
		content.on('input', '#search-events input[type="text"]', settingsFunctionality.findEventsTimeout);
		content.on('input', '#search-users input[type="text"]', settingsFunctionality.findUsersTimeout);
		content.on('click', 'input.create-group', settingsFunctionality.showCreateGroup);
		content.on('click', 'input.create-room', settingsFunctionality.showCreateRoom);
		content.on('click', 'input.create-personal-event', settingsFunctionality.showCreatePersonalEvent);
		content.on('click', 'div.event-delete span', settingsFunctionality.deleteEvent);
		content.on('click', 'div.personalevent-delete span', settingsFunctionality.deletePersonalEvent);
		content.on('click', 'div.room-delete span', settingsFunctionality.deleteRoom);
		content.on('click', 'a.tab', settingsFunctionality.changeSecondaryTab);
		overlay.on('click', 'div.tabs a', settingsFunctionality.changeOverlayTab);
		overlay.on('click', 'div#cross', settingsFunctionality.closeOverlay);
		overlay.on('click', 'input.create-event', settingsFunctionality.showCreateEvent);
		overlay.on('click', 'input.manage-subscriptions', settingsFunctionality.showManageSubscriptions);
		overlay.on('click', 'input#add-event', settingsFunctionality.createEvent);
		overlay.on('click', 'input#add-personal-event', settingsFunctionality.createPersonalEvent);
		overlay.on('click', 'input#add-group', settingsFunctionality.createGroup);
		overlay.on('click', 'input#add-room', settingsFunctionality.createRoom);
		overlay.on('click', 'div.force-subscribe', settingsFunctionality.forceSubscribe);
		overlay.on('click', 'div.accept', settingsFunctionality.acceptSubscription);
		overlay.on('click', 'div.refuse', settingsFunctionality.refuse);
		overlay.on('click', 'div.remove', settingsFunctionality.remove);

		settingsFunctionality.initHover();

		// //Event management
		// this.globals.eventDeleteDialog.find('button.yes').button();
		// this.globals.eventDeleteDialog.find('button.no').button();
		// this.globals.eventDeleteDialog.find('button.yes').on('click', this.ajaxDeleteEvent);
		// this.globals.eventDeleteDialog.find('button.no').on('click', this.closeEventDeleteDialog);

		// this.globals.eventDeleteDialog.dialog({
		// 	autoOpen : false
		// });

		//User management
		this.globals.privilegeDialog.find('button.yes').button();
		this.globals.privilegeDialog.find('button.no').button();
		this.globals.privilegeDialog.find('button.yes').on('click', this.ajaxChangePrivilege);
		this.globals.privilegeDialog.find('button.no').on('click', this.closePrivilegeDialog);

		this.globals.privilegeDialog.dialog({
			autoOpen : false
		});
		content.on('change', '#usersTable select', this.changeUserType);
		content.on('focus', '#usersTable select', this.saveOldUserType);
	},

	/*=======================================================*/
	/* Change to the clicked tab                             */
	/*=======================================================*/
	changeTab : function(event) {
		event.preventDefault();
		var content = settingsFunctionality.globals.content;
		var navMenu = settingsFunctionality.globals.navMenu;
		$("div#" + String(settingsFunctionality.globals.currentTab)).addClass('hidden');
		settingsFunctionality.globals.currentTab = $(this).attr('id');
		var contentId = "div#" + String(settingsFunctionality.globals.currentTab);
		$(contentId).removeClass('hidden');
		$('span.arrow').remove('span.arrow');
		navMenu.find('li.selected-option').removeClass('selected-option');
		$(this).closest('li').addClass('selected-option').append(settingsFunctionality.globals.arrow);
	},

	/*=======================================================*/
	/* Change to the clicked subtab                          
	/*=======================================================*/
	changeSecondaryTab : function() {
		var button = $(this);

		var settings = $(this).closest('div.primarytab');
		settings.find('span.arrow-down').remove('span.arrow-down');
		settings.find('a.selected-suboption').removeClass('selected-suboption');
		settings.find('div.tabentry').each(function() {
			$(this).fadeOut(150);
		});
		settingsFunctionality.globals.currentSubTab = button.data('show');

		setTimeout(function() {
			var desired = settings.find('div#' + button.data('show'));
			desired.fadeIn(150);
		}, 150);
		button.addClass('selected-suboption');
		button.append(settingsFunctionality.globals.arrowDown);
	},

	/*=======================================================*/
	/* Change the tab in the overlay
	/*=======================================================*/
	changeOverlayTab : function() {
		var button = $(this);

		var overlay = settingsFunctionality.globals.overlay;
		overlay.find('a.selected').removeClass('selected');
		overlay.find('ul').each(function() {
			$(this).fadeOut(150);
		});
		setTimeout(function() {
			var desired = overlay.find('ul#' + button.data('show'));
			desired.fadeIn(150);
		}, 150);
		button.addClass('selected');
	},

	/*=======================================================*/
	/* Animate navigation links
	/*=======================================================*/
	initHover : function() {

		// Animate buttons, move reflection and fade

		$("div#nav a").hover(function() {
			$(this).stop().animate({
				top : "-6px",
			}, 200);
		}, function() {
			$(this).stop().animate({
				top : "0px"
			}, 300);
		});
	},

	/*=======================================================*/
	/* Set timeout before searching groups
	/*=======================================================*/
	findGroupsTimeout : function(event) {
		var context = this;
		clearTimeout(settingsFunctionality.globals.timeOut);
		settingsFunctionality.globals.timeOut = setTimeout(function() {
			settingsFunctionality.findGroups.call(context);
		}, 500);
	},

	/*=======================================================*/
	/* Search for groups matching the given criteria
	/*=======================================================*/
	findGroups : function(event) {
		var form = $(this);
		$.ajax({
			url : baseUrl + 'groupsmanager/findGroup',
			data : $(form).serialize(),
			dataType : 'json',
			type : 'POST',
			success : function(result) {
				var source = $("script#group-list-template").html();
				var template = Handlebars.compile(source);
				form.parent().parent().find('ul.groupslist').html(template(result));
			}
		});
	},

	/*=======================================================*/
	/* Set timeout before searching events
	/*=======================================================*/
	findEventsTimeout : function(event) {
		var context = this;
		clearTimeout(settingsFunctionality.globals.timeOut);
		settingsFunctionality.globals.timeOut = setTimeout(function() {
			settingsFunctionality.findEvents.call(context);
		}, 500);
	},

	/*=======================================================*/
	/* Search for events matching a criteria
	/*=======================================================*/
	findEvents : function(event) {
		var form = $(this);
		$.ajax({
			url : baseUrl + 'eventsmanager/findEvent',
			data : $(form).serialize(),
			dataType : 'json',
			type : 'POST',
			success : function(result) {
				var source = $("script#event-list-template").html();
				var template = Handlebars.compile(source);
				form.closest('form').next('ul.table-list').eq(0).html(template(result));
			}
		});
	},

	/*=======================================================*/
	/* Set timeout before searching users
	/*=======================================================*/
	findUsersTimeout : function(event) {
		var context = this;
		clearTimeout(settingsFunctionality.globals.timeOut);
		settingsFunctionality.globals.timeOut = setTimeout(function() {
			settingsFunctionality.findUsers.call(context);
		}, 500);
	},

	/*=======================================================*/
	/* Search for users matching a criteria
	/*=======================================================*/
	findUsers : function(event) {
		var form = $(this);
		$.ajax({
			url : baseUrl + 'adminpanel/findUserByName',
			data : $(form).serialize(),
			dataType : 'json',
			type : 'POST',
			success : function(result) {
				var source = $("script#user-list-template").html();
				var template = Handlebars.compile(source);
				settingsFunctionality.globals.content.find('div#users-admin ul.table-list').html(template(result));
			}
		});
	},

	/*=======================================================*/
	/* Initialize time picker UI components
	/*=======================================================*/
	initPickers : function() {
		var overlay = settingsFunctionality.globals.overlay;
		var start_time = overlay.find('input#start-time');
		var end_time = overlay.find('input#end-time');

		start_time.datetimepicker({
			dateFormat : 'yy-mm-dd',
			onClose : function(dateText, inst) {
				if (end_time.val() != '') {
					var testStartDate = new Date(dateText);
					var testEndDate = new Date(end_time.val());
					if (testStartDate > testEndDate) {
						end_time.val(dateText);
					}
				} else {
					end_time.val(dateText);
				}
			},
			onSelect : function(selectedDateTime) {
				var start = $(this).datetimepicker('getDate');
				end_time.datetimepicker('option', 'minDate', new Date(start.getTime()));
			}
		});

		end_time.datetimepicker({
			dateFormat : 'yy-mm-dd',
			onClose : function(dateText, inst) {
				if (start_time.val() != '') {
					var testStartDate = new Date(start_time.val());
					var testEndDate = new Date(dateText);
					if (testStartDate > testEndDate)
						start_time.val(dateText);
				} else {
					start_time.val(dateText);
				}
			},
			onSelect : function(selectedDateTime) {
				var end = $(this).datetimepicker('getDate');
				start_time.datetimepicker('option', 'maxDate', new Date(end.getTime()));
			}
		});
	},


	/*=======================================================*/
	/* Subscribe a user to a group
	/*=======================================================*/
	forceSubscribe : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var userId = $(this).closest('li').attr('data-id').toString();
		var groupId = $(overlay).find('div.tabs').first().attr('data-groupid').toString();
		$.ajax({
			url : baseUrl + 'groupsmanager/forcedSubscription',
			type : 'POST',
			dataType : 'json',
			data : {
				"userid" : userId,
				"groupid" : groupId
			},
			success : function(data) {
				if (data === true) {
					var n = noty({
						text : 'Успешно го зачленивте корисникот во групата!',
						type : 'success',
						dismissQueue : true,
						timeout : 1000,
						layout : 'topCenter',
						theme : 'default',
					});
				}
			}
		});
	},


	/*=======================================================*/
	/* Accept subscription
	/*=======================================================*/
	acceptSubscription : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var Id = $(this).closest('li').attr('data-id').toString();
		overlay.fadeOut(150, function() {
			$.ajax({
				url : baseUrl + 'groupsmanager/approveSubscription',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : Id,
				},
				success : function(data) {
					if (data === true) {
						var n = noty({
							text : 'Барањето за членство беше успешно прифатено!',
							type : 'success',
							dismissQueue : true,
							timeout : 1000,
							layout : 'topCenter',
							theme : 'default',
						});
					}
				}
			});
		});
	},

	/*=======================================================*/
	/* Refuse subscription
	/*=======================================================*/
	refuseSubscription : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var Id = $(this).closest('li').attr('data-id').toString();
		overlay.fadeOut(150, function() {
			$.ajax({
				url : baseUrl + 'groupsmanager/deleteRequest',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : Id
				},
				success : function(data) {
					if (data === true) {
						var n = noty({
							text : 'Барањето за членство беше успешно одбиено!',
							type : 'success',
							dismissQueue : true,
							timeout : 1000,
							layout : 'topCenter',
							theme : 'default',
						});
					}
				}
			});
		});
	},

	/*=======================================================*/
	/* Remove subscription
	/*=======================================================*/
	removeSubscription : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var Id = $(this).closest('li').attr('data-id').toString();
		overlay.fadeOut(150, function() {
			$.ajax({
				url : baseUrl + 'groupsmanager/forcedUnsubscription',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : Id
				},
				success : function(data) {
					if (data === true) {
						var n = noty({
							text : 'Корисникот беше успешно отстранет од групата!',
							type : 'success',
							dismissQueue : true,
							timeout : 1000,
							layout : 'topCenter',
							theme : 'default',
						});
					}
				}
			});
		});
	},

	/*=======================================================*/
	/* Unsubscribe from group
	/*=======================================================*/
	unsubscribe : function(event) {
		event.preventDefault();
		var list = $(this).closest('li.group-entry');
		var id = list.attr('id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да се отпишете од групата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'groupsmanager/unsubscribe',
						type : 'POST',
						dataType : 'json',
						data : {
							"groupid" : id
						},
						success : function(data) {
							if (data === true) {
								var n = noty({
									text : 'Успешно бевте отпишани од групата!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								var subscribedTab = $('div#subscribed-groups ul.groupslist');
								var subscribedList = $('div#subscribed-groups li[id="' + id + '"]');
								subscribedList.fadeOut(300, function() {
									subscribedList.remove();
									if ($.trim(subscribedTab.html()) == '') {
										subscribedTab.html('<div class="empty-list">' + 'нема групи' + '</div>');
									}
								});

								var allList = $('div#all-groups li[id="' + id + '"]');
								var operations = allList.find('div.operations');
								var originalElements = $(settingsFunctionality.globals.originalElements);
								operations.find('div.unsubscribe').remove();
								operations.append(originalElements.find('div.subscribe').clone());
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},

	/*=======================================================*/
	/* Subscribe to group
	/*=======================================================*/
	subscribe : function(event) {
		event.preventDefault();
		var list = $(this).closest('li.group-entry');
		var id = list.attr('id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да се приклучите на групата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'groupsmanager/requestSubscription',
						type : 'POST',
						dataType : 'json',
						data : {
							"groupid" : id
						},
						success : function(data) {
							if (data === "PUBLIC") {
								var n = noty({
									text : 'Успешно бевте приклучени кон групата',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								var operations = list.find('div.operations');
								var originalElements = $(settingsFunctionality.globals.originalElements);
								operations.find('div.subscribe').remove();
								operations.append(originalElements.find('div.unsubscribe').clone());

								var subscribedList = $('div#subscribed-groups ul.groupslist');

								console.log(subscribedList);

								subscribedList.find('div.empty-list').remove();
								subscribedList.append('<li class="group-entry" id="' + id + '">' + '<div class="name">' + list.find('div.name').html() + '</div>' + '<div class="operations">' + '	<div class="details" title="Детали"></div>' + '	<div class="unsubscribe" title="Откажи претплата"></div>' + '</div>' + '</li>');
							}
							else if(data === "PRIVATE"){
								var n = noty({
									text : 'Успешно беше испратено барање за приклучување',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});

								var operations = list.find('div.operations');
								var originalElements = $(settingsFunctionality.globals.originalElements);
								operations.find('div.subscribe').remove();
								operations.append(originalElements.find('div.pending').clone());
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},

	/*=======================================================*/
	/* Show subscription manager
	/*=======================================================*/
	showManageSubscriptions : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var groupId = $(this).attr('data-groupid').toString();
		overlay.fadeOut(150, function() {
			$.ajax({
				url : baseUrl + 'groupsmanager/getGroupUsers',
				type : 'POST',
				dataType : 'json',
				data : {
					"id" : groupId
				},
				success : function(data) {
					var subscriptionTemplate = settingsFunctionality.globals.subscriptionTemplate.html();
					var template = Handlebars.compile(subscriptionTemplate);
					data.groupid = groupId;
					overlay.children('div#overlay-content').html(template(data));
					overlay.fadeIn(150);
				}
			});
		});
	},

	/*=======================================================*/
	/* Show event creation dialog
	/*=======================================================*/
	showCreateEvent : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var groupId = $(this).attr('data-groupid').toString();
		overlay.fadeOut(150, function() {
			var overlay = settingsFunctionality.globals.overlay;
			var eventTemplate = settingsFunctionality.globals.eventTemplate;
			eventTemplate.find('input#groupid').eq(0).val(String(groupId));
			overlay.children('div#overlay-content').html(eventTemplate.html());
			overlay.fadeIn(150);
			settingsFunctionality.initPickers();
		});
	},

	/*=======================================================*/
	/* Show personal event creation dialog
	/*=======================================================*/
	showCreatePersonalEvent : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var personalEventTemplate = settingsFunctionality.globals.personalEventTemplate;
		overlay.children('div#overlay-content').html(personalEventTemplate.html());
		overlay.fadeIn(300);
		settingsFunctionality.initPickers();
	},

	/*=======================================================*/
	/* Show group creation dialog
	/*=======================================================*/
	showCreateGroup : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		overlay.fadeOut(150, function() {
			var overlay = settingsFunctionality.globals.overlay;
			var groupTemplate = settingsFunctionality.globals.groupTemplate;
			overlay.children('div#overlay-content').html(groupTemplate.html());
			overlay.fadeIn(150);
		});
	},

	/*=======================================================*/
	/* Show room creation dialog
	/*=======================================================*/
	showCreateRoom : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		overlay.fadeOut(150, function() {
			var overlay = settingsFunctionality.globals.overlay;
			var groupTemplate = settingsFunctionality.globals.roomTemplate;
			overlay.children('div#overlay-content').html(groupTemplate.html());
			overlay.fadeIn(150);
		});
	},

	/*=======================================================*/
	/* Show group details
	/*=======================================================*/
	showGroupDetails : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		var groupId = $(this).closest('li').attr('id');
		$.ajax({
			url : baseUrl + 'groupsmanager/getGroupData',
			type : 'POST',
			dataType : 'json',
			data : {
				"groupid" : groupId
			},
			success : function(data) {
				var source = $('script#group-details-template').html();
				var template = Handlebars.compile(source);
				overlay.children('div#overlay-content').html(template(data));
				overlay.fadeIn(300);
			}
		});
	},

	/*=======================================================*/
	/* Close the overlay
	/*=======================================================*/
	closeOverlay : function(event) {
		event.preventDefault();
		var overlay = settingsFunctionality.globals.overlay;
		overlay.fadeOut(300);
	},

	
	/*=======================================================*/
	/* Delete group
	/*=======================================================*/
	deleteGroup : function(event) {
		event.preventDefault();
		var list = $(this).closest('li.group-entry');
		var id = list.attr('id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да ја избришете групата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'groupsmanager/deleteGroups',
						type : 'POST',
						dataType : 'json',
						data : {
							"ids[]" : id
						},
						success : function(data) {
							if (data === true) {
								var n = noty({
									text : 'Групата беше успешно избришана!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								list.fadeOut(300, function() {
									list.remove();
								});
								$('ul.groupslist li.group-entry[id="' + id + '"]').fadeOut(300, function() {
									$(this).remove();
								});
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},


	/*=======================================================*/
	/* Create event
	/*=======================================================*/
	createEvent : function(event) {
		event.preventDefault();
		var form = $(this).closest('form');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да го креирате настанот?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'eventsmanager/insertGroupEvent',
						type : 'POST',
						dataType : 'json',
						data : form.serialize(),
						success : function(data) {
							if (data.status === true) {

								var n = noty({
									text : 'Настанот беше успешно креиран!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								settingsFunctionality.globals.overlay.fadeOut(300);
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},


	/*=======================================================*/
	/* Create personal event
	/*=======================================================*/
	createPersonalEvent : function(event) {
		event.preventDefault();
		var form = $(this).closest('form');

		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да го креирате настанот?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'eventsmanager/insertPrivateEvent',
						type : 'POST',
						dataType : 'json',
						data : form.serialize(),
						success : function(data) {
							if (data.status === true) {
								var n = noty({
									text : 'Настанот беше успешно креиран!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
							}
							settingsFunctionality.globals.overlay.fadeOut(300);
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},


	/*=======================================================*/
	/* Create group
	/*=======================================================*/
	createGroup : function(event) {
		event.preventDefault();
		var form = $(this).closest('form');
		var formData = form.serialize();
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да ја креирате групата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'groupsmanager/insertGroup',
						type : 'POST',
						dataType : 'json',
						data : formData,
						success : function(data) {
							if (data !== false) {

								var n = noty({
									text : 'Групата беше успешно креирана!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});

								var serializedForm = (form.serializeArray());

								$('div#groups-admin ul.groupslist').prepend('<li class="group-entry" id="' + data + '">' + '<div class="name">' + serializedForm[0].value + '</div>' + '<div class="operations">' + '<div class="details" title="Детали"></div>' + '<div class="deletegroup delete" title="Избриши група"></div>' + '</div>' + '</li>');
								if (serializedForm[2].value !== userId) {
									$('div#all-groups ul.groupslist').prepend('<li class="group-entry" id="' + data + '">' + '<div class="name">' + serializedForm[0].value + '</div>' + '<div class="operations">' + '<div class="details" title="Детали"></div>' + '<div class="subscribe" title="Претплати се"></div>' + '</div>' + '</li>');
								} else {
									$('div#all-groups ul.groupslist').prepend('<li class="group-entry" id="' + data + '">' + '<div class="name">' + serializedForm[0].value + '</div>' + '<div class="operations">' + '<div class="details" title="Детали"></div>' + '<div class="mygroup" title="Моја група"></div>' + '</div>' + '</li>');
									$('div#my-groups ul.groupslist').prepend('<li class="group-entry" id="' + data + '">' + '<div class="name">' + serializedForm[0].value + '</div>' + '<div class="operations">' + '<div class="details" title="Детали"></div>' + '<div class="mygroup" title="Моја група"></div>' + '</div>' + '</li>');
								}

								settingsFunctionality.globals.overlay.fadeOut(300);
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},


	/*=======================================================*/
	/* Create room
	/*=======================================================*/
	createRoom : function(event) {
		event.preventDefault();
		var form = $(this).closest('form');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да ја креирате просторијата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();
					$.ajax({
						url : baseUrl + 'roomsmanager/insertRoom',
						type : 'POST',
						dataType : 'json',
						data : form.serialize(),
						success : function(data) {
							if (data !== false) {
								var n = noty({
									text : 'Просторијата беше успешно креирана!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
							}
							settingsFunctionality.globals.overlay.fadeOut(300);
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
	},

	/*=======================================================*/
	/* USER MANAGEMENT FUNCTIONS
	/*
	/*=======================================================*/

	/*=======================================================*/
	/* Change user type
	/*=======================================================*/
	ajaxChangePrivilege : function() {
		$.ajax({
			url : baseUrl + 'adminpanel/changeUserPrivilege',
			data : {
				userid : settingsFunctionality.globals.selectedUser,
				type : settingsFunctionality.globals.newType
			},
			type : 'POST',
			success : function(data) {
				settingsFunctionality.globals.oldType = settingsFunctionality.globals.newType;
				settingsFunctionality.globals.privilegeDialog.dialog('close');
			}
		});
	},

	/*=======================================================*/
	/* Close privilege dialog
	/*=======================================================*/
	closePrivilegeDialog : function() {
		settingsFunctionality.globals.privilegeDialog.dialog('close');
		currentSelect.val(settingsFunctionality.globals.oldType);
	},

	/*=======================================================*/
	/* Change user privilege
	/*=======================================================*/
	saveOldUserType : function() {
		settingsFunctionality.globals.oldType = $(this).attr('value');
		currentSelect = $(this);
	},

	/*=======================================================*/
	/* Change user privilege confirmation dialog
	/*=======================================================*/
	changeUserType : function() {
		var element = $(this);
		var userid = element.closest('tr').data('id');
		var newType = element.find('option:selected').val();
		settingsFunctionality.globals.privilegeDialog.dialog('open');
		settingsFunctionality.globals.selectedUser = userid;
		settingsFunctionality.globals.newType = newType;
	},



	/*=======================================================*/
	/* EVENTS MANAGEMENT FUNCTIONS
	/*
	/*=======================================================*/

	deleteEvent : function(event) {
		event.preventDefault();
		event.preventDefault();
		var list = $(this).closest('li.admin-events');
		var id = list.attr('data-id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да го избришете настанот?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();

					$.ajax({
						url : baseUrl + 'eventsmanager/deleteEvent',
						type : 'POST',
						dataType : 'json',
						data : {
							"id" : id
						},
						success : function(data) {
							if (data === true) {
								var n = noty({
									text : 'Настанот беше успешно избришан!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								list.fadeOut(300, function() {
									list.remove();
								});
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
		// settingsFunctionality.globals.deletingEvent = $(this).closest('li').data('id');
		// settingsFunctionality.globals.eventDeleteDialog.dialog('open');
	},

	/*=======================================================*/
	/* Delete a user's personal event
	/*=======================================================*/
	deletePersonalEvent : function(event) {
		event.preventDefault();
		event.preventDefault();
		var list = $(this).closest('li.list-row');
		var id = list.attr('data-id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да го избришете настанот?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();

					$.ajax({
						url : baseUrl + 'eventsmanager/deletePrivateEvent',
						type : 'POST',
						dataType : 'json',
						data : {
							"id" : id
						},
						success : function(data) {
							if (data === true) {
								var n = noty({
									text : 'Настанот беше успешно избришан!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								list.fadeOut(300, function() {
									list.remove();
								});
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
		// settingsFunctionality.globals.deletingEvent = $(this).closest('li').data('id');
		// settingsFunctionality.globals.eventDeleteDialog.dialog('open');
	},


	/*=======================================================*/
	/* Delete a room
	/*=======================================================*/
	deleteRoom : function(event) {
		event.preventDefault();
		event.preventDefault();
		var list = $(this).closest('li.room');
		var id = list.attr('data-id');
		var notyq = noty({
			text : 'Дали сте сигурни дека сакате да ја избришете просторијата?',
			type : 'confirm',
			layout : 'topCenter',
			theme : 'default',
			buttons : [{
				addClass : 'btn btn-primary',
				text : 'Да',
				onClick : function() {
					notyq.close();

					$.ajax({
						url : baseUrl + 'roomsmanager/deleteRoom',
						type : 'POST',
						dataType : 'json',
						data : {
							"room_id" : id
						},
						success : function(data) {
							if (data === true) {
								var n = noty({
									text : 'Просторијата беше успешно избришана!',
									type : 'success',
									dismissQueue : true,
									timeout : 1000,
									layout : 'topCenter',
									theme : 'default',
								});
								list.fadeOut(300, function() {
									list.remove();
								});
							}
						}
					});
				}
			}, {
				addClass : 'btn btn-danger',
				text : 'Не',
				onClick : function() {
					notyq.close();
				}
			}]
		});
		// settingsFunctionality.globals.deletingEvent = $(this).closest('li').data('id');
		// settingsFunctionality.globals.eventDeleteDialog.dialog('open');
	},
}

$(document).ready(function() {
	settingsFunctionality.init();
});
